package com.gs.objectRepo;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class MatchingModulePage {
	public static WebElement element = null;
	
	
	public static WebElement matchingMoodule(WebDriver driver) {
		By matchingMoodule = By.xpath("//span[contains(text(),'Matching Module')]");
		return driver.findElement(matchingMoodule);
	}
	
	public static WebElement manualMatching(WebDriver driver) {
		By manualMatching = By.xpath("//span[contains(text(),'Manual Matching')]");
		return driver.findElement(manualMatching);
	}
	
	public static void getSelectMatchingType(WebDriver driver, String text) throws InterruptedException {
		element = driver.findElement(By.xpath("//select[@id='Types']"));
		Select sel = new Select(element);
		Thread.sleep(2000);
		sel.selectByVisibleText(text);

	}
	
	public static void getSelectMatch(WebDriver driver, String text) throws InterruptedException {
		element = driver.findElement(By.xpath("//select[@id='Match']"));
		Select sel = new Select(element);
		Thread.sleep(2000);
		sel.selectByVisibleText(text);

	}
	
	public static void getSelectMatchWith(WebDriver driver, String text) throws InterruptedException {
		element = driver.findElement(By.xpath("//select[@id='With']"));
		Select sel = new Select(element);
		Thread.sleep(2000);
		sel.selectByVisibleText(text);

	}
	
	public static WebElement btnSubmit(WebDriver driver) {
		element = driver.findElement(By.xpath("//button[contains(text(),'Submit')]"));
		return element;
	}
	
	public static WebElement searchMatchPaymentID(WebDriver driver) {
		element = driver.findElement(By.xpath("//div[@id='Match']//input[@id='searchBox']"));
		return element;
	}
	
	public static WebElement searchMatchWithPaymentID(WebDriver driver) {
		element = driver.findElement(By.xpath("//div[@id='With']//input[@id='searchBox']"));
		return element;
	}
	
	public static WebElement btnSearchMatchIcon(WebDriver driver) {
		element = driver.findElement(By.xpath("//span[@id='SearchFontIcon']//i[@class='fa fa-search']"));
		return element;
	}
	
	public static WebElement btnSearchMatchWithIcon(WebDriver driver) {
		element = driver.findElement(By.xpath("//div[@id='With']//div[@class='row ng-scope']//i[@class='fa fa-search']"));
		return element;
	}
	
	public static WebElement selectMatchCheckBox(WebDriver driver) {
		element = driver.findElement(By.xpath("//*[@id='sectionTwoTableOne']/div/div[2]/table/tbody/tr/td[last()]/div/input[@type='checkbox']"));
		return element;
	}
	
	public static WebElement selectMatchWithCheckBox(WebDriver driver) {
		element = driver.findElement(By.xpath("//*[@id=\"SectionThreeTable\"]/table/tbody/tr/td[last()]/div/input[@type='checkbox']"));
		return element;
	}
	
	public static WebElement matchPaymentId(WebDriver driver,String sPaymentId) {
		element = driver.findElement(By.xpath("//td[@class='bold ng-binding'][contains(text(),'"+sPaymentId+"')]"));
		return element;
	}
	
	public static WebElement btnForceMatch(WebDriver driver) {
		element = driver.findElement(By.xpath("//div[@id='Match']//button[@id='forcematch']"));
		return element;
	}
	
	
	
	
	
	
	
	
}
