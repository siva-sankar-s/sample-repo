package com.gs.objectRepo;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class FileUploadPage 
{
public static WebElement fileUploadSec(WebDriver driver)
{
	//element = driver.findElement(By.xpath("//*[@id='FileUpload']/a/span"));
	//element = driver.findElement(By.xpath("//span[contains(text(),'File Upload')]"));
	WebElement element = driver.findElement(By.xpath("//span[@class='ng-binding'][contains(text(),'File Upload')]"));
	return element;
}
public static WebElement selPSAField(WebDriver driver)
{
	By selPSAField = By.xpath("//span[contains(text(),'Select an option')]");
	return driver.findElement(selPSAField);
}
public static WebElement enterPSA(WebDriver driver)
{
	By enterPSA = By.xpath("//span[contains(text(),'Select an option')]//following::input[2]");
	return driver.findElement(enterPSA);
}

public static WebElement uploadBtn(WebDriver driver)
{
	By uploadBtn = By.id("uploadHere");
	return driver.findElement(uploadBtn);	
}

public static WebElement setParty(WebDriver driver)
{
	By setParty = By.xpath("//span[@id='select2-party-container']");
	return driver.findElement(setParty);
}
public static WebElement setPartyTxtBox(WebDriver driver)
{
	By setPartyTxtBox = By.xpath("//input[@class='select2-search__field']");
	return driver.findElement(setPartyTxtBox);
	//*[@id='select2-party-container']//following::input[@class='select2-search__field'][@role='textbox']"));
}

public static WebElement setService(WebDriver driver)
{
	By setService = By.xpath("//span[@id='select2-service-container']");
	return driver.findElement(setService);
}
public static WebElement setServiceTxtBox(WebDriver driver)
{
	By setServiceTxtBox = By.xpath("//input[@class='select2-search__field']");
	return driver.findElement(setServiceTxtBox);
	
//"//span[@id='select2-service-container']//following::input[@class='select2-search__field'][@role='textbox']"));
}

public static WebElement setInputFormat(WebDriver driver)
{
	By setInputFormat = By.xpath("//span[@id='select2-inputformat-container']");
	return driver.findElement(setInputFormat);
}

public static WebElement setInputFormatTxtBox(WebDriver driver)
{
	By setInputFormatTxtBox = By.xpath("//input[@class='select2-search__field']");
	return driver.findElement(setInputFormatTxtBox);
}
public static WebElement setPSA(WebDriver driver)
{
	By setPSA = By.xpath("//span[@id='select2-Filechannel-container']");
	return driver.findElement(setPSA);
}

public static WebElement setPSATxtBox(WebDriver driver)
{
	By setPSATxtBox = By.xpath("//span[@id='select2-Filechannel-container']//following::input[@class='select2-search__field'][@role='textbox']");
	return driver.findElement(setPSATxtBox);
}

public static WebElement chooseFile(WebDriver driver)
{
	System.out.println("under choose file");
	System.out.println("After clicking nd before return choose file");
	By chooseFile = By.xpath("//input[@file-model='myFile']");
	return driver.findElement(chooseFile);
//By.id("uploadBtn"));
//	driver.findElement(By.name("submit")).sendKeys(Keys.ENTER);//
}



}
