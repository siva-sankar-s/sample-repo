package com.gs.objectRepo;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.gs.utilities.WaitLibrary;

public class SecurityPage {
	
public static WebElement element=null;
	
	public static WebElement security(WebDriver driver)
	{
		By security = By.xpath("//a[contains(@title,'Security')]");
		WaitLibrary.waitForElementPresence(driver, 30, security);
		//*[@id="Security"]/a/span
		return driver.findElement(security);
	}
	
	public static WebElement approvals(WebDriver driver)
	{
		By approvals = By.xpath("//a[contains(@title,'Approvals')]");
		WaitLibrary.waitForElementPresence(driver, 30, approvals);
		return driver.findElement(approvals);
	}
	
	public static WebElement filterIcon(WebDriver driver)
	{
		By filterIcon = By.xpath("//i[@class='fa fa-filter']");
		return driver.findElement(filterIcon);
	}
	
	public static WebElement keywordSearch(WebDriver driver)
	{
		By keywordSearch = By.xpath("//input[@name='keywordSearch']");
		return driver.findElement(keywordSearch);
	}
	
	public static WebElement searchSymbol(WebDriver driver)
	{
//		By searchSymbol = By.xpath("//span[@class='input-group-addon']//i[@class='fa fa-search']");
		By searchSymbol = By.xpath("//div[@class='row input-group pull-right']/button[2]");
//											
		return driver.findElement(searchSymbol);
	}
	public static WebElement closeSearchBox(WebDriver driver)
	{
		By closeSearchBox = By.xpath("//i[@class='fa fa-times']");
		return driver.findElement(closeSearchBox);
	}
	public static WebElement user(WebDriver driver)
	{
		By user = By.xpath("//div[@id='dataGroup_0']");
		return driver.findElement(user);
	}
	public static WebElement businessKeyWithPaymentId(WebDriver driver, String paymentid)
	{
		By businessKeyWithPaymentId = By.xpath("//td[@class='ng-scope']//span[@class='ng-binding ng-scope'][contains(text(),'PaymentID:"+paymentid+"')]");
		return driver.findElement(businessKeyWithPaymentId);
	}
	public static WebElement businessKeyWithUserId(WebDriver driver, String user)
	{
		By businessKeyWithUserId = By.xpath("//td[@class='ng-scope']//span[@class='ng-binding ng-scope'][contains(text(),'"+user+"')]");
		return driver.findElement(businessKeyWithUserId);
	}
	
	/*public static WebElement businessKeyWithPaymentId(WebDriver driver, String paymentid)
	{
	element = driver.findElement(By.xpath("//td[@class='ng-scope']//span[@class='ng-binding ng-scope'][contains(text(),'PaymentID:"+paymentid+"')]"));
	return element;
	}*/
	
	public static WebElement approve(WebDriver driver)
	{
		By approve = By.xpath("//input[@value='Approve']");
		return driver.findElement(approve);
	}
	public static WebElement reject(WebDriver driver)
	{
		By reject = By.xpath("//input[@value='Reject']");
		return driver.findElement(reject);
	}
	
	public static WebElement notes(WebDriver driver)
	{
		By notes = By.xpath("//textarea[@id='idforNotes']");
		return driver.findElement(notes);
	}
	
	
	public static WebElement cancel(WebDriver driver)
	{
		By cancel = By.xpath("//input[@value='Cancel']");
		return driver.findElement(cancel);
	}
	public static WebElement clickele(WebDriver driver, String instid) 
	{
		By clickele = By.xpath("//div[@class='listView FixHead dataGroupsScroll']//td//span[@class='ng-binding ng-scope'][contains(text(),'"+ instid + "')]/../../td[1]");
		return driver.findElement(clickele);
		////div[@class='listView FixHead dataGroupsScroll']//span[@class='ng-binding ng-scope'][contains(text(),'"+instid+"')]
		
	}

}
