package com.gs.objectRepo;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class AddOn 
{
	public static WebElement element=null;

	public static WebElement addOnTab(WebDriver driver)
	{
		By addOnTab = By.xpath("//span[contains(text(),'Add on')]");
		return driver.findElement(addOnTab);
	}
	
	public static WebElement logicaTerminalProfile(WebDriver driver)
	{
		By logicaTerminalProfile = By.xpath("//span[contains(text(),'Logical Terminal Profile')]");
		return driver.findElement(logicaTerminalProfile);
	}

	public static WebElement ltermID1(WebDriver driver)
	{
		By ltermID1 = By.xpath("//div[contains(text(),'LTERMID1 - LTERMID1')]");
		return driver.findElement(ltermID1);
	}
	
	public static WebElement ltermID1Page(WebDriver driver)
	{
		By ltermID1Page = By.xpath("//div[@class='panel-group']//div[@class='row']");
		return driver.findElement(ltermID1Page);
	}
}
