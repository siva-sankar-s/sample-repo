package com.gs.objectRepo;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import com.gs.utilities.WaitLibrary;

public class AllPaymentsPage {
	public static WebElement element = null;

	public static WebElement getRRFI_IMAD_Status_COMPLETED(WebDriver driver) {
		By rrfiIMADStatus = By.xpath("//tr[last()-3]//td[9]/p");
		return driver.findElement(rrfiIMADStatus);
	}

	public static WebElement getRRFI_IMAD_Status_WAIT_IN_RRFI(WebDriver driver) {
		By rrfiIMADStatus = By.xpath("//tr[last()-1]//td[9]/p");
		return driver.findElement(rrfiIMADStatus);
	}

	public static WebElement getRRFI_IMAD_Status(WebDriver driver) {
		By rrfiIMADStatus = By.xpath("//tr[last()]//td[9]/p");
		return driver.findElement(rrfiIMADStatus);
	}

	public static WebElement getRRFI_IMAD_ID(WebDriver driver) {
		By rrfiIMAD = By.xpath("//tr[last()]//td[1]//a[1]");
		return driver.findElement(rrfiIMAD);
	}

//To navigate to All Payments Section
	public static WebElement goToAllPayments(WebDriver driver) {
		By goToAllPayments = By.xpath("//span[contains(text(),'All Payments')]");
		return driver.findElement(goToAllPayments);
	}

	public static List<WebElement> getAllPaymentIds(WebDriver driver) {
		By getAllPaymentIds = By.xpath("//span[@data-original-title='Payment ID']");
		return driver.findElements(getAllPaymentIds);
	}

	public static WebElement paymentModule(WebDriver driver) {
		By paymentModule = By.xpath("//span[contains(text(),'Payment Module')]");
		return driver.findElement(paymentModule);
	}

	public static WebElement checkStatus(WebDriver driver) {
		By checkStatus = By.xpath("//span[contains(text(),'COMPLETED')]");
		return driver.findElement(checkStatus);

	}

	public static WebElement sidebar(WebDriver driver) {
		By sidebar = By.xpath("//section[@class='sidebar']//div[@class='slimScrollBar']");
		return driver.findElement(sidebar);
	}

//To navigate to External Communications Section
	public static WebElement goToExtComm(WebDriver driver) {
		By goToExtComm = By.xpath("//b[contains(text(),'External Communications')]");
		return driver.findElement(goToExtComm);
	}
	
	public static WebElement goToIssueInformation(WebDriver driver) 
	{
		By goToIssueInformation = By.xpath("//b[contains(text(),'Issue Information')]");
		return driver.findElement(goToIssueInformation);
	}
	
	public static WebElement issueInformationTable(WebDriver driver) 
	{
		By issueInformationTable = By.xpath("//div[@id='tableExport']");
		return driver.findElement(issueInformationTable);
	}
	
	
	

	public static WebElement goToSysInteration(WebDriver driver) {
		By goToSysInteration = By.xpath("//b[contains(text(),'System Interaction')]");
		return driver.findElement(goToSysInteration);
	}

	public static WebElement goToLinkedMessages(WebDriver driver) {
		By goToLinkedMessages = By.xpath("//b[contains(text(),'Linked Messages')]");
		return driver.findElement(goToLinkedMessages);
	}

	public static WebElement goToReturnedCustCredTrans(WebDriver driver) {
		By goToReturnedCustCredTrans = By.xpath(
				"//*[@id='tab6']/tbody/tr/td[2][contains(text(),'Returned Customer Credit Transfer')]/following::td[1][contains(text(),'OUT')]");
		return driver.findElement(goToReturnedCustCredTrans);
	}

	public static WebElement goToRFPCustCredTrans(WebDriver driver) {
		By goToRFPCustCredTrans = By.xpath(
				"//*[@id='tab6']/tbody/tr/td[3][contains(text(),'OUT')]/preceding::td[.='Customer Credit Transfer-RFP']");
		return driver.findElement(goToRFPCustCredTrans);
	}

	public static WebElement goToRFP_IN_CustCredTrans(WebDriver driver) {
		By goToRFP_IN_CustCredTrans = By.xpath(
				"//*[@id='tab6']/tbody/tr/td[3][contains(text(),'IN')]/preceding::td[.='Customer Credit Transfer']");
		return driver.findElement(goToRFP_IN_CustCredTrans);
	}

	public static WebElement goToROF_IN_CredTrans(WebDriver driver) {
		By goToROF_IN_CredTrans = By.xpath(
				"//*[@id='tab6']/tbody/tr/td[3][contains(text(),'IN')]/preceding::td[.='Credit Transfer - Return of Funds']");
		return driver.findElement(goToROF_IN_CredTrans);
	}

	public static WebElement getMessageIdPaymentInstruction(WebDriver driver) {
		By getMessageIdPaymentInstruction = By.xpath(
				"//*[@id='tab3']/tbody/tr/td[3][contains(text(),'Payment Instruction')]/following::td[2][contains(text(),'OUT')]/preceding::td[3]");
		return driver.findElement(getMessageIdPaymentInstruction);
	}

	public static WebElement getMessageIdPropAck(WebDriver driver) {
		By getMessageIdPropAck = By.xpath(
				"//*[@id='tab3']/tbody/tr/td[3][.='Proprietary Acknowledgement']/following::td[2][contains(text(),'OUT')]/preceding::td[3]");
		return driver.findElement(getMessageIdPropAck);
	}

	public static WebElement getMessageId_RFP(WebDriver driver) {
		By getMessageId_RFP = By.xpath(
				"//*[@id='tab3']/tbody/tr/td[.='RequestForPayment-Ack']/following::td[2][contains(text(),'OUT')]/preceding::td[3]");
		return driver.findElement(getMessageId_RFP);
	}

	public static WebElement getGroupReferenceId(WebDriver driver) {
		By getGroupReferenceId = By.xpath(
				"//*[@id='tab3']/tbody/tr/td[3][contains(text(),'Payment Instruction')]/following::td[2][contains(text(),'OUT')]/preceding::td[4]");
		return driver.findElement(getGroupReferenceId);
	}

	public static WebElement getStatusForWaitActionRRFI(WebDriver driver) {
		By getStatusForWaitActionRRFI = By.xpath(
				"//*[@id='tab3']/tbody/tr/td[3][contains(text(),'FreeFormatMessage')]/following::td[2][contains(text(),'OUT')]/following::td[4]//p");
		return driver.findElement(getStatusForWaitActionRRFI);
	}

	public static WebElement getMessageIdRFI(WebDriver driver) {
		By getMessageIdRFI = By.xpath(
				"//*[@id='tab3']/tbody/tr/td[3][contains(text(),'Request for Information')]/following::td[2][contains(text(),'OUT')]/preceding::td[3]");
		return driver.findElement(getMessageIdRFI);
	}

	public static WebElement getMessageIdRRFI(WebDriver driver) {
		By getMessageIdRRFI = By.xpath(
				"//*[@id='tab3']/tbody/tr/td[3][.='Response to Request for Information']/following::td[2][contains(text(),'OUT')]/preceding::td[3]");
		return driver.findElement(getMessageIdRRFI);
	}

	public static WebElement getMessageIdROF(WebDriver driver) {
		By getMessageIdROF = By.xpath(
				"//*[@id='tab3']/tbody/tr/td[.='Return of Funds']/following::td[2][contains(text(),'OUT')]/preceding::td[3]");
		return driver.findElement(getMessageIdROF);
		// element=
		// driver.findElement(By.xpath("//*[@id='tab3']/tbody/tr/td[3][contains(text(),'Return
		// of Funds')]/following::td[2][contains(text(),'OUT')]/preceding::td[3]"));
	}

	public static WebElement getMessageIdRROF(WebDriver driver) {
		By getMessageIdRROF = By.xpath(
				"//*[@id='tab3']/tbody/tr/td[3][contains(text(),'Response to Return of Funds')]/following::td[2][contains(text(),'OUT')]/preceding::td[3]");
		return driver.findElement(getMessageIdRROF);
		// element= driver.findElement(By.xpath("//*[@id='tab3']/tbody/tr/td[.='Return
		// of Funds')]/following::td[2][contains(text(),'OUT')]/preceding::td[3]"));
	}

	public static WebElement getMessageIdACKPACS002(WebDriver driver) {
		By getMessageIdACKPACS002 = By.xpath(
				"//*[@id='tab3']/tbody/tr/td[.='ACK to US-RTPG-PACS002']/following::td[2][contains(text(),'OUT')]/preceding::td[3]");
		return driver.findElement(getMessageIdACKPACS002);
	}

	public static WebElement getMessageIdRRFP(WebDriver driver) {
		By getMessageIdRRFP = By.xpath(
				"//*[@id='tab3']/tbody/tr/td[3][.='Response to Request for Payment']/following::td[2][contains(text(),'OUT')]/preceding::td[3]");
		return driver.findElement(getMessageIdRRFP);
	}

	public static WebElement getGrpRefIdRRFP(WebDriver driver) {
		By getGrpRefIdRRFP = By.xpath(
				"//*[@id='tab3']/tbody/tr/td[3][.='Response to Request for Payment']/following::td[2][contains(text(),'OUT')]/preceding::td[4]");
		return driver.findElement(getGrpRefIdRRFP);
	}

	public static void callRefresh(WebDriver driver) throws Exception {
		By callRefresh = By.xpath("//*[@tooltip= 'Refresh']");
		for (int j = 0; j < 4; j++) {
			driver.findElement(callRefresh).click();
			Thread.sleep(2000);
		}
	}

	public static void callRefreshByCount(WebDriver driver, int count) throws Exception {
		By callRefreshByCount = By.xpath("//*[@tooltip= 'Refresh']");
		for (int j = 0; j < count; j++) {
			driver.findElement(callRefreshByCount).click();
			Thread.sleep(2000);
		}
	}

	public static WebElement getPaymentID(WebDriver driver, String InsID) {
		By getPaymentID = By.xpath(
				"//span[contains(text(),'" + InsID + "')]/following::span[1][@data-original-title='Payment ID']");
		return driver.findElement(getPaymentID);
		// element
		// =driver.findElement(By.xpath("//span[contains(text(),'"+InsID+"')]/following::span[1]"));
	}

	public static WebElement getPaymentReference(WebDriver driver, String pmtId) {
		By getPaymentReference = By.xpath("//span[contains(text(),'" + pmtId
				+ "')][@data-original-title='Payment ID']/following::span[.='Original Payment Reference']/following::span[1]");
		return driver.findElement(getPaymentReference);
	}

	public static WebElement selectIncomingPmtId(WebDriver driver, String pmtRef) {
		By selectIncomingPmtId = By.xpath("//span[contains(text(),'" + pmtRef
				+ "')]/preceding::span[contains(text(),'IN_US_TCH_RTP')]//preceding::span[@data-original-title='Payment ID']");
		return driver.findElement(selectIncomingPmtId);
	}

	public static WebElement getPaymentStatus(WebDriver driver, String InsId) {
		By getPaymentStatus = By.xpath(
				"//span[contains(text(),'" + InsId + "')]/following::span[4][@data-original-title='Payment Status']");
		return driver.findElement(getPaymentStatus);
	}

	public static WebElement getPmtstatusFrmPmtid(WebDriver driver, String pmtId) {
		By getPmtstatusFrmPmtid = By.xpath("//span[contains(text(),'" + pmtId
				+ "')][@data-original-title='Payment ID']/following::span[3][@data-original-title='Payment Status']");
		return driver.findElement(getPmtstatusFrmPmtid);
	}

	public static WebElement getMOP(WebDriver driver, String pmtId) {
		By getMOP = By.xpath("//span[contains(text(),'" + pmtId
				+ "')][@data-original-title='Payment ID']/following::span[.='MOP']/following::span[1]");
		return driver.findElement(getMOP);
	}

	public static WebElement getOFACReqStatus(WebDriver driver) {
		By getOFACReqStatus = By.xpath(
				".//*[@id='tab2']/tbody/tr/td[3][contains(text(),'REQUEST')]/following::td[1][contains(text(),'OFAC')]/following::td[4]");
		return driver.findElement(getOFACReqStatus);
	}

	public static WebElement getBECNotifSuccessStatus(WebDriver driver) {
		element = driver.findElement(By.xpath(
				".//*[@id='tab2']/tbody/tr/td[3][contains(text(),'NOTIFICATION')]/following::td[1][contains(text(),'BECNOTIFICATION')]/following::td[4][contains(text(),'SUCCESS')]"));
		return element;
	}

//BECNOTIFICATION_RELATED

	public static List<WebElement> getAllBECS(WebDriver driver) {
		List<WebElement> list = driver.findElements(By.xpath("//*[@id='tab2']/tbody/tr/td[11]"));
		return list;
	}

	public static List<WebElement> getAllNotifications(WebDriver driver) {
		List<WebElement> list = driver.findElements(By.xpath("//*[@id='tab2']/tbody/tr/td/span/span"));
		return list;
	}

	public static WebElement getBECTitle(WebDriver driver, int i) {
		element = driver.findElement(By.xpath("//*[@id='rawData1_" + i + "']/div/div/div[1]/span"));
		return element;
	}

	public static WebElement getBECTitleByRowNum(WebDriver driver, int i) {
		element = driver.findElement(By.xpath(".//*[@id='rawData1_" + i + "']/div/div/div[1]/span"));
		return element;
	}

	public static WebElement getBECContentByRowNum(WebDriver driver, int i) {
		element = driver.findElement(By.xpath(".//*[contains(@id,'rawData1_" + i + "')]//pre"));
		return element;
	}

	public static WebElement viewCloseByRowNum(WebDriver driver, int i) {
		element = driver
				.findElement(By.xpath(".//*[@id='rawData1_" + i + "']//button[@class='close'][@type='button']"));
		return element;
	}

	public static WebElement getBECContent(WebDriver driver, int i) {
		// element= driver.findElement(By.xpath(".//*[contains(@id,'rawData')]//pre"));
		element = driver.findElement(By.xpath(".//*[@id='rawData1_" + i + "']/div/div/div[2]/pre"));
		return element;
	}

	public static WebElement viewClose(WebDriver driver, int i) {
		// element=
		// driver.findElement(By.xpath("//*[@id='rawData1_"+i+"']/div/div/div[1]/button"));
		element = driver
				.findElement(By.xpath("//div[@id='rawData1_" + i + "']//button[@class='close'][contains(text(),'�')]"));
		return element;
	}

	public static WebElement getOFACResSuccessStatus(WebDriver driver) {
		element = driver.findElement(By.xpath(
				".//*[@id='tab2']/tbody/tr/td[3][contains(text(),'RESPONSE')]/following::td[1][contains(text(),'OFAC')]/following::td[4][contains(text(),'SUCCESS')]"));
		return element;
	}

	public static WebElement getInsidByPmtId(WebDriver driver, String pmtId) {
		element = driver.findElement(By.xpath(
				"//span[contains(text(),'" + pmtId + "')]/preceding::span[1][@data-original-title='Instruction ID']"));
		return element;
	}

	public static WebElement getInsIdPaymentsSec(WebDriver driver, String InsId) {
		element = driver.findElement(By.xpath("//span[contains(text(),'" + InsId + "')]"));
		return element;
	}

	public static WebElement getPmtIdByInsId(WebDriver driver, String InsId) {
		element = driver.findElement(By
				.xpath("//span[contains(text(),'" + InsId + "')]/following::span[@data-original-title='Payment ID']"));
		return element;
	}

	public static WebElement paymentConfirmationButton(WebDriver driver) {
		element = driver.findElement(By.xpath("//button//span[contains(text(),'Payment Confirmation')]"));
		return element;
	}

//Payment Confirmation Pop-Up window
	public static void confStatusDrpdwn(WebDriver driver, String value) {
		Select sel = new Select(driver.findElement(By.xpath("//select[@id='Confirmation Status']")));
		sel.selectByVisibleText(value);

	}

//<ROF button visibility>
	public static void ROFReasonCodes(WebDriver driver, String value) {
		Select sel = new Select(driver.findElement(By.xpath("//select[@id='Reason Codes']")));
		sel.selectByVisibleText(value);

	}

	public static void RFPReasonTypes(WebDriver driver, String value) {
		Select sel = new Select(driver.findElement(By.xpath("//select[@id='Response Type']")));
		sel.selectByVisibleText(value);

	}

	public static void RROFReasonCodes(WebDriver driver, String value) {
		Select sel = new Select(driver.findElement(
				By.xpath("//select[@id='Accept Or Reject ROF request'][@name='AcceptOrRejectROFRequest']")));
		sel.selectByVisibleText(value);

	}

	public static void RROFRejectionReasonCodes(WebDriver driver, String value) {
		Select sel = new Select(
				driver.findElement(By.xpath("//select[@id='Reason of Rejection'][@name='ReasonOfRejection']")));
		sel.selectByVisibleText(value);

	}

	public static void RRFPRejectionReasonCodes(WebDriver driver, String value) {
		Select sel = new Select(
				driver.findElement(By.xpath("//select[@id='Rejection Reason'][@name='RejectionReason']")));
		sel.selectByVisibleText(value);

	}

	public static WebElement ROFSubmitButton(WebDriver driver) {
		element = driver.findElement(By.xpath("//button[contains(text(),'Submit ')]"));
		return element;
	}

	public static WebElement RROFSubmitButton(WebDriver driver) {
		element = driver.findElement(By.xpath("//button[contains(text(),'Submit ')]"));
		return element;
	}

//</ROF button visibility>
//After RFI button visibility

	public static void RFI_MissingInfoCodes(WebDriver driver, String value) {
		Select sel = new Select(driver.findElement(By.xpath(".//*[@id='Missing Information Codes']")));
		sel.selectByVisibleText(value);
	}

//<After RRFI button visibility>
	public static void RRFIInfoAvailable(WebDriver driver, String value) {
		// driver.findElement(By.xpath(".//*[@id='select2-Information
		// Available-container']")).click();
		// driver.findElement(By.xpath("html/body/span/span/span[1]/input")).sendKeys(value);
		// driver.findElement(By.xpath("html/body/span/span/span[1]/input")).sendKeys(Keys.ENTER);

		// Select sel = new
		// Select(driver.findElement(By.xpath(".//*[@id='select2-Information
		// Available-container']")));
		Select sel = new Select(driver.findElement(By.xpath(".//*[@id='Information Available']")));
		sel.selectByVisibleText(value);

	}

	public static void RRFIParentId(WebDriver driver) {

		// WebElement element=driver.findElement(By.xpath(".//*[@id='select2-Parent
		// ID-container']"));
		WebElement element = driver.findElement(By.xpath(".//*[@id='Parent ID']"));
		Select sel = new Select(element);

		sel.selectByIndex(1);

	}

	public static WebElement RRFIReasonOfRejection(WebDriver driver) {

		WebElement element = driver.findElement(
				By.xpath(".//*[@id='Reason of Rejection/Remittance Information'][@name='ReasonOfRejection']"));
		return element;

	}

	public static WebElement RRFISubmitButton(WebDriver driver) {
		element = driver.findElement(By.xpath("//button[contains(text(),'Submit ')]"));
		return element;
	}

	public static WebElement RFISubmitButton(WebDriver driver) {
		element = driver.findElement(By.xpath("//button[contains(text(),'Submit ')]"));
		return element;
	}

	public static WebElement SubmitButton(WebDriver driver) {
		element = driver.findElement(By.xpath("//button[contains(text(),'Submit ')]"));
		return element;
	}

//</After RRFI button visibility>

	public static WebElement pmtConfSubmitButton(WebDriver driver) {
		element = driver.findElement(By.xpath("//button[contains(text(),'Submit ')]"));
		return element;
	}

	public static WebElement generateROFButton(WebDriver driver) {
		element = driver.findElement(By.xpath("//button/span[contains(text(),'Generate ROF')]"));
		return element;
	}

	/*
	 * public static WebElement generateRROFButton(WebDriver driver) { element=
	 * driver.findElement(By.xpath("//button/span[contains(text(),'Generate RROF')]"
	 * )); return element; }
	 */

	public static WebElement generateRROFButton(WebDriver driver) {
		element = driver.findElement(By.xpath("//span[.='Generate RROF']/ancestor::button"));
		return element;
	}

	public static WebElement generateRFIButton(WebDriver driver) {
		element = driver.findElement(By.xpath("//button/span[.='Generate RFI']"));
		return element;
	}

	public static WebElement PaymentACK(WebDriver driver) {
		element = driver.findElement(By.xpath("//button/span[.='Payment ACK']"));
		return element;
	}

	public static WebElement PaymentACK_AddlInfo(WebDriver driver) {
		element = driver.findElement(By.xpath(".//*[@id='Additional Information'][@name='AdditionalInformation']"));
		return element;
	}

	public static WebElement generateRRFIButton(WebDriver driver) {
		element = driver.findElement(By.xpath("//button/span[.='Generate RRFI']"));
		return element;
	}

	public static WebElement generateRRFPButton(WebDriver driver) {
		element = driver.findElement(By.xpath("//button/span[contains(text(),'Generate RRFP')]"));
		return element;
	}

	public static WebElement selectPaymentId(WebDriver driver, String pmtId) {
		element = driver
				.findElement(By.xpath("//span[contains(text(),'" + pmtId + "')][@data-original-title='Payment ID']"));
		return element;
	}

//Downloading the files
	public static WebElement downloadOutputFile(WebDriver driver) {
		element = driver.findElement(By.xpath(
				"//*[@id='tab3']/tbody/tr/td[3][contains(text(),'Payment Instruction')]/following::td[2][contains(text(),'OUT')]/following::td[3]//span"));
		return element;
	}

	public static WebElement getStatusSWIFTRequestforInformation(WebDriver driver) {
		element = driver.findElement(By.xpath(
				"//*[@id='tab3']/tbody/tr/td[3][contains(text(),'SWIFT Request for Information')]/following::td[2][contains(text(),'OUT')]/following::td[4]//p"));
		return element;
	}

	public static WebElement getGroupRefIdSWIFTRequestforInformation(WebDriver driver) {
		element = driver.findElement(By.xpath(
				"//*[@id='tab3']/tbody/tr/td[3][contains(text(),'SWIFT Request for Information')]/following::td[2][contains(text(),'OUT')]/preceding::td[4]"));
		return element;
	}

	public static WebElement getStatusSWIFTRequestforReturnOfFunds(WebDriver driver) {
		element = driver.findElement(By.xpath(
				"//*[@id='tab3']/tbody/tr/td[3][text()='Return of Funds']/following::td[2][contains(text(),'OUT')]/following::td[4]//p"));
		return element;
	}

	public static WebElement getStatusSWIFTResponceforInformation(WebDriver driver) {
		element = driver.findElement(By.xpath(
				"//*[@id='tab3']/tbody/tr/td[3][contains(text(),'SWIFT Response to Request for Information')]/following::td[2][contains(text(),'OUT')]/following::td[4]//p"));
		return element;
	}

	public static WebElement getStatusSWIFTResponceforInformationBasedOnSource(WebDriver driver) {
		element = driver.findElement(By.xpath(
				"//*[@id='tab3']/tbody/tr/td[4][contains(text(),'GSBI_INTERBANK_SWIFTManualRROF_Tr')]/following::td[1][contains(text(),'OUT')]/following::td[4]//p"));
		return element;
	}

	public static WebElement downloadOutputFileMT103(WebDriver driver, String messageType) {
		element = driver.findElement(By.xpath("//*[@id='tab3']/tbody/tr/td[3][contains(text(),'" + messageType
				+ "')]/following::td[2][contains(text(),'OUT')]/following::td[3]//span"));
		return element;
	}

	public static WebElement downloadRFPACK(WebDriver driver) {
		element = driver.findElement(By.xpath(
				"//*[@id='tab3']/tbody/tr/td[.='RequestForPayment-Ack']/following::td[2][contains(text(),'OUT')]/following::td[3]//span//span"));
		return element;
	}

	public static WebElement downloadISOCAMT054(WebDriver driver) {
		element = driver.findElement(By.xpath(
				"//*[@id='tab3']/tbody/tr/td[3][contains(text(),'ISOCAMT054')]/following::td[2][contains(text(),'OUT')]/following::td[3]//span//span"));
		return element;
	}

	public static WebElement downloadProprietaryAck(WebDriver driver) {
		element = driver.findElement(By.xpath(
				"//*[@id='tab3']/tbody/tr/td[3][.='Proprietary Acknowledgement']/following::td[2][contains(text(),'OUT')]/following::td[3]//span//span"));
		return element;
	}

	public static WebElement downloadRFIOutput(WebDriver driver) {
		element = driver.findElement(By.xpath(
				"//*[@id='tab3']/tbody/tr/td[.='Request for Information']/following::td[2][contains(text(),'OUT')]/following::td[3]//span//span"));
		return element;
	}

	public static WebElement downloadRRFPOutput(WebDriver driver) {
		element = driver.findElement(By.xpath(
				"//*[@id='tab3']/tbody/tr/td[.='Response to Request for Payment']/following::td[2][contains(text(),'OUT')]/following::td[3]//span//span"));
		return element;
	}

	public static WebElement downloadRRFIOutput(WebDriver driver) {
		element = driver.findElement(By.xpath(
				"//*[@id='tab3']/tbody/tr/td[.='Response to Request for Information']/following::td[2][contains(text(),'OUT')]/following::td[3]//span//span"));
		return element;
	}

	public static WebElement downloadRROFOutput(WebDriver driver) {
		element = driver.findElement(By.xpath(
				"//*[@id='tab3']/tbody/tr/td[.='Response to Return of Funds']/following::td[2][contains(text(),'OUT')]/following::td[3]//span//span"));
		return element;
	}

	public static WebElement downloadRRFP(WebDriver driver) {
		element = driver.findElement(By.xpath(
				"//*[@id='tab3']/tbody/tr/td[.='Response to Return of Funds']/following::td[2][contains(text(),'OUT')]/following::td[3]//span//span"));
		return element;
	}

	public static WebElement downloadROFOutput(WebDriver driver) {
		element = driver.findElement(By.xpath(
				"//*[@id='tab3']/tbody/tr/td[.='Return of Funds']/following::td[2][contains(text(),'OUT')]/following::td[3]//span//span"));
		return element;
	}

	public static WebElement downloadROFConfOutput(WebDriver driver) {
		element = driver.findElement(By.xpath(
				"//*[@id='tab3']/tbody/tr/td[.='ACK to CIACAMT056']/following::td[2][contains(text(),'OUT')]/following::td[3]//span//span"));
		return element;
	}

	public static WebElement downloadUSRTPGPACS002Output(WebDriver driver) {
		element = driver.findElement(By.xpath(
				"//*[@id='tab3']/tbody/tr/td[.='US-RTPG-PACS002']/following::td[2][contains(text(),'OUT')]/following::td[3]//span//span"));
		return element;
	}

	public static WebElement getErrorMsg(WebDriver driver) {
		element = driver.findElement(By.xpath("//div[@class='col-md-12 ng-scope']//pre[@class='ng-binding']"));
		return element;
	}

	public static WebElement getAlertMsg(WebDriver driver) {
		element = driver
				.findElement(By.xpath("//*[@class='animated bounce bold ng-binding ng-scope alert alert-danger']"));
		return element;
	}

	public static WebElement checkStatusExtComm(WebDriver driver) {
		element = driver.findElement(By.xpath(
				"//*[@id='tab3']/tbody/tr/td[.='Return of Funds']/following::td[2][contains(text(),'OUT')]/following::td[4]"));
		return element;
	}

	public static WebElement checkROFStatusExtComm(WebDriver driver) {
		element = driver.findElement(By.xpath(
				"//*[@id='tab3']/tbody/tr/td[.='Return of Funds']/following::td[2][contains(text(),'OUT')]/following::td[4]"));
		return element;
	}

	public static WebElement checkRFIStatusExtComm(WebDriver driver) {
		element = driver.findElement(By.xpath(
				"//*[@id='tab3']/tbody/tr/td[.='Request for Information']/following::td[2][contains(text(),'IN')]/following::td[4]"));
		return element;
	}

	public static WebElement checkOUTRFIStatusExtComm(WebDriver driver) {
		element = driver.findElement(By.xpath(
				"//*[@id='tab3']/tbody/tr/td[.='Request for Information']/following::td[2][contains(text(),'OUT')]/following::td[4]"));
		return element;
	}

	public static WebElement checkRRFIStatusExtComm(WebDriver driver) {
		element = driver.findElement(By.xpath(
				"//*[@id='tab3']/tbody/tr/td[.='Response to Request for Information']/following::td[2][contains(text(),'OUT')]/following::td[4]"));
		return element;
	}

	public static WebElement checkIN_RRFIStatusExtComm(WebDriver driver) {
		element = driver.findElement(By.xpath(
				"//*[@id='tab3']/tbody/tr/td[.='Response to Request for Information']/following::td[2][contains(text(),'IN')]/following::td[4]"));
		return element;
	}

	public static WebElement check_IN_ROFStatusExtComm(WebDriver driver) {
		element = driver.findElement(By.xpath(
				"//*[@id='tab3']/tbody/tr/td[.='Return of Funds']/following::td[2][contains(text(),'IN')]/following::td[4]"));
		return element;
	}

	public static WebElement check_OUT_CAMT35Status(WebDriver driver) {
		element = driver.findElement(By.xpath(
				"//*[@id='tab3']/tbody/tr/td[.='Proprietary Acknowledgement']/following::td[2][contains(text(),'OUT')]/following::td[4]"));
		return element;
	}

	public static WebElement check_CTOUT_CAMT35Status(WebDriver driver) {
		element = driver.findElement(By.xpath(
				"//*[@id='tab3']/tbody/tr/td[.='Proprietary Acknowledgement']/following::td[2][contains(text(),'IN')]/following::td[4]"));
		return element;
	}

	// REMT Related
	public static WebElement check_IN_REMTStatusExtComm(WebDriver driver) {
		element = driver.findElement(By.xpath(
				"//*[@id='tab3']/tbody/tr/td[.='Remittance Advice']/following::td[2][contains(text(),'IN')]/following::td[4]"));
		return element;
	}

//cxc related

	public static WebElement check_ECHO_CONF_Status(WebDriver driver) {
		element = driver.findElement(By.xpath(
				"//*[@id='tab3']/tbody/tr/td[.='Confirmation 1']/following::td[2][contains(text(),'IN')]/following::td[4]"));
		return element;
	}

	public static WebElement check_CONF_2_Status(WebDriver driver) {
		element = driver.findElement(By.xpath(
				"//*[@id='tab3']/tbody/tr/td[.='Confirmation 2']/following::td[2][contains(text(),'IN')]/following::td[4]"));
		return element;
	}

	public static WebElement paymentModulePlus(WebDriver driver) {
		element = driver.findElement(By.xpath("//li[@id='PaymentModule']//i[@class='fa fa-plus']"));
		return element;
	}

	public static WebElement homeModule(WebDriver driver) {
		element = driver.findElement(By.xpath("//span[contains(text(),'Home')]"));
		return element;
	}

	public static List<WebElement> getAllDownloadFiles(WebDriver driver) {
		List<WebElement> list = driver.findElements(By.xpath("//tr//td[8]//span[1]"));
		return list;
	}

	public static WebElement collapsePaymentsModule(WebDriver driver) {
		By collapsePaymentsModule = By.xpath("//*[@id='PaymentModule']/a/i[2]");
		return driver.findElement(collapsePaymentsModule);
	}

	public static WebElement showAdvancedSearchButton(WebDriver driver) {
		By adSearchBtn = By.xpath(".//button[@class='btn btnStyle']//span[@class='ng-binding ng-scope']/span[text()='Show Advanced Search']");
		element = driver.findElement(adSearchBtn);
		return element;
	}

	public static WebElement receivedDateStartTextField(WebDriver driver) {
		By receivedDateStartTextField = By.xpath("//input[@id='ReceivedDateStart']");
		return driver.findElement(receivedDateStartTextField);
	}

	public static WebElement receivedDateEndTextField(WebDriver driver) {
		By receivedDateStartTextField = By.xpath("//input[@id='ReceivedDateEnd']");
		return driver.findElement(receivedDateStartTextField);
	}

	public static WebElement partyServiceAssociationCodeSearchBox(WebDriver driver) {
		By partyServiceAssociationCodeSearchBox = By
				.xpath("//label[contains(text(),'PSA Code')]/../../div[2]//span/ul/li//input");
		return driver.findElement(partyServiceAssociationCodeSearchBox);
	}

	public static WebElement instructionIdSearchBox(WebDriver driver) {
		By instructionIdSearchBox = By.xpath("//div[@class='col-md-6 ng-scope']//input[@placeholder='Instruction ID']");
		return driver.findElement(instructionIdSearchBox);
	}

	public static WebElement partyServiceAssociationCodeName(WebDriver driver) {
		By partyServiceAssociationCodeSearchBox = By
				.xpath("//*[@id='select2-PartyServiceAssociationCode-ga-results']/li");
		return driver.findElement(partyServiceAssociationCodeSearchBox);
	}

	public static WebElement methodOfPaymentSearchBox(WebDriver driver) {
		By methodOfPaymentSearchBox = By.xpath("//label[contains(text(),'MOP')]/../../div[2]//span/ul/li//input");
		return driver.findElement(methodOfPaymentSearchBox);
	}

	public static WebElement paymentStatusSearchBox(WebDriver driver) {
		By paymentStatusSearchBox = By
				.xpath("//label[contains(text(),'Payment Status')]/../../div[2]//span/ul/li//input");
		return driver.findElement(paymentStatusSearchBox);
	}

	public static WebElement searchButton(WebDriver driver) {
		By searchButton = By.xpath("//button[@id='AdSearchBtn']");
		WaitLibrary.waitForElementPresence(driver, 30, searchButton);
		return driver.findElement(searchButton);
	}

	public static WebElement sPSACodeSearchedResult(WebDriver driver) {
		By sPSACodeSearchedResult = By.xpath("//div[@class='row']//td[7]//span");
		return driver.findElement(sPSACodeSearchedResult);
	}

	public static WebElement sTableRow(WebDriver driver) {
		By sTableRow = By
				.xpath("//div//table[@class='table table-striped table-bordered smallTable floatThead stickyheader']");
		return driver.findElement(sTableRow);
	}

	public static WebElement paymentStatusCol(WebDriver driver) {
		By clickOutside = By.xpath("//div[@class='row']//tr[1]//td[13]");
		return driver.findElement(clickOutside);
	}

	public static WebElement sOriginalMassageSubFunctionCol(WebDriver driver) {
		By sPSACodeSearchedResult = By.xpath("//div[@class='row']//td[last()-1]/span");
		return driver.findElement(sPSACodeSearchedResult);
	}

	public static WebElement instructionIDInSearchedTable(WebDriver driver) {
		By instructionIDInSearchedTable = By.xpath("//div[@class='row']//td[3]//span");
		return driver.findElement(instructionIDInSearchedTable);
	}

	public static WebElement instructionIDInPayment(WebDriver driver, String sInstrId) {
		By instructionIDInPayment = By.xpath("//span//a[text()='" + sInstrId + "']");
		return driver.findElement(instructionIDInPayment);
	}

	public static WebElement noSearchRecord(WebDriver driver) {
		By noSearchRecord = By.xpath("//div[contains(@class,'countbar-title')]//div[contains(@class,'ng-binding')]");
		return driver.findElement(noSearchRecord);
	}

	public static WebElement rofIMADId(WebDriver driver) {
		By rofIMADId = By.xpath("//div//tbody//tr[last()]//td[1]//a");
		return driver.findElement(rofIMADId);
	}

	public static WebElement getStatusProcessed(WebDriver driver) {
		By getStatusProcessed = By.xpath("//div//tbody//tr[last()]//td[last()]//p");
		return driver.findElement(getStatusProcessed);
	}

	public static WebElement rofIMADStatus_WaitInRROF(WebDriver driver) {
		By rofIMADStatus_WaitInRROF = By.xpath("//div//tbody//tr[last()-1]//td[last()]//p");
		return driver.findElement(rofIMADStatus_WaitInRROF);
	}

	public static WebElement rofIMADStatus_Completed(WebDriver driver) {
		By rofIMADStatus_Completed = By.xpath("//div//tbody//tr[last()-1]//td[last()]//p");
		return driver.findElement(rofIMADStatus_Completed);
	}

	public static WebElement status_CompletedRFI(WebDriver driver) {
		By rofIMADStatus_Completed = By.xpath("//div//tbody//tr[last()-3]//td[last()]//p");
		return driver.findElement(rofIMADStatus_Completed);
	}

	public static WebElement statementMatching_WaitForMatchStatus(WebDriver driver, String sAccounId) {
		By waitForMatch = By.xpath("//table[@id='tab4']//td[@class='ng-binding'][contains(text(),'" + sAccounId
				+ "')]/following-sibling::td[last()]");
		return driver.findElement(waitForMatch);
	}

	public static WebElement paymentDetailsStatus(WebDriver driver) {
		By paymentDetailsStatus = By
				.xpath("(//div[@class='col-md-4 lineHeight35 ng-binding']//span/following-sibling::text()[1])[5]");
		return driver.findElement(paymentDetailsStatus);
	}

	public static WebElement sDebitCreditIndicator(WebDriver driver, String sAccountId) {
		By sDebitCreditIndicator = By.xpath("//tr[@class='ng-scope']//td[@class='ng-binding'][text()='" + sAccountId
				+ "']/following-sibling::td[7]");
		return driver.findElement(sDebitCreditIndicator);
	}

	public static WebElement sDebitCreditIndicatorReversal(WebDriver driver, String sAccountId) {
		By sDebitCreditIndicator = By.xpath("(//tr[@class='ng-scope']//td[@class='ng-binding'][text()='" + sAccountId
				+ "']/following-sibling::td[7])[2]");
		return driver.findElement(sDebitCreditIndicator);
	}

	public static WebElement getStatusWaitInRRFI(WebDriver driver) {
		element = driver.findElement(By.xpath(".//tbody//tr[last()-1]//td[last()]//p"));
		return element;
	}

	public static WebElement getStatusReqForPaymentDRC(WebDriver driver) {
		element = driver
				.findElement(By.xpath("//td[contains(text(),'RequestForPayment')]/following-sibling::td[last()]"));
		return element;
	}

	public static WebElement getInstructionID(WebDriver driver) {
		element = driver.findElement(By.xpath("//span[@class='caption-helper ng-binding']//a[@class='ng-binding']"));
		return element;
	}

	public static WebElement btnProccessAsPayment(WebDriver driver) {
		element = driver.findElement(By.xpath("//span[contains(text(),'Process as Payment')]"));
		return element;
	}

	// ----------------------------------------------Deepak---------------------------------------------------------------

	/*
	 * public static WebElement getShowAdvanceSearchButton(WebDriver driver) {
	 * return driver.findElement(By.
	 * xpath("//span[contains(text(),'Show Advanced Search')]/../parent::button"));
	 * }
	 */

	public static WebElement getPaymentRefTextBox(WebDriver driver) {
		return driver.findElement(By.xpath("//label[contains(text(),'Payment Reference')]/../../div[2]/input"));
	}

	public static WebElement paymentEltemBox(WebDriver driver) {
		By paymentEltemBox = By.xpath("//label[contains(text(),'Payment Reference')]/../../div[2]/input");
		return driver.findElement(paymentEltemBox);
	}
	
	public static WebElement origInstId(WebDriver driver) {
		By origInstId = By.xpath("//label[contains(text(),'Original Instruction ID')]/../../div[2]/input");
		WaitLibrary.waitForElementPresence(driver, 30, origInstId);
		return driver.findElement(origInstId);
	}

	public static WebElement getShowAdvanceSearchButton(WebDriver driver) {
		return driver.findElement(By.xpath("//span[contains(text(),'Show Advanced Search')]"));
	}

//	public static WebElement getReturnOfFundWithCompleteStatus (WebDriver driver) {
//	    return driver.findElement(By.xpath("//td[@class='ng-binding'][text()='Return of Funds']/parent::tr//p[text()='COMPLETED']"));
//	}

	public static WebElement statementDetails(WebDriver driver) {

		element = driver.findElement(By.xpath("//i[@class='fa fa-money']"));
		return element;
	}

	public static WebElement linkedStatementID(WebDriver driver) {

		element = driver.findElement(By.xpath("(//a[@class='bold cursorPointer ng-binding'])[2]"));
		return element;
	}

	public static WebElement linkedMessageTab(WebDriver driver) {
		By waitForMatch = By.xpath("//b[text()='Linked Messages']");
		return driver.findElement(waitForMatch);
	}

	public static WebElement linkedMessageID(WebDriver driver) {
		By waitForMatch = By.xpath("(//a[@class='bold cursorPointer ng-binding'])[2]");
		return driver.findElement(waitForMatch);
	}

	public static WebElement linkedStatementIDCredit(WebDriver driver) {
		By waitForMatch = By.xpath("(//a[@class='bold cursorPointer ng-binding'])[5]");
		return driver.findElement(waitForMatch);
	}
	
	 public static WebElement destinationMessageType1(WebDriver driver) {
	        element = driver.findElement(By.xpath("(//div[8])[1]"));
	        return element;
	    }
	
	//------------------------------------------------End------------------------------------------------------------------


	// ------------------------------------------------End------------------------------------------------------------------


	public static WebElement getServiceID(WebDriver driver) {
		By rofIMADStatus_Completed = By
				.xpath("//div[@class='panel-heading fileListStyle']//span[@tooltip='Payment ID']");
		return driver.findElement(rofIMADStatus_Completed);
	}

	public static WebElement getStatusProcessAsPaymentButton(WebDriver driver) {
		By getProcesseAsPmt = By.xpath("//span[text()='Process as Payment']");
		return driver.findElement(getProcesseAsPmt);
	}

	public static WebElement getReturnOfFundWithCompleteStatus(WebDriver driver) {
		return driver.findElement(By.xpath(
				"//*[@id='tab3']/tbody/tr/td[3][contains(text(),'Return of Funds')]/following::td[2][contains(text(),'IN')]/following::td[4]//p"));//// //td[@class='ng-binding'][text()='Return
																																					//// of
																																					//// //Funds']/parent::tr//p[text()='COMPLETED']
	}

	public static WebElement statementMatching_WaitForMatchStatus(WebDriver driver) {
		By waitForMatch = By.xpath(
				"(//tr[@class='ng-scope']//td[@class='ng-binding'][contains(text(),'NOS')]/following-sibling::td[last()])[2]");
		return driver.findElement(waitForMatch);
	}

	public static WebElement getReturnOfFundWithWaitOutRROFStatus(WebDriver driver) {
		return driver.findElement(By.xpath(
				"//*[@id='tab3']/tbody/tr/td[3][contains(text(),'Return of Funds')]/following::td[2][contains(text(),'OUT')]/following::td[4]//p"));//// ///td[@class='ng-binding'][text()='Return
																																					//// of
																																					//// //Funds']/parent::tr//p[text()='WAIT_OUT_RROF']
	}

	public static WebElement destinationMessageType(WebDriver driver) {
		element = driver.findElement(By.xpath("(//div[8])[1]"));
		return element;
	}

}
