package com.gs.objectRepo;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.gs.utilities.WaitLibrary;

public class Login {
	static By username = By.id("username");
	static By password = By.id("password");
	static By loginButton = By.xpath("//button[contains(text(),'Login')]");

	public static void launch(WebDriver driver, String baseURL) throws Exception {
		driver.get(baseURL);
		driver.manage().window().maximize();
		WaitLibrary.waitForElementPresence(driver, 30, loginButton);
	}

	public static WebElement usrName(WebDriver driver) {
		WaitLibrary.waitForElementPresence(driver, 30, username);
		return driver.findElement(username);
	}

	public static WebElement password(WebDriver driver) {
		WaitLibrary.waitForElementPresence(driver, 30, password);
		return driver.findElement(password);
	}

	public static WebElement loginButton(WebDriver driver) {
		WaitLibrary.waitForElementPresence(driver, 30, loginButton);
		return driver.findElement(loginButton);
	}

//public static WebElement usrName(WebDriver driver)
//{
//	element = driver.findElement(By.id("username"));
//	return element;
//}
//public static WebElement password(WebDriver driver)
//{
//	element = driver.findElement(By.id("password"));
//	return element;
//}
//public static WebElement loginButton(WebDriver driver)
//{
//	element = driver.findElement(By.xpath("//button[contains(text(),'Login')]"));
//	return element;
//}

}
