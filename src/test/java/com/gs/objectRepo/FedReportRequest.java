package com.gs.objectRepo;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class FedReportRequest 
{
	
	public static WebElement element=null;
	
	//Locating 'FED Report Request' Tab
	public static WebElement fedReportRequest(WebDriver driver)
	{
		By fedReportRequest = By.xpath("//span[contains(text(),'FEDReportRequest')]");
		return driver.findElement(fedReportRequest);
	}
	
	//Locating 'Ad hoc FED Report Request' Tab
    public static WebElement adHocfedReportRequest(WebDriver driver)
	{
	  By adHocfedReportRequest = By.xpath("	//span[contains(text(),'Ad hoc FED Report Request')]");
	  return driver.findElement(adHocfedReportRequest);
    }
		
    public static void mopFamily(WebDriver driver, String option) throws InterruptedException
   	{
    	element = driver.findElement(By.xpath("//select[@id='mop']"));
    	Select sel = new Select(element);
    	Thread.sleep(2000);
    	sel.selectByVisibleText(option);
     }	
    
    public static void reportType(WebDriver driver, String option) throws InterruptedException
   	{
    	element = driver.findElement(By.xpath("//select[@id='ReportType']"));
    	Select sel = new Select(element);
    	Thread.sleep(2000);
    	sel.selectByVisibleText(option);
     }
    
    public static void balanceType(WebDriver driver, String option) throws InterruptedException
   	{
    	
    	element = driver.findElement(By.xpath("//div//div//div//div[3]//div[1]//select[1]"));
    	element.sendKeys(option);
		Thread.sleep(2000);
		element.sendKeys(Keys.ENTER);
     }
    
    public static void inquiryABA(WebDriver driver, String option) throws InterruptedException
   	{
    	
    	element = driver.findElement(By.xpath("//div[4]//div[1]//select[1]"));
    	element.sendKeys(option);
		Thread.sleep(2000);
		element.sendKeys(Keys.ENTER);
     }
    
    public static void endpoint_ID(WebDriver driver, String option) throws InterruptedException
   	{
    	
    	element = driver.findElement(By.xpath("//div//div//div//div[3]//div[1]//select[1]"));
    	element.sendKeys(option);
		Thread.sleep(2000);
		element.sendKeys(Keys.ENTER);
     }
    
    public static void cycleDate(WebDriver driver, int index) throws InterruptedException
   	{
    	
    	element = driver.findElement(By.xpath("//div[4]//div[1]//select[1]"));
    	Select sel = new Select(element);
    	Thread.sleep(2000);
    	sel.selectByIndex(index);
     }
    
    public static void traffic_Type_FTI051(WebDriver driver, String option) throws InterruptedException
   	{
    	element = driver.findElement(By.xpath("//div[5]//div[1]//select[1]"));
    	element.sendKeys(option);
		Thread.sleep(2000);
		element.sendKeys(Keys.ENTER);
     } 
    
    public static void traffic_Type(WebDriver driver, String option) throws InterruptedException
   	{
    	
    	element = driver.findElement(By.xpath("//div[4]//div[1]//select[1]"));
    	element.sendKeys(option);
		Thread.sleep(2000);
		element.sendKeys(Keys.ENTER);
     }
    
    public static WebElement error_CD(WebDriver driver) 
   	{
    	
    	By error_CD = By.xpath("//input[@type='text']");
    	return driver.findElement(error_CD);
     }
    
    public static WebElement text_NoGapFund(WebDriver driver) 
   	{
    	
    	By noGapFound = By.xpath("//*[text()='No Gap found']");
    	return driver.findElement(noGapFound);
     }
    
    public static WebElement sequenceNo(WebDriver driver) 
   	{
    	
    	By sequenceNo = By.xpath("//tr[1]//td[5]");
    	return driver.findElement(sequenceNo);
     }
    
    public static WebElement startSeq(WebDriver driver) 
   	{
    	
    	By startSeq = By.xpath("//div[6]//div[1]//input[1]");
    	return driver.findElement(startSeq);
     }
    
  
  
    
    public static WebElement requestReport(WebDriver driver) 
   	{
    	By requestReport = By.xpath("//input[@value='Request Report']");
    	return driver.findElement(requestReport);
     }
    
    public static WebElement adddException(WebDriver driver) 
   	{
    	By requestReport = By.xpath("//input[@value='Add Exception']");
    	return driver.findElement(requestReport);
     }

  
	
}
