package com.gs.objectRepo;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class InitiateTransaction {
public static WebElement element=null;


public static WebElement goToInitiateTxn(WebDriver driver)
{
	element = driver.findElement(By.xpath("//span[.='Initiate Transaction']"));
	return element;
	
}


public static void Party(WebDriver driver,String value) throws Exception
{
	driver.findElement(By.xpath("//div[@class='collapse in']//span[@id='select2-PartyCode-container']")).click();
	Thread.sleep(2000);
	driver.findElement(By.xpath("//div[@class='collapse in']//span[@id='select2-PartyCode-container']//following::input[@class='select2-search__field'][@role='textbox']")).sendKeys(value);
	Thread.sleep(3000);
	driver.findElement(By.xpath("//div[@class='collapse in']//span[@id='select2-PartyCode-container']//following::input[@class='select2-search__field'][@role='textbox']")).sendKeys(Keys.ENTER);
	/*Select sel  = new Select(driver.findElement(By.xpath("//div[@class='collapse in']//span[@id='select2-PartyCode-container']")));
	sel.selectByVisibleText(value);*/
	
}


public static void service(WebDriver driver,String value)
{
    Select sel  = new Select(driver.findElement(By.xpath("//select[@name='service11'][@ng-model='service11']")));
	sel.selectByVisibleText(value);
	
}


public static void messageType(WebDriver driver,String value)
{
    Select sel  = new Select(driver.findElement(By.xpath("//select[@name='MessageType']")));
	sel.selectByVisibleText(value);
	
}



public static void PSACode(WebDriver driver,String value)
{
    Select sel  = new Select(driver.findElement(By.xpath("//div[@class='col-md-4']//select[@id='Filechannel']")));
	sel.selectByVisibleText(value);
	
}


public static void tmp(WebDriver driver,String value)
{
    Select sel  = new Select(driver.findElement(By.xpath("")));
	sel.selectByVisibleText(value);
	
}

public static void branch(WebDriver driver,String value)
{
    Select sel  = new Select(driver.findElement(By.xpath("//div[@class='col-md-3']//select[@name='Branch']")));
	sel.selectByVisibleText(value);
	
}

public static void productsSupported(WebDriver driver,String value)
{
    Select sel  = new Select(driver.findElement(By.xpath("//select[@name='productsupported']")));
	sel.selectByVisibleText(value);
	
}


public static void valueDate(WebDriver driver,String value)
{
    Select sel  = new Select(driver.findElement(By.xpath("//input[@placeholder='Please Select Value Date'][@name='ValueDate']")));
	sel.selectByVisibleText(value);
	
}

public static void drCustPropCode(WebDriver driver,String value)
{
	WebElement element1=driver.findElement(By.xpath("//select[@name='DebtorCustomerProprietaryCode']"));
	JavascriptExecutor js = (JavascriptExecutor)driver; 
	js.executeScript("arguments[0].scrollIntoView();", element1);
	Select sel  = new Select(driver.findElement(By.xpath("//select[@name='DebtorCustomerProprietaryCode']")));
	sel.selectByVisibleText(value);
	
}

public static void pmtCurrency(WebDriver driver,String value)
{
    Select sel  = new Select(driver.findElement(By.xpath(".//*[@id='select2-PaymentCurrency-9x-container']")));
	sel.selectByVisibleText(value);
	
}


public static void amount(WebDriver driver,String value)
{
    Select sel  = new Select(driver.findElement(By.xpath("//input[@placeholder='Please Enter Amount'][@name='Amount']")));
	sel.selectByVisibleText(value);
	
}


public static void endToEndId(WebDriver driver,String value)
{
    Select sel  = new Select(driver.findElement(By.xpath("//input[@id='EndToEndId'][@name='EndToEndId']")));
	sel.selectByVisibleText(value);
	
}

/* Ordering Customer Xpath's*/

public static void ordCustAccNum(WebDriver driver,String value)
{
    Select sel  = new Select(driver.findElement(By.xpath(".//span[@id='select2-OrderingCustomer_AccountNumber-07-container']")));
	sel.selectByVisibleText(value);
	
}

public static void beneBankDetailsBIType(WebDriver driver,String value)
{
    Select sel  = new Select(driver.findElement(By.xpath(".//*[@id='select2-BankIdentifierType1-b0-container']")));
	sel.selectByVisibleText(value);
	
}


public static void beneBankDetailsBIC(WebDriver driver,String value)
{
    Select sel  = new Select(driver.findElement(By.xpath(".//*[@id='select2-BIC1-yw-container'][@title='Select the Bank Identifier Code']")));
	sel.selectByVisibleText(value);
	
}

public static WebElement beneDetailsAccNum(WebDriver driver)
{
    element=driver.findElement(By.xpath("//input[@placeholder='Please Enter Account Number'][@ng-model='Beneficiary.AccountNumber']"));
	return element;
	
}


public static WebElement beneDetailsName(WebDriver driver)
{
    element=driver.findElement(By.xpath(".//input[@name='BeneficiaryName']"));
	return element;
	
}

public static void button_saveAsTemplate(WebDriver driver,String value)
{
    Select sel  = new Select(driver.findElement(By.xpath("//input[@type='button'][@value='Save as Template and Submit']")));
	sel.selectByVisibleText(value);
}

public static void templateName(WebDriver driver,String value)
{
    Select sel  = new Select(driver.findElement(By.xpath("//input[@placeholder='Please Enter Template Name']")));
	sel.selectByVisibleText(value);
}

public static void templateStatus(WebDriver driver,String value)
{
    Select sel  = new Select(driver.findElement(By.xpath("//select[@class='form-control ng-pristine ng-untouched ng-invalid ng-invalid-required']")));
	sel.selectByVisibleText(value);
}

public static void templateEffDate(WebDriver driver,String value)
{
    Select sel  = new Select(driver.findElement(By.xpath("//input[@placeholder='Please Select Effective From Date']")));
	sel.selectByVisibleText(value);
}





	
	

}










	
	


