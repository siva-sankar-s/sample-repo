package com.gs.utilities;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.gs.objectRepo.RecInspage;
import com.gs.objectRepo.SecurityPage;
import com.gs.pages.Approvals;
import com.gs.pages.OnboardingData;
import com.gs.pages.ReceivedInstruction;
import com.gs.pages.ReceivedStatement;

public class CommonMethods extends LoginLogout{
	
	public static ArrayList<String> tabs = null;
    
    public static void searchOriginalInstructionId(String sheetName, String TestCaseName, WebDriver driver) throws Exception {

		String sStatus = "";
		String origInstrId = "";

		try {
			log = Logger.getLogger(TestCaseName);

			origInstrId = ExcelUtilities.getCellDataBySheetName(Constants.File_TestData1, sheetName, TestCaseName,
					Constants.Col_OrigInstructionId);

			log.info(sStatus + "Original instruction ID is ::::" + origInstrId);

			js = (JavascriptExecutor) driver;
			js.executeScript("arguments[0].click();", RecInspage.showAdvancedSearchButton(driver));
			
			Thread.sleep(3000);
			WebElement origInstructionId = RecInspage.origInstId(driver);
			origInstructionId.sendKeys(origInstrId, Keys.ENTER);

			RecInspage.searchButton(driver).sendKeys(Keys.ENTER);
		}

		catch (Exception e) {
			throw new Exception("Error Occured In performAdvancedSearchTest():" + e.getMessage());
		}
	}
    
    public static void clickStatementWithFileName(WebDriver driver, String fileName) throws Exception{
    	
    	try {
    		//Scroll to Element with fileName
    		System.out.println(fileName);
    		BrowserResolution.scrollToElement(driver, ReceivedStatement.selectStatementByName(driver, fileName));
    		js.executeScript("window.scrollBy(0,-120)");
    		
    		//Click Instruction Id
    		log.info("Click on instruction id"); 
    		WebElement getInsIdByFileName = ReceivedStatement.getInsIdByFileName(driver, fileName);
    		WaitLibrary.waitForElementToBeClickable(driver, getInsIdByFileName, 30).click();
    		
    		WaitLibrary.waitForAngular(driver);
    		log.info("Verifying Statement Page with Transport name");
    		WebElement TransportName = ReceivedStatement.verifyStatementnPage(driver, fileName);
    		System.out.println(TransportName);
    	}
    	catch (Exception et) {
    		throw new Exception("Error Occured In clickStatementWithFileName():" + et.getMessage());
    	}
    	
    }
    
    public static void approveStatement(WebDriver driver, String searchParams) throws Exception {
    	
    	try {
    		        	
        	tabs = new ArrayList<String>(driver.getWindowHandles());
    		driver.switchTo().window(tabs.get(1));
    		
    		WebElement clickFilterIcon = Approvals.clickFilterIcon(driver);
    		WaitLibrary.waitForElementToBeClickable(driver, clickFilterIcon, 30).click();
    		System.out.println("clicked filter icon");
    		
    		WebElement filterWithKeyword = Approvals.filterWithKeyword(driver);
    		filterWithKeyword.sendKeys(searchParams, Keys.ENTER);
    		System.out.println("Entered search params");
    			
    		WaitLibrary.waitForElementToBeClickable(driver, clickFilterIcon, 30).click();
    		System.out.println("clicked filter icon");
    		
    		WaitLibrary.waitForAngular(driver);
    		WebElement approvalData = Approvals.approvalTableData(driver, 0);
    		System.out.println(approvalData.getText());
    		approvalData.click();
    		WaitLibrary.waitForAngular(driver);
    		
    		Thread.sleep(Constants.tooshort_sleep);
    		BrowserResolution.scrollDown(driver);
    		
    		WebElement notes = Approvals.notes(driver);
    		WaitLibrary.waitForElementToBeClickable(driver, notes, 30).click();
    		notes.sendKeys(searchParams);
    		
    		BrowserResolution.scrollDown(driver);
    		WebElement ApproveBtn = Approvals.clickApproveBtn(driver);
    		WaitLibrary.waitForElementToBeClickable(driver, ApproveBtn, 30).click();
    		WaitLibrary.waitForAngular(driver);
    		System.out.println("clicked approve btn");
    		
    		driver.switchTo().window(tabs.get(0));
    	}
    	catch (Exception e) {
    		System.out.println("Approval Catch");
    		js.executeScript("arguments[0].click();", SecurityPage.security(driver));
			js.executeScript("arguments[0].click();", SecurityPage.approvals(driver));
    		driver.switchTo().window(tabs.get(0));
    		throw new Exception("Error Occured In approveStatement():" + e.getMessage());
    	}    	
    }
    
    public static String filterAccountNumDiv(String divData) throws Exception {
    	try {
    		return divData.split("\n")[0].replace("Account : ", "");
    	}
    	catch (Exception ex) {
    		throw new Exception("Error Occured In filterAccountNumDiv():" + ex.getMessage());
    	}
    }
    
    public static String filterPayMethodDiv(String divData) throws Exception {
    	try {
    		return divData.split("\n")[2].replace("Method Of Payment : ", "");
    	}
    	catch (Exception ex) {
    		throw new Exception("Error Occured In filterPayMethodDiv():" + ex.getMessage());
    	}
    }
    
    public static String orimsgDiv(String divData) throws Exception {
    	try {
    		return divData.split("\n")[4].replace("Original Message Sub Function : ", "");
    	}
    	catch (Exception ex) {
    		throw new Exception("Error Occured In orimsgDiv():" + ex.getMessage());
    	}
    }
    
    public static String readMREFDownloadFile() throws Exception {
    	String MREFValue = "";
		File directoryPath = new File(System.getProperty("user.dir")+"\\Downloads");
		//List of all files and directories
	    File filesList[] = directoryPath.listFiles();
	    BufferedReader reader = null;	    				
    	try {
    	    File fileToRead = filesList[0];
    	    reader = new BufferedReader(new FileReader(fileToRead));
    	    String line = reader.readLine();
    	    while (line != null) {
    	    	 if(line.startsWith("/MREF/")) {
    	    		 String[] data = line.split("/MREF/");
    	    		 System.out.println(data);
    	    		 MREFValue = data[1];
    	    		 break;
    	    	 }
    	    	 line = reader.readLine();
    	    }
    	    return MREFValue;
    	}catch (Exception et){
    		throw new Exception("Error Occured In readMREFDownloadFile(): " + et.getMessage());
    	}finally {
			try {
				// Closing the resources
				reader.close();
				FileUtils.cleanDirectory(new File(System.getProperty("user.dir")+"\\Downloads"));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
    }
    
    public static void cleanDownloadDir() throws Exception{
    	try {
    		FileUtils.cleanDirectory(new File(System.getProperty("user.dir")+"\\Downloads"));
    	}catch(Exception e){
    		throw new Exception("Error Occured In cleanDownloadDir(): " + e.getMessage());
    	}
    }
    
    public static File getDownloadedFile() throws Exception{
    	File directoryPath = new File(System.getProperty("user.dir")+"\\Downloads");
		//List of all files and directories
	    File filesList[] = directoryPath.listFiles();
	    try {
	    	File fileToRead = filesList[0];
		    return fileToRead;
	    }catch (Exception e) {
			throw new Exception("Error Occured In getDownloadedFile(): " + e.getMessage());
		}   
    }
    
    // Method to get OFAC data value
    public static String getOFACDataVal(String OFACData, String Field) throws Exception{
    	try {
    		if(OFACData.contains(Field)) {
    			OFACData = OFACData.replace("/", "");
        		OFACData = OFACData.replace("\r", "");
        		OFACData = OFACData.replace(" ", "");
        		OFACData = OFACData.replace("\n", "");
        		String [] splitField = OFACData.split("\\["+Field+"\\]");
        		String [] splitData = splitField[1].split("\\[");
        		return splitData[0];
    		}
    		else {
    			throw new Exception(Field + "value not found in OFAC Data");
    		}
	    }catch (Exception e) {
			throw new Exception("Error Occured In getOFACData(): " + e.getMessage());
		}
    }
    
    // Method to get payment details value from string
    public static String getPaymentDetailVal (String data, String Field) throws Exception{
    	try {
    		if(data.contains(Field)) {
    			String [] splitData = data.split(Field + " : ");
    			if(splitData.length > 1) {
    				splitData = splitData[1].split("\n");
        			return splitData[0].trim();
    			}else {
    				return "";
    			}
    		}
    		else {
    			return "";
    		}
    		
    	}catch(Exception e) {
    		throw new Exception("Error Occured In getPaymentDetailVal(): " + e.getMessage());
    	}
    }
    
    //Onboarding Transport to be suspended
    public static void getTransport(WebDriver driver, String searchParams) throws Exception {
    	
    	try {

    		WebElement clickFilterIcon = OnboardingData.clickFilterIcon(driver);
    		WaitLibrary.waitForElementToBeClickable(driver, clickFilterIcon, 30).click();
    		System.out.println("clicked filter icon");
 		
    		WebElement filterWithKeyword = OnboardingData.filterWithKeyword(driver);
    		filterWithKeyword.sendKeys(searchParams, Keys.ENTER);
    		System.out.println("Entered search params");
    		
    		WaitLibrary.waitForElementToBeClickable(driver, clickFilterIcon, 30).click();
    		System.out.println("clicked filter icon");
    		
    		WaitLibrary.waitForAngular(driver);
    		WebElement transData = OnboardingData.TransportableData(driver, 0);
    		System.out.println(transData.getText());
    		transData.click();
    		WaitLibrary.waitForAngular(driver);
    		Thread.sleep(Constants.tooshort_sleep);
    	 		
    	}
    	catch (Exception e) {
    		throw new Exception("Error Occured In suspendTransport():" + e.getMessage());
    	}    	
    }
    
    // Get Json Object from input data
    public static JsonObject getFormDataJson(String jsonPath) throws IOException {
		// parsing ".json" file & create "JsonObject"
		String jsonString = new String(Files.readAllBytes(Paths.get(jsonPath)));
		JsonObject JSONFileObj = (JsonObject) JsonParser.parseString(jsonString).getAsJsonObject();
		
		return JSONFileObj;
	}

    public static void forceRelease(WebDriver driver, String paymentID, String paymentFile, String insId) throws Exception {
    	try {
    		WebElement forceRelease = ReceivedInstruction.ForceRelease(driver);
    		WaitLibrary.waitForAngular(driver);
			WaitLibrary.waitForElementToBeClickable(driver, forceRelease, Constants.avg_explicit).click();
			WaitLibrary.waitForAngular(driver);
			
			CommonMethods.approveStatement(driver, paymentID);
    		WaitLibrary.waitForAngular(driver);
    		
    		log.info("Navigate to Received Instructions");
    		WebElement recInsTabInIn = ReceivedInstruction.recInsTab(driver);
    		BrowserResolution.scrollToElement(driver, recInsTabInIn);
    		WaitLibrary.waitForElementToBeClickable(driver, recInsTabInIn, Constants.avg_explicit).click();
    		
    		log.info("Click on List View");
    		WebElement listViewIn = ReceivedInstruction.listView(driver);
    		String listViewClassIn = WaitLibrary.waitForElementToBeVisible(driver, listViewIn, Constants.avg_explicit).getAttribute("class");

    		if (listViewClassIn.equalsIgnoreCase("btn-trans cmmonBtnColors")) {
    			WaitLibrary.waitForElementToBeClickable(driver, listViewIn, Constants.avg_explicit).click();
    		}
    		WaitLibrary.waitForAngular(driver);
    		
    		WebElement searchInstructionIn = ReceivedInstruction.searchWithInsID(driver);
    		searchInstructionIn.clear();
    		searchInstructionIn.sendKeys(insId, Keys.ENTER);
    		WaitLibrary.waitForAngular(driver);
    		
    		CommonMethods.clickStatementWithFileName(driver, paymentFile);
    		WaitLibrary.waitForAngular(driver);
			BrowserResolution.scrollDown(driver);
		
    	}catch (Exception e) {
    		throw new Exception("Error Occured In Force Release" + e.getMessage());
    	}  
  
    }
     
    //Filter Original Instruction ID
    public static String filterOriginalInsId(String divData) throws Exception {
    	try {
    		return divData.split("\n")[0].replace("OriginalInstructionID: ", "");
    	}
    	catch (Exception ex) {
    		throw new Exception("Error Occured In filterPayMethodDiv():" + ex.getMessage());
    	}
    }
    
    //Filter Instruction Status
    public static String filterInstructionStatus(String divData) throws Exception {
    	try {
    		return divData.split("\n")[0].replace("InstructionStatus: ", "");
    	}
    	catch (Exception ex) {
    		throw new Exception("Error Occured In filterPayMethodDiv():" + ex.getMessage());
    	}
    }
    
    //Filter Value Date
    public static String filterValueDate(String divData) throws Exception {
    	try {
    		return divData.split("\n")[1].replace("Value Date : ", "");
    	}
    	catch (Exception ex) {
    		throw new Exception("Error Occured In filterPayMethodDiv():" + ex.getMessage());
    	}
    }
    
    //Get Value Date and Creditor Value from downloaded ACH OUT file
    public static String verifyACHOutFile(String type, String payType) throws Exception{
    	String retVal ="";
		File directoryPath = new File(System.getProperty("user.dir")+"\\Downloads");
		//List of all files and directories
		File filesList[] = directoryPath.listFiles();
		BufferedReader reader = null;
		try{
			File fileToRead = filesList[0];
			reader = new BufferedReader(new FileReader(fileToRead));
			String line = reader.readLine();
			if(type.equals("valueDate")){
				while (line != null) {
					if(line.startsWith("522")) {
						if (payType.equals("DD")){
							String[] data = line.split("USDMYR");
							retVal = data[1].substring(0,6);
						}else if(payType.equals("CT")){
							String[] data = line.split("EURUSD");
							retVal = data[1].substring(0,6);
						}
						break;
					}
					line = reader.readLine();
				}
			}else if(type.equals("creditorValue")){
				while (line != null) {
					if (payType.equals("DD")){
						if(line.startsWith("627")) {
							retVal = line.substring(3,12);
							break;
						}
					}else if (payType.equals("CT")){
						if(line.startsWith("622")) {
							retVal = line.substring(3,12);
							break;
						}
					}
					line = reader.readLine();
				}
			}else if(type.equals("addendaValue")){
				while (line != null) {
					if (payType.equals("DD")){
						if(line.startsWith("627")) {
							retVal = line.substring(13,16);
							break;
						}
					}else if (payType.equals("CT")){
						if(line.startsWith("622")) {
							retVal = line.substring(13,16);
							break;
						}
					}
					line = reader.readLine();
				}
			}
			return retVal;
		}catch (Exception et){
			throw new Exception("Error Occured In readMREFDownloadFile(): " + et.getMessage());
		}finally {
			try {
				// Closing the resources
				reader.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
    
    // Get Addenda count from downloaded file
    public static int checkAddendaCount() throws Exception{
		int retVal = 0;
		File directoryPath = new File(System.getProperty("user.dir")+"\\Downloads");
		//List of all files and directories
		File filesList[] = directoryPath.listFiles();
		BufferedReader reader = null;
		try{
			File fileToRead = filesList[0];
			reader = new BufferedReader(new FileReader(fileToRead));
			String line = reader.readLine();
			while (line != null) {
				if(line.startsWith("7")) {
					retVal++;
				}
				line = reader.readLine();
			}
			return retVal;
		}catch (Exception et){
			throw new Exception("Error Occured In readMREFDownloadFile(): " + et.getMessage());
		}finally {
			try {
				// Closing the resources
				reader.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
    
  //End of Class
}
