package com.gs.utilities;


import java.io.FileInputStream;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelOPerations {
	

	
	private static FileInputStream excelfile;
//	private static FileOutputStream fileOut;
	private static XSSFSheet ExcelWSheet;

	private static XSSFWorkbook ExcelWBook;

	private static XSSFCell Cell;

	//private static XSSFRow Row;
    
    public static int getRowContainsBySheetName(String path,String SheetName,String sTestCaseName, int colNum) throws Exception{

    	int i;



    	try {
    		
    		excelfile = new FileInputStream(path);

    		// Access the required test data sheet

    		ExcelWBook = new XSSFWorkbook(excelfile);

    		ExcelWSheet = ExcelWBook.getSheet(SheetName);

    		int rowCount = ExcelUtilities.getRowUsed();

    		for ( i=0 ; i<rowCount; i++){

    			if  (ExcelOPerations.getCellData(i,colNum).equalsIgnoreCase(sTestCaseName)){

    				break;

    			}

    		}

    		return i;
    		

    	}catch (Exception e){

    		//Log.error("Class ExcelUtil | Method getRowContains | Exception desc : " + e.getMessage());

    		throw(e);

    	}
    
   

    }
	
    
    public static String getCellData(int RowNum, int ColNum) throws Exception{

        try{

     	  Cell = ExcelWSheet.getRow(RowNum).getCell(ColNum);

           String CellData = Cell.getStringCellValue();

           return CellData;

           }catch (Exception e){

             return"";

           }

 }
    
    public static int getRowUsed() throws Exception {

    	try{

    		int RowCount = ExcelWSheet.getLastRowNum();

    		//Log.info("Total number of Row used return as &lt; " + RowCount + " &gt;.");		

    		return RowCount;

    	}catch (Exception e){

    		//Log.error("Class ExcelUtil | Method getRowUsed | Exception desc : "+e.getMessage());

    		System.out.println(e.getMessage());

    		throw (e);

    	}

    }

}
