package com.gs.utilities;

import java.io.File;
import java.util.ArrayList;

public class DownloadandMoveOutput {
	public static void renameFileName(String source, String dest, String fName) {
		String tempFileName = fName;
		System.out.println("**************source:" + source + "\n***********tempFileName:" + tempFileName);

		// source=source.replace("\\","\\\\");
		// System.out.println("**************source:"+source+"\n***********tempFileName:"+tempFileName);
		// dest=dest.replace("\\","\\\\");
		File dir = new File(dest);
		System.out.println("**********dir:" + dir);
		ArrayList<String> filenames = new ArrayList<String>();
		for (File desfile : dir.listFiles()) {
			System.out.println("***************desfile.getName():" + desfile);
			filenames.add(desfile.getName());
			System.out.println("***************after add desfile.getName():" + desfile);
		}
		// printing filenames of list
		/*
		 * for (int i = 0; i < filenames.size(); i++) { String s = filenames.get(i);
		 * System.out.println("File "+i+" : "+s); } System.out.println("\n");
		 */

		File files = new File(source);
		System.out.println("files:" + files);
		File[] filesInDir = files.listFiles();
		// String[] filesInDirnames = files.list();
		System.out.println("\nfilesInDir.length:" + files.listFiles());
		for (File file : filesInDir) {
			System.out.println("***************Inside foreach loop***************");
			int i = 0;
			String name = file.getName();
			// System.out.println("File Before Rename "+name);
			for (;;) {
				System.out.println("***************in for loop***********************");
				if (filenames.contains(tempFileName)) {
					System.out.println("***************in if block***********************");

					System.out.println("file already exists with given name so modifying file name");
					i++;
					tempFileName = fName + "-" + i + "-" + ".txt";

					continue;

				} else {
					System.out.println("***************in else block***********************");

					tempFileName = fName + "-" + i + "-" + ".txt";
					filenames.add(tempFileName);
					System.out.println("file name doesn't exist in dest folder");
					String newName = tempFileName;
					String newPath = dest + "/" + newName;
					file.renameTo(new File(newPath));
					System.out.println(name + " changed to " + newName);
					file.delete();
					break;
				}
			}
		}
	}

}
