package com.gs.utilities;

public class Constants_ACH {
	
	public static final String Path_TestData = "TestData";
	
	public static String DownloadsPath = "Downloads";
	
	public static final String baseRESTURL = "https://ec2-3-208-75-199.compute-1.amazonaws.com:8443/VolPayRest/rest/v2";

	public static String extendedreport = "reports\\Report.html";
	
	// wait constants
	public static int tooshort_sleep = 2000;
	public static int short_sleep = 5000;
	public static int short_implicit = 5;
	public static int short_explicit = 10;
	
	public static int avg_sleep = 10000;
	public static int avg_implicit = 10;
	public static int avg_explicit = 30;
	
	public static int long_sleep = 20000;
	public static int long_implicit = 15;
	public static int long_explicit = 50;


	public static String Deployment = "E:\\GS_Environment\\apache-tomcat\\VPH_messages\\Input";
	public static String FEDACH_IN = "FEDACH_IN/";
	
	public static final String gs_Samples = Path_TestData + "\\" + "Samples" + "\\";
	
	public static String Path_AutoIT = Path_TestData + "/" + "AutoIT/FileUpload.exe";

	public static final String File_TestCases = Path_TestData + "\\" + "TestCases.xlsx";
	public static final String File_TestData = Path_TestData + "\\" + "TestData.xlsx";
	
	// Testcases excel sheet
	public static final int Col_testcaseName = 1;
	public static final int Col_sampleDirectory = 2;
	public static final int Col_status = 3;
	public static final int Col_exeDateTime = 4;
	public static final int Col_outputsPath = 5;
	public static final int Col_comments = 6;
	public static final int Col_AssertionComments = 7;
	
	// Mandate sheet
	public static final int Col_ACH_Payment = 2;
	public static final int Col_Mandate_Profile = 3;
	public static final int Col_Profile_Form = 4;
	public static final int Col_Amount_LimitCode = 5;
	public static final int Col_AdviceFile = 6;
}
