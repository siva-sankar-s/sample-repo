package com.gs.pages;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import com.gs.utilities.Constants;
import com.gs.utilities.WaitLibrary;
import com.paulhammant.ngwebdriver.ByAngular;
import com.paulhammant.ngwebdriver.ByAngularRepeater;

public class InitiateLineTransfer {
	//Line transfer Tab
	public static WebElement initiateLineTransferTab(WebDriver driver) {
		By initiateLineTransferTab = By.xpath("//span[contains(text(),'Initiate Line Transfer')]");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, initiateLineTransferTab);
		return driver.findElement(initiateLineTransferTab);
	}
	//Enter Amount
	public static WebElement amountIniLineTrans(WebDriver driver) {
		By amount = By.xpath("//input[contains(@placeholder,'Please Enter Amount')]");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, amount);
		return driver.findElement(amount);
	}
	//Click on currency
	public static WebElement currencyIniLineTransfer(WebDriver driver) {
		By currency = By.xpath("//span[contains(@id,'select2-Currency_0_2-container')]");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, currency);
		return driver.findElement(currency);
	}
	//Enter Currency
	public static WebElement currencySearchField(WebDriver driver) {
		By currencySearch = By.xpath("//span[@class='select2-search select2-search--dropdown']//input[contains(@aria-controls,'select2-Currency_0_2-results')]");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, currencySearch);
		return driver.findElement(currencySearch);
	}//span[@class="select2-search select2-search--dropdown"]//input[@aria-controls="select2-Currency_0_2-results"]
	//date
	public static WebElement date(WebDriver driver) {
		By date = By.xpath("//input[contains(@fieldid,'0_3')]");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, date);
		return driver.findElement(date);
	}
	//Click on Source Account Number
	public static WebElement sourceAccountNumber(WebDriver driver) {
		By sourceAccountNumber = By.xpath("//span[@id='select2-SourceAccountNo_1_0-container']");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, sourceAccountNumber);
		return driver.findElement(sourceAccountNumber);
	}
	//Search Source Account Number
	public static WebElement sourceAccountNumberSearch(WebDriver driver) {
		By sourceAccountNumberSearch = By.xpath("//input[contains(@aria-controls,'select2-SourceAccountNo_1_0-results')]");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, sourceAccountNumberSearch);
		return driver.findElement(sourceAccountNumberSearch);
	}
	//Click on Destination Account Number
	public static WebElement destinationAccountNumber(WebDriver driver) {
		By destinationAccountNumber = By.xpath("//span[contains(@id,'select2-DestinationAccountNo_2_0-container')]");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, destinationAccountNumber);
		return driver.findElement(destinationAccountNumber);
	}
	//Enter Destination Account Number
	public static WebElement destinationAccountNumberSearch(WebDriver driver) {
		By destinationAccountNumberSearch = By.xpath("//input[contains(@aria-controls,'select2-DestinationAccountNo_2_0-results')]");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, destinationAccountNumberSearch);
		return driver.findElement(destinationAccountNumberSearch);
	}
	//Submit
	public static WebElement submitLineTransfer(WebDriver driver) {
		By submitLineTransfer = By.xpath("//button[@type='submit']//span[@class='ng-scope'][contains(.,'Submit')]");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, submitLineTransfer);
		return driver.findElement(submitLineTransfer);
	}

}
