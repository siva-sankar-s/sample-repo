package com.gs.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import com.gs.utilities.Constants;
import com.gs.utilities.WaitLibrary;
import com.paulhammant.ngwebdriver.ByAngular;
import com.paulhammant.ngwebdriver.ByAngularRepeater;

public class ReceivedInstruction {
	
	public static WebElement element = null;
	
	public static By accountPostingRepeater = ByAngular.repeater("apData in accountPostingDetails");
	public static By originalPaymentRef = ByAngular.repeater("payments in filedetailpcd");
	public static By linkedMsgs = ByAngular.repeater("lData in linkedPayments");
	public static By sysInteraction = ByAngular.repeater("msg in cDataBID");
	public static By actionBtnRepeater = ByAngular.repeater("newbuttons in nActionBtns");
	public static By externalCommunicationRepeater = ByAngular.repeater("msg in relatedMsgs");
	public static By webFormDetails = ByAngular.repeater("fields in webformFields");
	public static By eventLog = ByAngular.repeater("audit1 in cDataAudit");
	public static By inputBatchRepeater = ByAngular.repeater("batchInfo in batchInfodata");
	public static By recInsRepeater = ByAngular.repeater("detail in items");
	
	public static WebElement recInsTab(WebDriver driver) {
		By recInsTab = By.xpath("//span[contains(text(),'Received Instructions')]");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, recInsTab);
		return driver.findElement(recInsTab);
	}
		
	//Get List View path
	public static WebElement listView(WebDriver driver) {
		By listView = By.xpath("//*[@id=\'btn_1\']");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, listView);
		return driver.findElement(listView);
	}
	
	// Get Received Instruction Repeater List
	public static List<WebElement> recInsList(WebDriver driver) {
		By getRecDataList = By.xpath("//div[contains(@class,'FixHead')]//tr[contains(@ng-repeat-start,'detail in items')]");
		return driver.findElements(getRecDataList);
	}
	
	//get rec ins data
	public static WebElement recInsData(WebDriver driver, int row, String col) {
		By recInsData = ((ByAngularRepeater) recInsRepeater).row(row).column(col);
		return driver.findElement(recInsData);
	}
	
	public static WebElement getSourceIns(WebDriver driver){
		By getSourceIns = By.xpath("//div[contains(@class,'FixHead')]//tr[contains(@ng-repeat-start,'detail in items')][1]//td[6]");
		return driver.findElement(getSourceIns);
	}
	
	//Getting Transport name using File name
	public static WebElement getTransportNameStatusByFileName(WebDriver driver, String fileName) {
		By getTransportNameStatusByFileName = By.xpath("//li[contains(text(),'" + fileName + "')]/following::span[@tooltip='Instruction Status']");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, getTransportNameStatusByFileName);
		return driver.findElement(getTransportNameStatusByFileName);
	}
	
	//Getting Instruction Id by Transport name
	public static WebElement getInsIdByTransportName(WebDriver driver, String transName) {
		By getInsIdByTransportName = By.xpath("//li[contains(text(),'" + transName + "')]/preceding::li[@tooltip='Instruction ID'][1]");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, getInsIdByTransportName);
		return driver.findElement(getInsIdByTransportName);
	}
		
	public static WebElement getPaymentId(WebDriver driver) {
		By getPayId = By.xpath("//div[contains(@id,'tab')]//tbody[1]/tr[1]/td[1]/span");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, getPayId);
		WebElement element = driver.findElement(getPayId);
		return element;
	}
	
	// Verify Instruction Page
	
	public static WebElement verifyInstructionPage(WebDriver driver, String fileName) {
		By filNm = By.xpath("//span[@tooltip='Transport Name'][contains(.,'"+fileName+"')]");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, filNm);
		return driver.findElement(filNm);
	}
	
	// Verify Payment Page
	
	public static WebElement verifyPaymentPage(WebDriver driver, String pmtId) {
		By payIDHeader = By.xpath("//span[@class='bold ng-binding'][contains(.,'"+pmtId+"')]");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, payIDHeader);
		return driver.findElement(payIDHeader);
	}
	
	// Get Payment status
	
	public static WebElement getPaymentStatus(WebDriver driver) {
		element = driver.findElement(By.xpath("//div[contains(@id,'tab')]//tbody[1]/tr[1]/td[9]"));
		return element;
	}
	
	// Get Account posting Tab
	
	public static WebElement getAccountPostingTab(WebDriver driver) {
		element = driver.findElement(By.xpath("//b[contains(text(),'Account Posting')]"));
		return element;
	}

	// Get account posting Repeater element
	public static WebElement accountPostingData(WebDriver driver, int row, String col) {
		By accountPosData = ((ByAngularRepeater) accountPostingRepeater).row(row).column(col);
		return driver.findElement(accountPosData);
	}
	
	// Get account posting Repeater List
	public static List<WebElement> accountPostingDetailsList(WebDriver driver) {
		return driver.findElements(accountPostingRepeater);
	}
	
	//Locates payment id
	public static WebElement paymentId(WebDriver driver) {
		By PaymentId = By.xpath("//span[@class='caption-subject bold uppercase ng-binding']");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, PaymentId);
		return driver.findElement(PaymentId);
	}
	
	//get original payment reference
	public static WebElement originalPaymentReference(WebDriver driver, int row, String col) {
		By OriginalPayRef = ((ByAngularRepeater) originalPaymentRef).row(row).column(col);
		return driver.findElement(OriginalPayRef);
	}
	
	// Get payment Repeater List
		public static List<WebElement> paymentList(WebDriver driver) {
			return driver.findElements(originalPaymentRef);
		}
	
	//get output instruction Id
	public static WebElement outputInstructionId(WebDriver driver) {
		By outputInsIdId = By.xpath("//a[@ng-click='goToPaymentSummaryy(cData)']");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, outputInsIdId);
		return driver.findElement(outputInsIdId);
	}
	
	//click on linked message tab
	public static WebElement linkedMessage(WebDriver driver) {
		element = driver.findElement(By.xpath("//b[contains(text(),'Linked Messages')]"));
		return element;
	}
	
	// get Linked message List
	public static  List<WebElement> linkedMsgsList(WebDriver driver) {
		return driver.findElements(linkedMsgs);
	}
	
	// Get Linked message Data
	public static WebElement linkedMsgsData(WebDriver driver, int row, String col) {
		By linkedMsgsData = ((ByAngularRepeater) linkedMsgs).row(row).column(col);
		return driver.findElement(linkedMsgsData);
	}
	
	//get linked message id
	public static WebElement linkedMessageId(WebDriver driver, int row, String col) {
		By LinkedMessageId = ((ByAngularRepeater) linkedMsgs).row(row).column(col);
		element = driver.findElement(LinkedMessageId).findElement(By.xpath("//a"));
		return driver.findElement(LinkedMessageId);
	}
	
	//get status of Payment
	public static WebElement getStatus(WebDriver driver) {
		By status = By.xpath("//span[contains(@tooltip,'Payment Status')]");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, status);
		element = driver.findElement(status);
		return element;
	}
	
	//get Amount of Payment
	public static WebElement getAmount(WebDriver driver) {
		By amount = By.xpath("//span[contains(@tooltip,'Amount')]");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, amount);
		element = driver.findElement(amount);
		return element;
	}
	
	//search with Instruction ID
	public static WebElement searchWithInsID(WebDriver driver) {
		By searchBox = By.xpath("//input[contains(@ng-model,'esearch')]");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, searchBox);
		return driver.findElement(searchBox);
	}
	
	public static WebElement clearSearchBtn(WebDriver driver) {
		By clearSearchBtn = By.xpath("//input[contains(@ng-click,'clearSearch()')]");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, clearSearchBtn);
		return driver.findElement(clearSearchBtn);
	}
	
	// Get System interaction Tab
	public static WebElement systemInteraction(WebDriver driver) {
		element = driver.findElement(By.xpath("//b[contains(text(),'System Interaction')]"));
		return element;
	}
	
	// Get System Interaction Repeater Elements
	public static  List<WebElement> sysInteractionList(WebDriver driver) {
		return driver.findElements(sysInteraction);
	}
	
	// Get System Interaction Data
	public static WebElement sysInteractionData(WebDriver driver, int row, String col) {
		By sysInteractionData = ((ByAngularRepeater) sysInteraction).row(row).column(col);
		return driver.findElement(sysInteractionData);
	}
	
	//Get view button of particular System Interaction Data
	public static WebElement sysInterViewBtn(WebDriver driver, int row) {
		By sysViewBtn = By.xpath("//span[contains(@data-target,'#rawData1_"+row+"')]");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, sysViewBtn);
		return driver.findElement(sysViewBtn);
	}
	
	// Get object Data of System Interaction
	public static WebElement sysInterObjData(WebDriver driver, int row) {
		By objData = By.xpath("//div[@id='rawData1_"+row+"']//pre");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, objData);
		return driver.findElement(objData);
	}
	
	// SysInnteraction Modal close button
	public static WebElement sysModalClose(WebDriver driver, int row) {
		By modalClose = By.xpath("//div[@id='rawData1_"+row+"']//button[@type='button'][normalize-space()='�']");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, modalClose);
		return driver.findElement(modalClose);
	}

	// get the text of Swift in
	public static WebElement paymentMethod(WebDriver driver) {
		By payMethod = By
				.xpath("//div[@class='panel-body collapse in']//div[@class='col-md-4 lineHeight35 ng-binding']");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, payMethod);
		return driver.findElement(payMethod);

	}

	// get original msg sub function
	public static WebElement originalMsgfn(WebDriver driver) {
		By msgFn = By.xpath(
				"//div[@class='panel-body collapse in']//div[@class='col-md-4 colStyle ng-binding'][contains(.,'Original Message Sub Function  :')]");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, msgFn);
		return driver.findElement(msgFn);

	}

	// Get action buttons
	public static WebElement actionBtn(WebDriver driver, int ActionName) {
		By actionBtn = ((ByAngularRepeater) actionBtnRepeater).row(ActionName);
		return driver.findElement(actionBtn);
	}
	
	public static WebElement actionBtn(WebDriver driver) {
		By actionBtn = By.xpath("//span[@class='ng-binding ng-scope'][contains(.,'PaymentAction')]");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, actionBtn);
		return driver.findElement(actionBtn);
	}

	// Select the SWIFT Payment Action
	public static WebElement swiftPaymentAction(WebDriver driver) {
		By swiftPaymentAction = By.xpath("//span[@id=\"select2-Action-container\"][@class=\"select2-selection__rendered\"]");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, swiftPaymentAction);
		return driver.findElement(swiftPaymentAction);
	}

	// Get Field Causing REJT/RETN
	public static WebElement retrejSelect(WebDriver driver) {
		By retrejOption = By.xpath("//select[contains(@ng-change,'diabledFields(fieldData[field.name],field,fieldDetails.section,fieldDetails)')][@id='Field Causing REJT/RETN']");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, retrejOption);
		return driver.findElement(retrejOption);
	}

	// Select Field Causing REJT/RETN
	public static WebElement retrejOptions(WebDriver driver) {
		By retrejOption = By.xpath("//option[@ng-repeat='x in field.renderer.Choice.choiceOptions'][contains(@value,'20')]");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, retrejOption);
		return driver.findElement(retrejOption);
	}

	// Get Reason code
	public static WebElement ReasonCode(WebDriver driver) {
		By ReasonCode = By.xpath("//select[contains(@ng-change,'diabledFields(fieldData[field.name],field,fieldDetails.section,fieldDetails)')][@id='Reason Code']");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, ReasonCode);
		return driver.findElement(ReasonCode);
	}

	// Select Reason code
	public static WebElement selectReasonCode(WebDriver driver) {
		By ReasonCode = By.xpath("//option[@ng-repeat='x in field.renderer.Choice.choiceOptions'][contains(@value,'AC01-Format of the account number specified is not correct.')]");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, ReasonCode);
		return driver.findElement(ReasonCode);
	}

	// Get Account posting Tab
	public static WebElement getExtCommunicationTab(WebDriver driver) {
		element = driver.findElement(By.xpath("//b[contains(text(),'External Communications')]"));
		return element;
	}

	// get External Communication data
	public static WebElement externalCommunication(WebDriver driver, int row, String col) {
		By externalCommuni = ((ByAngularRepeater) externalCommunicationRepeater).row(row).column(col);
		return driver.findElement(externalCommuni);
	}

	// Get external communication Repeater Elements
	public static List<WebElement> extCommunicationList(WebDriver driver) {
		return driver.findElements(externalCommunicationRepeater);
	}
	
	//Download button in External communication
	public static WebElement externalCommDwnldBtn(WebDriver driver, int row) {
		By extDwnldBtn = By.xpath("(//span[contains(@ng-click,'exportToDoc(msg)')])["+(row+1)+"]");
		return driver.findElement(extDwnldBtn);
	}
		
	// Get Action select box for Force status
	public static WebElement actionSelect(WebDriver driver) {
		By actionSelectElem = By.xpath("//select[contains(@name,'Action')]/following-sibling::span");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, actionSelectElem);
		return driver.findElement(actionSelectElem);
	}
	
	public static WebElement actionSelectSearch(WebDriver driver) {
		By actionSelectSearchElem = By.xpath("//input[contains(@type,'search')]");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, actionSelectSearchElem);
		return driver.findElement(actionSelectSearchElem);
	}
	
	// Get Field causing Reject/Return select box for Force status
	public static WebElement FieldCausingREJTRETN(WebDriver driver) {
		By FieldCausingREJTRETNElem = By.xpath("//select[contains(@name,'FieldCausingREJT/RETN')]/following-sibling::span");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, FieldCausingREJTRETNElem);
		return driver.findElement(FieldCausingREJTRETNElem);
	}
	
	public static WebElement fieldCausingREJTRETNSearch(WebDriver driver) {
		By FieldCausingREJTRETNSearchElem = By.xpath("//input[contains(@type,'search')]");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, FieldCausingREJTRETNSearchElem);
		return driver.findElement(FieldCausingREJTRETNSearchElem);
	}
	
	
	// Get Additional Information search box
	public static WebElement additionalInfoSearch(WebDriver driver) {
		By additionalInfoSearchElem = By.xpath("//input[contains(@id,'Additional Information')]");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, additionalInfoSearchElem);
		return driver.findElement(additionalInfoSearchElem);
	}
	
	// Get Reason code select box for Force status
	public static WebElement reasonCodeSelect(WebDriver driver) {
		By reasonCodeSelectElem = By.xpath("//select[contains(@id,'Reason Code')]");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, reasonCodeSelectElem);
		return driver.findElement(reasonCodeSelectElem);
	}
		
	// Get Action select box for Force status
	public static WebElement attachesMsgSelect(WebDriver driver) {
		By attachesMsgSelectElem = By.xpath("//select[contains(@name,'AttachMessageId')]/following-sibling::span[1]");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, attachesMsgSelectElem);
		return driver.findElement(attachesMsgSelectElem);
	}
	
	public static WebElement attachesMsgSelectSearch(WebDriver driver) {
		By attachesMsgSelectSearchElem = By.xpath("//input[contains(@type,'search')]");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, attachesMsgSelectSearchElem);
		return driver.findElement(attachesMsgSelectSearchElem);
	}
	
			
	//click on Accept Return button
		public static WebElement acceptReturn(WebDriver driver) {
		By acceptReturn = By.xpath("//span[@class='ng-binding ng-scope'][contains(.,'Accept Return')]");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, acceptReturn);
		return driver.findElement(acceptReturn);
	}
		//click on Force Status button
		public static WebElement forceStatus(WebDriver driver) {
		By forceStatus = By.xpath("//span[@class='ng-binding ng-scope'][contains(.,'Force Status')]");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, forceStatus);
		return driver.findElement(forceStatus);
	}							

		// Select the field causing REt RE
		public static WebElement RETREJ(WebDriver driver) {
		By swiftPaymentAction = By.xpath("//span[@id=\"select2-Field Causing REJT/RETN-container\"][@class=\"select2-selection__rendered\"]");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, swiftPaymentAction);
		return driver.findElement(swiftPaymentAction);
	}	
		// enter additional details
		public static WebElement additionalDet(WebDriver driver) {
		By additionalDet = By.xpath("//input[contains(@class,'form-control ng-pristine ng-empty ng-invalid ng-invalid-required ng-valid-maxlength ng-touched')][@id=\"Additional Information\"]");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, additionalDet);
		return driver.findElement(additionalDet);
	}	
		// enter Attach message ID
		public static WebElement attachMsgID(WebDriver driver) {
		By attachMsgID = By.xpath("//span[contains(@class,'select2-selection__rendered')][@id=\"select2-Attach Message Id-container\"]");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, attachMsgID);
		return driver.findElement(attachMsgID);
	}
	// linked payment msg ID 
		public static WebElement linkedPaymentID(WebDriver driver) {
		By linkedPaymentID = By.xpath("//span[@class='bold ng-binding'][@tooltip=\"Payment ID\"]");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, linkedPaymentID);
		return driver.findElement(linkedPaymentID);
	}	
	
	public static WebElement processAsPayment(WebDriver driver) {
		By processAsPayment = By.xpath("//span[@class='ng-binding ng-scope'][contains(.,'Process as Payment')]");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, processAsPayment);
		return driver.findElement(processAsPayment);
	}
	
	//click on repair button
	public static WebElement repairBtn(WebDriver driver) {
		By repair = By.xpath("//button[@type='button'][contains(text(),'Repair')]");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, repair);
		return driver.findElement(repair);
	}
	//click on Creditor
		public static WebElement Creditor(WebDriver driver) {
		By Creditor = By.xpath("//div[@class='form-group']//div[@class='panel panel-default panel-custom ng-scope']//h4[@data-target='#Creditor']");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, Creditor);
		return driver.findElement(Creditor);
	}
	//click on Creditor text
	public static WebElement creditorTxt(WebDriver driver) {
		By creditorTxt = By.xpath("//div[@id='Creditor']//input[@placeholder='Please Enter Account']");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, creditorTxt);
		return driver.findElement(creditorTxt);
	}

	// click on Submit Repair
	public static WebElement Submit(WebDriver driver) {
		By Submit = By.xpath("//div[@class='panel-footer']//button[@type='submit']");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, Submit);
		return driver.findElement(Submit);
	}

	// click on Creditor Agent account text
	public static WebElement creditorAgentTxt(WebDriver driver) {
		By creditorAgentTxt = By.xpath("//div[@id='CreditorAgent']//input[@placeholder='Please Enter Account']");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, creditorAgentTxt);
		return driver.findElement(creditorAgentTxt);
	}

	// click on Creditor Agent BIC text
	public static WebElement creditorAgentBIC(WebDriver driver) {
		By creditorAgentBIC = By.xpath("//div[@id='CreditorAgent']//input[@placeholder='Please Enter BIC']");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, creditorAgentBIC);
		return driver.findElement(creditorAgentBIC);
	}

	// get OFAC data
	public static WebElement OFAC(WebDriver driver, int row) {
		By ofacData = By.xpath("//div[contains(@id,'rawData1_" + row + "')]");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, ofacData);
		return driver.findElement(ofacData);
	}

	// click on Creditor
	public static WebElement CreditorAgent(WebDriver driver) {
		By Creditor = By.xpath("//div[@class='ng-scope col-xs-12']//div[@class='form-group']//div[@class='panel panel-default panel-custom ng-scope']//h4[@data-target='#CreditorAgent']");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, Creditor);
		return driver.findElement(Creditor);
	}
	
	//Accept Payment
	public static WebElement acceptPayment(WebDriver driver) {
	By acceptPayment = By.xpath("//span[@class='ng-binding ng-scope'][contains(.,'Accept Payment')]");
	WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, acceptPayment);
	return driver.findElement(acceptPayment);
}
	//Getting ins Id element using ins id
	public static WebElement findInsID(WebDriver driver, String insID) {
		By getInsID = By.xpath("//li[contains(.,'"+insID+"')][@tooltip='Instruction ID']");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, getInsID);
		return driver.findElement(getInsID);
	}
	
	//Get instruction type from Source data
	public static WebElement getSource(WebDriver driver, String sourceData) {
		By getSource = By.xpath("//li[contains(.,'"+sourceData+"')]/preceding::li[@tooltip='Instruction Type'][1]");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, getSource);
		return driver.findElement(getSource);

	}
	
	//get insID in LineTransfer
	public static WebElement getInsId(WebDriver driver) {
		By getInsId = By.xpath("//span[contains(@tooltip,'Instruction ID')]");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, getInsId);
		return driver.findElement(getInsId);

	}
	
	// Get Payment details Div1
		public static WebElement getPaymentDetailsCol1(WebDriver driver) {
			By getDiv = By.xpath("//div[contains(@id,'pymntDetailTable')]//following::div[1]");
			WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, getDiv);
			return driver.findElement(getDiv);
		}
		
		// Get Payment details Div2
		public static WebElement getPaymentDetailsCol2(WebDriver driver) {
			By getDiv = By.xpath("//div[contains(@id,'pymntDetailTable')]//following::div[2]");
			WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, getDiv);
			return driver.findElement(getDiv);
		}
		
		// Get Payment details Div3
		public static WebElement getPaymentDetailsCol3(WebDriver driver) {
			By getDiv = By.xpath("//div[contains(@id,'pymntDetailTable')]//following::div[3]");
			WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, getDiv);
			return driver.findElement(getDiv);
		}
		
		//Get Payment Event Log Repeater elements
		public static  List<WebElement> eventLogList(WebDriver driver) {
			return driver.findElements(eventLog);
		}
		public static WebElement eventLogData(WebDriver driver, int row, String col) {
			By eventLogData = ((ByAngularRepeater) eventLog).row(row).column(col);
			return driver.findElement(eventLogData);
		}
		
		//Click on Accept Mandate button
		public static WebElement acceptMandateBtn(WebDriver driver) {
		By acceptMandateBtn = By.xpath("//span[contains(.,'AcceptMANDATE')]");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, acceptMandateBtn);
		return driver.findElement(acceptMandateBtn);
		}

		//Click on default Mandate Button
		public static WebElement defaultMandateBtn(WebDriver driver) {
		By defaultMandateBtn = By.xpath("//span[contains(.,'DefaultActionMANDATE')]");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, defaultMandateBtn);
		return driver.findElement(defaultMandateBtn);
		}

		//Click on Payment Action Button
		public static WebElement paymentAction(WebDriver driver) {
		By paymentAction = By.xpath("//span[contains(.,'PaymentAction')]");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, paymentAction);
		return driver.findElement(paymentAction);
		}
		
		// Reject - Payment action - Action textbox 
		public static WebElement paymentActionActionBtn(WebDriver driver) {
			By paymentActionBtn = By.xpath("//span[(@id='select2-Action-container')]");
			WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, paymentActionBtn);
			return driver.findElement(paymentActionBtn);
		}
		
		//Reject - Payment action - Action search
		public static WebElement paymentActionBtnSearch(WebDriver driver) {
			By paymentActionBtnSearch = By.xpath("//input[(@type='search')]");
			WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, paymentActionBtnSearch);
			return driver.findElement(paymentActionBtnSearch);
		}
		
		//Reject - Payment action - Action list	
		public static WebElement paymentActionList(WebDriver driver) {
			By paymentActionList = By.xpath("//ul[@id='select2-Action-results']/li[text()='REJECT PAYMENT']");
			WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, paymentActionList);
			return driver.findElement(paymentActionList);
		}

		// Reject - Payment action - Reason Code textbox 
		public static WebElement paymentActionReasonBtn(WebDriver driver) {
			By paymentActionReasonBtn = By.xpath("//span[(@id='select2-Reason Code-container')]");
			WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, paymentActionReasonBtn);
			return driver.findElement(paymentActionReasonBtn);
		}
		
		//Reject - Payment action - Reason Code search
		public static WebElement paymentReasonBtnSearch(WebDriver driver) {
			By paymentReasonBtnSearch = By.xpath("//input[(@type='search')]");
			WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, paymentReasonBtnSearch);
			return driver.findElement(paymentReasonBtnSearch);
		}
		
		//Reject - Payment action - Reason Code list	
		public static WebElement paymentReasonList(WebDriver driver) {
			By paymentReasonList = By.xpath("//ul[@id='select2-Reason Code-results']/li[text()='R01-Insufficient Funds']");
			WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, paymentReasonList);
			return driver.findElement(paymentReasonList);
		}
		
		public static WebElement getPaymentID(WebDriver driver) {
			By getPaymentID = By.xpath("(//span[@tooltip='Payment ID'])");
			WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, getPaymentID);
			return driver.findElement(getPaymentID);
		}
		
		//click on submit for Mandate Repair
		public static WebElement submitRepairBtn(WebDriver driver) {
			By submitRepairBtn = By.xpath("//div[@class='panel-footer actionWebForm']//button[contains(@type,'submit')]");
			WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, submitRepairBtn);
			return driver.findElement(submitRepairBtn);
		}
		
		//Search with existing Payment ID
		public static WebElement searchPaymentID(WebDriver driver, String PaymentId) {
			By searchPaymentID = By.xpath("//span[contains(.,'"+PaymentId+"')]");
			WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, searchPaymentID);
			return driver.findElement(searchPaymentID);
		}
		
		//Click InsID from Payment Page
		public static WebElement insIDPayment(WebDriver driver, String insId) {
			By insIDPayment = By.xpath("//span[@ng-if=\"fromPage =='filedetail'\"]//a[contains(.,'"+insId+"')]");
			WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, insIDPayment);
			return driver.findElement(insIDPayment);
		}
		
		public static WebElement payEventLog(WebDriver driver) {
			By payEventLog = By.xpath("//b[contains(text(),'Payment Event Log')]");
			WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, payEventLog);
			return driver.findElement(payEventLog);
		}
		
		//get status of Instruction
		public static WebElement getInsStatus(WebDriver driver) {
			By status = By.xpath("//span[contains(@tooltip,'Instruction Status')]");
			WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, status);
			element = driver.findElement(status);
			return element;
		}
		
		// Get Input Batch Information Tab
		public static WebElement batchInformation(WebDriver driver) {
			element = driver.findElement(By.xpath("//b[contains(text(),'Input Batch Information')]"));
			return element;
		}
		
		// Get input batch Repeater element
		public static WebElement inputBatchData(WebDriver driver, int row, String col) {
			By inputBatchData = ((ByAngularRepeater) inputBatchRepeater).row(row).column(col);
			return driver.findElement(inputBatchData);
		}

		//get status of Batch Id
		public static WebElement getBatchStatus(WebDriver driver) {
			By batchStatus = By.xpath("//span[contains(@tooltip,'Batch Status')]");
			WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, batchStatus);
			element = driver.findElement(batchStatus);
			return element;
		}
		
		// Verify Batch id in batch page
		public static WebElement verifyBatchPage(WebDriver driver, String batId) {
			By batIdHeader = By.xpath("//span[@class='bold ng-binding'][contains(.,'"+batId+"')]");
			WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, batIdHeader);
			return driver.findElement(batIdHeader);
		}
		
		//Force Release
		public static WebElement ForceRelease(WebDriver driver) {
			By ForceRelease = By.xpath("//span[contains(.,'ForceRelease')]");
			WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, ForceRelease);
			return driver.findElement(ForceRelease);
		}
		

		//Accept Payment
		public static WebElement AcceptPayment(WebDriver driver) {
			By acceptPayment = By.xpath("//span[contains(.,'Accept Payment')]");
			WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, acceptPayment);
			return driver.findElement(acceptPayment);
		}
		
		//Ignore
		public static WebElement Ignore(WebDriver driver) {
			By Ignore = By.xpath("//span[contains(.,'Ignore')]");
			WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, Ignore);
			return driver.findElement(Ignore);
		}
		
		//Reject Payment
		public static WebElement RejectPayment(WebDriver driver) {
			By RejectPayment = By.xpath("//span[contains(.,'Reject Payment')]");
			WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, RejectPayment);
			return driver.findElement(RejectPayment);
		}
		
		// Payment Refresh Icon 
		public static WebElement paymentRefreshIcon(WebDriver driver) {
			By paymentRefreshIcon = By.xpath("//button[contains(@tooltip,'Refresh')]");
			WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, paymentRefreshIcon);
			return driver.findElement(paymentRefreshIcon);
		}
		
		// Click back to instruction id
		public static WebElement clickBackIns(WebDriver driver, String insId) {
			By insIDElem = By.xpath("//li[contains(.,'"+insId+"')]");
			WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, insIDElem);
			return driver.findElement(insIDElem);
		}
		
		// Click Payment with Payment Id in table data
		public static WebElement clickPaymentID(WebDriver driver, String payId) {
			By payIDElem = By.xpath("//td[contains(.,'"+payId+"')]");
			WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, payIDElem);
			return driver.findElement(payIDElem);
		}

		//get Original Instruction ID
		public static WebElement getOriginalInsID(WebDriver driver) {
			By getOriginalInsID = By.xpath("//div[@class='col-md-4 ng-binding ng-scope'][contains(.,'OriginalInstructionID:')]");
			WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, getOriginalInsID);
			return driver.findElement(getOriginalInsID);
		}
		
		//get Instruction Status
		public static WebElement InsStatus(WebDriver driver) {
			By getInsStatus = By.xpath("//div[@class='col-md-4 ng-binding ng-scope'][contains(.,'InstructionStatus:')]");
			WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, getInsStatus);
			return driver.findElement(getInsStatus);
		}
		
		//Click on Payment Information
		public static WebElement payInformation(WebDriver driver) {
			By payDetails = By.xpath("//span[@class='bold'][contains(.,'Payment Information')]");
			WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, payDetails);
			return driver.findElement(payDetails);
		}
		
		//Debitor Agent Details
		public static WebElement debitAgentDet(WebDriver driver) {
			By debitAgentDet = By.xpath("//span[contains(.,'Debtor Agent Details')]");
			WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, debitAgentDet);
			return driver.findElement(debitAgentDet);
		}
		
		//Get Debitor Agent Details
		public static WebElement getDebitAgentDetails(WebDriver driver) {
			By getDebitAgentDetails = By.xpath("//div[contains(@collapse,'DebtorAgentCollapsed')]//div[2]");
			WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, getDebitAgentDetails);
			return driver.findElement(getDebitAgentDetails);
		}
		
		//Click on Creditor Agent Details
		public static WebElement creditAgentDet(WebDriver driver) {
			By creditAgentDet = By.xpath("//span[contains(.,'Creditor Agent Details')]");
			WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, creditAgentDet);
			return driver.findElement(creditAgentDet);
		}
		
		//Get Creditor Agent Details
		public static WebElement getCreditAgentDetails(WebDriver driver) {
			By getCreditAgentDetails = By.xpath("//div[contains(@collapse,'CreditorAgentCollapsed')]//div[2]");
			WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, getCreditAgentDetails);
			return driver.findElement(getCreditAgentDetails);
		}
		
		//Get Value Date
		public static WebElement getValDate(WebDriver driver) {
			By getValDate = By.xpath("//div[@collapse='isCollapsed']//div[8]");
			WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, getValDate);
			return driver.findElement(getValDate);
		}
}