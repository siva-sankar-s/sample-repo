package com.gs.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.gs.utilities.Constants;
import com.gs.utilities.WaitLibrary;
import com.paulhammant.ngwebdriver.ByAngular;
import com.paulhammant.ngwebdriver.ByAngularRepeater;

public class PartyServiceAssociation {
		public static By partyServiceAssCodeData = ByAngular.repeater("office in readData");
		

		//Get Grid View path
		public static WebElement gridView(WebDriver driver) {
			By gridView = By.xpath("//*[@id='btn_2']");
			WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, gridView);
			return driver.findElement(gridView);
		}		
		
		//edit profile btn
		public static WebElement editBtn(WebDriver driver) {
			By editBtn = By.xpath("//a[@tooltip='Edit']");
			WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, editBtn);
			return driver.findElement(editBtn);
		}	
		
		//Get PSA code data list
		public static  List<WebElement> partyServiceAssDataList(WebDriver driver) {
			return driver.findElements(partyServiceAssCodeData);
		}
		
		//Get PSA table data
		public static WebElement partyServiceAssTableData(WebDriver driver, int row, int col) {
			By partyServiceAssTableData = ((ByAngularRepeater) partyServiceAssCodeData).row(row);
			WebElement elem = driver.findElement(partyServiceAssTableData);
			By dat = ByAngular.repeater("field in fieldDetails.Section");
			return elem.findElements(dat).get(col);
		}
		
		//Batch duplicate YES
		public static WebElement BatchDuplicateYes(WebDriver driver) {
			By BatchDuplicateCheck = By.xpath("//input[@name='BatchDuplicatecheck' and @value='true']");
			WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, BatchDuplicateCheck);
			return driver.findElement(BatchDuplicateCheck);
		}
		
		//Batch Duplicate Days
		public static WebElement BatchDuplicateDays(WebDriver driver) {
			By BatchDuplicateDays = By.xpath("//input[@id='Batch Duplicate Check - Number Of Days']");
			WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, BatchDuplicateDays);
			return driver.findElement(BatchDuplicateDays);
		}
		
		//get batch field name
		public static WebElement BatchFieldName(WebDriver driver) {
			By BatchFieldName = By.xpath("//select[@name='batchfldName']/following-sibling::span");
			WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, BatchFieldName);
			return driver.findElement(BatchFieldName);
		}
		
		//get batch field list
		public static WebElement BatchFieldList(WebDriver driver, String batchFieldValue) {
			By BatchFieldList = By.xpath("//ul[@class = 'select2-results__options']/li[text()='"+ batchFieldValue +"']");
			WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, BatchFieldList);
			return driver.findElement(BatchFieldList);
		}
		
		//Save Changes
		public static WebElement SaveChanges(WebDriver driver) {
			By SaveChanges = By.xpath("//span[contains(.,'Save Changes')]");
			WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, SaveChanges);
			return driver.findElement(SaveChanges);
		}
		
		//Batch Duplicate NO
		public static WebElement BatchDuplicateNo(WebDriver driver) {
			By BatchDuplicateCheck = By.xpath("//input[@name='BatchDuplicatecheck' and @value='false']");
			WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, BatchDuplicateCheck);
			return driver.findElement(BatchDuplicateCheck);
		}
		
		//Payment Duplicate YES
		public static WebElement PaymentDuplicateYes(WebDriver driver) {
			By PaymentDuplicateCheck = By.xpath("//input[@name='PaymentDuplicatecheck' and @value='true']");
			WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, PaymentDuplicateCheck);
			return driver.findElement(PaymentDuplicateCheck);
		}
		
		//Payment Duplicate NO
		public static WebElement PaymentDuplicateNo(WebDriver driver) {
			By PaymentDuplicateCheck = By.xpath("//input[@name='PaymentDuplicatecheck' and @value='false']");
			WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, PaymentDuplicateCheck);
			return driver.findElement(PaymentDuplicateCheck);
		}	
		
		//get payment field name
		public static WebElement PaymentFieldName(WebDriver driver) {
			By paymentFieldName = By.xpath("//select[@name='fldName']/following-sibling::span");
			WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, paymentFieldName);
			return driver.findElement(paymentFieldName);
		}		
				
		//get payment field list
		public static WebElement PaymentFieldList(WebDriver driver, String paymentFieldValue) {
			By PaymentFieldList = By.xpath("//ul[@class = 'select2-results__options']/li[text()='"+paymentFieldValue+"']");
			WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, PaymentFieldList);
			return driver.findElement(PaymentFieldList);
		}	
		//payment duplicate check days
		public static WebElement PaymentDuplicateDays(WebDriver driver) {
			By PaymentDuplicateDays = By.xpath("//input[@id='Payment Duplicate Check - Number Of Days']");
			WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, PaymentDuplicateDays);
			return driver.findElement(PaymentDuplicateDays);
		}
		
		//File Duplicate Check Yes
		public static WebElement FileDuplicateYes(WebDriver driver) {
			By FileDuplicateYes = By.xpath("//input[@name='FileDuplicatecheck' and @value='true']");
			WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, FileDuplicateYes);
			return driver.findElement(FileDuplicateYes);
		}
		
		//File Duplicate days
		public static WebElement FileDuplicateDays(WebDriver driver) {
			By FileDuplicateDays = By.xpath("//input[@id='File Duplicate Check - Number Of Days']");
			WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, FileDuplicateDays);
			return driver.findElement(FileDuplicateDays);
		}
		
		//get file field name
		public static WebElement FieldPath(WebDriver driver) {
			By FieldPath = By.xpath("//select[@name='FieldPath']/following-sibling::span");
			WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, FieldPath);
			return driver.findElement(FieldPath);
		}
		
		//get file field list
		public static WebElement FileFieldList(WebDriver driver, String fileFieldValue) {
			By FileFieldList = By.xpath("//ul[@class = 'select2-results__options']/li[text()='"+ fileFieldValue +"']");
			WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, FileFieldList);
			return driver.findElement(FileFieldList);
		}
		//file duplicate check NO
		public static WebElement FileDuplicateNo(WebDriver driver) {
			By FileDuplicateNo = By.xpath("//input[@name='FileDuplicatecheck' and @value='false']");
			WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, FileDuplicateNo);
			return driver.findElement(FileDuplicateNo);
		}
}