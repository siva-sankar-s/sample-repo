package com.gs.tc.ach_Client_Defects;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.gs.pages.ReceivedInstruction;
import com.gs.utilities.BrowserResolution;
import com.gs.utilities.CaptureScreenshot;
import com.gs.utilities.CommonMethods;
import com.gs.utilities.Constants_ACH;
import com.gs.utilities.Constants_Defects;
import com.gs.utilities.ExcelUtilities;
import com.gs.utilities.FilesUpload;
import com.gs.utilities.GenericFunctions;
import com.gs.utilities.Log;
import com.gs.utilities.LoginLogout;
import com.gs.utilities.WaitLibrary;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class TC008_ACH_Defect_GSAC_3640 extends LoginLogout{
	@Test

	public void executeTC008() throws Exception {
		try {

			Log.startTestCase(log, TestCaseName);

			// Fetch sample Directory
			String sampleDir = ExcelUtilities.getCellDataBySheetName(Constants_Defects.File_TestCases, "TestCases", TestCaseName,Constants_Defects.Col_sampleDirectory);
			System.out.println(sampleDir);
			
			// Fetch Incoming Payment File Name
			String paymentFile = ExcelUtilities.getCellDataBySheetName(Constants_Defects.File_TestData, "ach_Client_Defects" ,TestCaseName, Constants_Defects.Col_Payment_File);
			System.out.println(paymentFile);
			((ExtentTest) test).log(LogStatus.INFO, "File name is : " + paymentFile);
			
    		// Upload Payment File
    		log.info("Uploading Payment File via WinSCP");
			log.info("Source File is  :" + sampleDir + paymentFile);
			log.info("Destination File is  :" + Constants_Defects.ACHPain008ChannelIn);
			FilesUpload.uploadFileByWinSCP(sampleDir + paymentFile, Constants_Defects.ACHPain008ChannelIn);
			((ExtentTest) test).log(LogStatus.PASS, "File Uploaded");
			Thread.sleep(Constants_Defects.short_sleep);
			
			log.info("Click on Received Instruction Tab");
			WebElement recInsTab = ReceivedInstruction.recInsTab(driver);
			WaitLibrary.waitForElementToBeClickable(driver, recInsTab, Constants_Defects.avg_explicit).click();
			WaitLibrary.waitForAngular(driver);
			
			log.info("Click on List View");
			WebElement listView = ReceivedInstruction.listView(driver);
			String listViewClass = WaitLibrary.waitForElementToBeVisible(driver, listView, Constants_Defects.avg_explicit).getAttribute("class");

			if (listViewClass.equalsIgnoreCase("btn-trans cmmonBtnColors")) {
				WaitLibrary.waitForElementToBeClickable(driver, listView, Constants_Defects.avg_explicit).click();
			}
			    		
			// Verify Status of Uploaded Payment File
			BrowserResolution.scrollToElement(driver, ReceivedInstruction.getTransportNameStatusByFileName(driver, paymentFile));
			js.executeScript("window.scrollBy(0,-120)");
			
			// Get and click Instruction ID
			WebElement instdid = ReceivedInstruction.getInsIdByTransportName(driver, paymentFile);
			String insId = WaitLibrary.waitForElementToBeVisible(driver, instdid, Constants_Defects.avg_explicit).getText();
			((ExtentTest) test).log(LogStatus.INFO, "Instruction id:::" + insId);
			WaitLibrary.waitForElementToBeClickable(driver, instdid, Constants_Defects.avg_explicit).click();
			
			log.info("Verifying Instruction Page with Transport name");
			WebElement TransportName = ReceivedInstruction.verifyInstructionPage(driver, paymentFile);
			WaitLibrary.waitForAngular(driver);
			
			log.info("Verifying Instruction Status");
			WebElement insStatusElem = ReceivedInstruction.getInsStatus(driver);
			String insStatus = WaitLibrary.waitForElementToBeVisible(driver, insStatusElem, Constants_Defects.avg_explicit).getText();			
			Assert.assertEquals(insStatus, "DEBULKED");
			((ExtentTest) test).log(LogStatus.PASS, "Instruction Status : " + insStatus);
			BrowserResolution.scrollDown(driver);
			
			log.info("Click on Payment ID");
			List<WebElement> paymentsList = ReceivedInstruction.paymentList(driver);
			int paymentscount = paymentsList.size();
			String paymentID="";
			BrowserResolution.scrollDown(driver);
			for(int l=0; l<paymentscount; l++) {
				WebElement paymentFunction = ReceivedInstruction.originalPaymentReference(driver, l, "MethodOfPayment");
				String paymentFun = paymentFunction.getText();
				String status = ReceivedInstruction.originalPaymentReference(driver, l, "Status").getText();
	
				if(paymentFun.equals("FUTURE_ACH_OUT")) {
					WebElement pmtIdElem = ReceivedInstruction.originalPaymentReference(driver, l, "PaymentID");
					paymentID = WaitLibrary.waitForElementToBeVisible(driver, pmtIdElem, Constants_ACH.avg_explicit).getText();
					
					if(status.equals("TIME WAREHOUSED") || status.equals("WAIT CREDIT PROCESSING BATCH")) {
						if(status.equals("TIME WAREHOUSED")) {
							CommonMethods.forceRelease(driver, paymentID, paymentFile, insId);
							WaitLibrary.waitForAngular(driver);
							TimeUnit.MINUTES.sleep(4);
						}else if(status.equals("WAIT CREDIT PROCESSING BATCH")) {
							TimeUnit.MINUTES.sleep(4);
						}
						log.info("Click on Received Instruction Tab");
						WebElement recInsTab2 = ReceivedInstruction.recInsTab(driver);
						WaitLibrary.waitForElementToBeClickable(driver, recInsTab2, Constants_Defects.avg_explicit).click();
						WaitLibrary.waitForAngular(driver);
						
						log.info("Click on List View");
						WebElement listView2 = ReceivedInstruction.listView(driver);
						String listViewClass2 = WaitLibrary.waitForElementToBeVisible(driver, listView2, Constants_Defects.avg_explicit).getAttribute("class");

						if (listViewClass2.equalsIgnoreCase("btn-trans cmmonBtnColors")) {
							WaitLibrary.waitForElementToBeClickable(driver, listView2, Constants_Defects.avg_explicit).click();
						}
						
						Thread.sleep(Constants_ACH.tooshort_sleep);
			    		WebElement searchInstructionIn = ReceivedInstruction.searchWithInsID(driver);
			    		searchInstructionIn.clear();
			    		searchInstructionIn.sendKeys(insId, Keys.ENTER);
			    		WaitLibrary.waitForAngular(driver);
			    		CommonMethods.clickStatementWithFileName(driver, paymentFile);
			    		BrowserResolution.scrollDown(driver);
			    		WaitLibrary.waitForAngular(driver);
			    		WebElement clickPayID = ReceivedInstruction.clickPaymentID(driver, paymentID);
						WaitLibrary.waitForElementToBeClickable(driver, clickPayID, Constants_ACH.avg_explicit).click();
						break;
					}else if(status.equals("COMPLETED")) {
						WebElement clickPayID = ReceivedInstruction.clickPaymentID(driver, paymentID);
						WaitLibrary.waitForElementToBeClickable(driver, clickPayID, Constants_ACH.avg_explicit).click();
						Thread.sleep(Constants_Defects.short_sleep);
						break;
					}else {
						WebElement clickPayID = ReceivedInstruction.clickPaymentID(driver, paymentID);
						WaitLibrary.waitForElementToBeClickable(driver, clickPayID, Constants_ACH.avg_explicit).click();
						Thread.sleep(Constants_Defects.short_sleep);
					}
				}
				else if (l==paymentscount-1 && !paymentFun.equals("FUTURE_ACH_OUT")) {
						throw new Exception("FUTURE_ACH_OUT payment with COMPLETED status is not Available");
				}
			}

			WaitLibrary.waitForAngular(driver);	
			
			log.info("Verifying Payment Page with PaymentId");
			WebElement paymentIdHeader = ReceivedInstruction.verifyPaymentPage(driver, paymentID);
			
			// Verifying Payment status
			WebElement PaymentStatusElement = ReceivedInstruction.getStatus(driver);
			String PaymentStatus = WaitLibrary.waitForElementToBeVisible(driver, PaymentStatusElement, Constants_Defects.avg_explicit).getText();
			System.out.println("Payment Status:::: "+PaymentStatus);
			Assert.assertEquals(PaymentStatus, "COMPLETED");
			((ExtentTest) test).log(LogStatus.PASS, "Payment Status : " + PaymentStatus);			
			
			Thread.sleep(Constants_Defects.short_sleep);
			log.info("Navigate to System Interaction Tab");
			js.executeScript("arguments[0].click();", ReceivedInstruction.systemInteraction(driver));
			BrowserResolution.scrollDown(driver);
			Thread.sleep(Constants_Defects.short_sleep);
			log.info("Verifying System Interaction");
			List<WebElement> sysIntDataListIn = ReceivedInstruction.sysInteractionList(driver);
			int sysIntDataCountIn = sysIntDataListIn.size();
			int becNotfCount = 0;
			int ofacCount = 0;
			for (int j=0; j< sysIntDataCountIn; j++) {
				String invoPoint = ReceivedInstruction.sysInteractionData(driver, j, "InvocationPoint").getText();
				
				String RelationshipTxt = ReceivedInstruction.sysInteractionData(driver, j, "Relationship").getText();
				
				if(invoPoint.equals("OFAC") && RelationshipTxt.equals("REQUEST")) {
					ofacCount++;
					if(ofacCount == 1) {
						WebElement viewBtn = ReceivedInstruction.sysInterViewBtn(driver, j);
						BrowserResolution.scrollToElement(driver, viewBtn);
						js.executeScript("arguments[0].click();", viewBtn);
						// Getting Data
						Thread.sleep(Constants_Defects.tooshort_sleep);
						
						WebElement OFACData = ReceivedInstruction.OFAC(driver, j);
						String getOFACData = OFACData.getText();
						
						// Clicking close modal
						WebElement modalCloseBtn = ReceivedInstruction.sysModalClose(driver, j);
						js.executeScript("arguments[0].click();", modalCloseBtn);
						
						log.info("Verifying SBBK Info");
						String SBBKinfo = CommonMethods.getOFACDataVal(getOFACData, "SBBKINFO");
						if((SBBKinfo.contains("SECONDARY_OFAC_SCREENING_INDICATOR")) && (SBBKinfo.contains("0"))) {
							((ExtentTest) test).log(LogStatus.PASS, "SBBK Info Verified for Secondary_Ofac_Screening_Indicator");
						}else {
							((ExtentTest) test).log(LogStatus.FAIL, "SBBK Info value not matched for Secondary_Ofac_Screening_Indicator");
						}
					}
				}
				if (invoPoint.equals("BECNOTIFICATION") && RelationshipTxt.equals("NOTIFICATION")) {
					becNotfCount = j;
				}
			}
			if(becNotfCount != 0) {
				WebElement viewBtn = ReceivedInstruction.sysInterViewBtn(driver, becNotfCount-1);
				BrowserResolution.scrollToElement(driver, viewBtn);
				js.executeScript("arguments[0].click();", viewBtn);
				
				// Getting Object Data
				Thread.sleep(Constants_Defects.tooshort_sleep);
				WebElement objDataXml = ReceivedInstruction.sysInterObjData(driver, becNotfCount - 1);
				WaitLibrary.waitForElementToBeVisible(driver, objDataXml, Constants_Defects.avg_explicit);
				String objDataXmlString = objDataXml.getText();
				objDataXmlString = objDataXmlString.replace("<", "");
				objDataXmlString = objDataXmlString.replace(">", "");
				objDataXmlString = objDataXmlString.replace("/", "");
				objDataXmlString = objDataXmlString.replace(" ", "");

				String ActualData ="<InstrForDbtrAgt>\n"
						+ "<Cd>Secondary_Ofac_Screening_Indicator</Cd>\n"
						+ "<InstrInf>0</InstrInf>\n"
						+ "</InstrForDbtrAgt>\n";
				ActualData = ActualData.replace("<", "");
				ActualData = ActualData.replace(">", "");
				ActualData = ActualData.replace("/", "");
				ActualData = ActualData.replace(" ", "");
		 
				 if(objDataXmlString.contains(ActualData)) {
					((ExtentTest) test).log(LogStatus.PASS,  "BECNOTIFICATION Verified for Secondary_Ofac_Screening_Indicator");
				}else {
					((ExtentTest) test).log(LogStatus.FAIL, "BECNOTIFICATION not Matched with Secondary_Ofac_Screening_Indicator");
				}
				
				// Clicking close modal
				WebElement modalCloseBtn = ReceivedInstruction.sysModalClose(driver, becNotfCount - 1);
				js.executeScript("arguments[0].click();", modalCloseBtn);
			} else {
				((ExtentTest) test).log(LogStatus.FAIL, "BECNOTIFICATION is not found");
			}

			ExcelUtilities.setCellDataBySheetName(Constants_Defects.File_TestCases, "TestCases", TestCaseRow, Constants_Defects.Col_status, "Pass");
			ExcelUtilities.setCellDataBySheetName(Constants_Defects.File_TestCases, "TestCases", TestCaseRow, Constants_Defects.Col_outputsPath,TC_Outputs);
			ExcelUtilities.setCellDataBySheetName(Constants_Defects.File_TestCases, "TestCases", TestCaseRow, Constants_Defects.Col_exeDateTime, GenericFunctions.getCurrentTime());

		}
		catch(AssertionError ae){
			ExcelUtilities.setCellDataBySheetName(Constants_Defects.File_TestCases, "TestCases", TestCaseRow, Constants_Defects.Col_status, "Failed");
			assertionerror=ae.getMessage();
			CaptureScreenshot.captureScreenshot();
			((ExtentTest) test).log(LogStatus.FAIL, "<br>"+"<b>Assertion Error</b>"+"<br>"+ae.getMessage()+LoginLogout.test.addScreenCapture(CaptureScreenshot.screenshotName));
			ExcelUtilities.setCellDataBySheetName(Constants_Defects.File_TestCases, "TestCases", TestCaseRow, Constants_Defects.Col_outputsPath,TC_Outputs);
			ExcelUtilities.setCellDataBySheetName(Constants_Defects.File_TestCases, "TestCases", TestCaseRow, Constants_Defects.Col_AssertionComments, assertionerror);
		}
		catch(Exception et){
			ExcelUtilities.setCellDataBySheetName(Constants_Defects.File_TestCases, "TestCases", TestCaseRow, Constants_Defects.Col_status, "Failed");
			exceptionerror=et.getMessage();			
			CaptureScreenshot.captureScreenshot();
			((ExtentTest) test).log(LogStatus.FAIL, "<br>"+"<b>Exception</b>"+"<br>"+et.getMessage()+LoginLogout.test.addScreenCapture(CaptureScreenshot.screenshotName));
			ExcelUtilities.setCellDataBySheetName(Constants_Defects.File_TestCases, "TestCases", TestCaseRow, Constants_Defects.Col_outputsPath,TC_Outputs);
			ExcelUtilities.setCellDataBySheetName(Constants_Defects.File_TestCases, "TestCases", TestCaseRow, Constants_Defects.Col_comments, exceptionerror);
		}
	}

	private String split(String string, String string2) {
		// TODO Auto-generated method stub
		return null;
	}
}