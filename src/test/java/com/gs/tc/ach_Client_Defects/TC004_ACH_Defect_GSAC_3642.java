package com.gs.tc.ach_Client_Defects;

import java.util.List;

import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.gs.pages.Approvals;
import com.gs.pages.PartyServiceAssociation;
import com.gs.pages.ReceivedInstruction;
import com.gs.pages.SideBarMenu;
import com.gs.utilities.BrowserResolution;
import com.gs.utilities.CaptureScreenshot;
import com.gs.utilities.CommonMethods;
import com.gs.utilities.Constants_ACH;
import com.gs.utilities.Constants_Defects;
import com.gs.utilities.ExcelUtilities;
import com.gs.utilities.FilesUpload;
import com.gs.utilities.GenericFunctions;
import com.gs.utilities.Log;
import com.gs.utilities.LoginLogout;
import com.gs.utilities.WaitLibrary;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class TC004_ACH_Defect_GSAC_3642 extends LoginLogout{
	@Test

	public void executeTC004() throws Exception {
		try {

			Log.startTestCase(log, TestCaseName);
			
			((ExtentTest) test).log(LogStatus.INFO, "Test Case : Payment Duplication - Outbound DD");
			
			String partyServiceAssociationCode = ExcelUtilities.getCellDataBySheetName(Constants_Defects.File_TestData, "ach_Client_Defects" ,TestCaseName, Constants_Defects.Col_Party_Service_Association_Code);
			
			log.info("Navigate to Onboarding Data");
			WebElement onboardingData = SideBarMenu.onboardingData(driver);
			BrowserResolution.scrollToElement(driver, onboardingData);
			WaitLibrary.waitForElementToBeClickable(driver, onboardingData, Constants_Defects.avg_explicit).click();

			log.info("Click on Party Service Association");
			WebElement partyServiceAssociation = SideBarMenu.partyServiceAss(driver);
			WaitLibrary.waitForElementToBeClickable(driver, partyServiceAssociation, Constants_Defects.avg_explicit).click();

			log.info("Click on Grid View");
			WebElement gridView = PartyServiceAssociation.gridView(driver);
			String gridViewClass= WaitLibrary.waitForElementToBeVisible(driver, gridView, Constants_Defects.avg_explicit).getAttribute("class");
						
			if (gridViewClass.equalsIgnoreCase("btn-trans cmmonBtnColors")) {			
				WaitLibrary.waitForElementToBeClickable(driver, gridView, Constants_Defects.avg_explicit).click();					
			}
			WaitLibrary.waitForAngular(driver);
			
			log.info("Click on Filter Icon");
			WaitLibrary.waitForAngular(driver);
			WebElement clickFilterIcon = Approvals.clickFilterIcon(driver);
    		WaitLibrary.waitForElementToBeClickable(driver, clickFilterIcon, Constants_Defects.avg_explicit).click();
    		WaitLibrary.waitForAngular(driver);
    	
    		log.info("Enter Party Service Association Code to filter");
    		WebElement filterWithKeyword = Approvals.filterWithKeyword(driver);
    		filterWithKeyword.sendKeys(partyServiceAssociationCode, Keys.ENTER);
    		WaitLibrary.waitForAngular(driver);
    		
    		log.info("Click on filter Icon");
    		WaitLibrary.waitForElementToBeClickable(driver, clickFilterIcon, Constants_Defects.avg_explicit).click();
    		WaitLibrary.waitForAngular(driver);
    		
    		log.info("Click on Party Service Association Code");    					
    		List<WebElement> partyServiceAssCodeData = PartyServiceAssociation.partyServiceAssDataList(driver);
			int partyServiceAssCodeCount = partyServiceAssCodeData.size();
			String partyService = null;
			
			for(int l = 0; l < partyServiceAssCodeCount; l++) {
				try {
					partyService = PartyServiceAssociation.partyServiceAssTableData(driver, l, 1).getText();
				}catch (NoSuchElementException ne) {
					throw new Exception("No Party Service Code found");
				}
				
				if(partyService.equals(partyServiceAssociationCode)) {
					WebElement PartyService = PartyServiceAssociation.partyServiceAssTableData(driver, l, 1);
					WaitLibrary.waitForElementToBeClickable(driver, PartyService, Constants_Defects.avg_explicit).click();
					break;
				}else if (l == partyServiceAssCodeCount-1 && !partyService.equals(partyServiceAssociationCode)) {
					throw new Exception("Party Service Association Code is not Available");
				}
			}
			WaitLibrary.waitForAngular(driver);
			
			((ExtentTest) test).log(LogStatus.INFO, "Edit PSA : " + partyServiceAssociationCode);
			
    		log.info("Click on Edit");
    		WaitLibrary.waitForAngular(driver);
    		try { 		
    			js.executeScript("arguments[0].click();", PartyServiceAssociation.editBtn(driver));
    		}
    		catch (Exception ne) {
				throw new Exception("Unable to edit PSA");
			}
    		WaitLibrary.waitForAngular(driver);
    		
    		log.info("Select Payment Duplicate check to Yes");
    		WebElement paymentDuplicateCheckYes = PartyServiceAssociation.PaymentDuplicateYes(driver);
    		BrowserResolution.scrollToElement(driver, paymentDuplicateCheckYes);
    		js.executeScript("arguments[0].click();", paymentDuplicateCheckYes);
    		js.executeScript("window.scrollBy(0,-120)");
    		WaitLibrary.waitForAngular(driver);

    		log.info("Enter Payment Duplicate Check - Number Of Days");
    		WebElement paymentDuplicateCheckDays = PartyServiceAssociation.PaymentDuplicateDays(driver);
    		BrowserResolution.scrollToElement(driver, paymentDuplicateCheckDays);
    		js.executeScript("arguments[0].click();", paymentDuplicateCheckDays);
    		paymentDuplicateCheckDays.clear();
    		paymentDuplicateCheckDays.sendKeys("2");
    		WaitLibrary.waitForAngular(driver);

    		String paymentFiledValue = "Amount";
    		Thread.sleep(Constants_Defects.tooshort_sleep);
    		log.info("Select Payment Field Name");
    		WebElement paymentFieldName = PartyServiceAssociation.PaymentFieldName(driver);
    		WaitLibrary.waitForElementToBeClickable(driver, paymentFieldName, Constants_Defects.avg_explicit).click();
    		WaitLibrary.waitForAngular(driver);

    		WebElement paymentFieldList = PartyServiceAssociation.PaymentFieldList(driver, paymentFiledValue);
    		String paymentList = PartyServiceAssociation.PaymentFieldList(driver, paymentFiledValue).getText();
    		WaitLibrary.waitForElementToBeClickable(driver, paymentFieldList, Constants_Defects.avg_explicit).click();
    		((ExtentTest) test).log(LogStatus.INFO, "Selected Field : "+paymentList);
    		BrowserResolution.scrollDown(driver);
    		Thread.sleep(Constants_Defects.tooshort_sleep);
			
    		log.info("Save Changes");
    		WebElement saveChanges = PartyServiceAssociation.SaveChanges(driver);
			WaitLibrary.waitForElementToBeClickable(driver, saveChanges, Constants_Defects.avg_explicit).click();
			WaitLibrary.waitForAngular(driver);
			
			log.info("Approve party service association code");
			CommonMethods.approveStatement(driver, partyServiceAssociationCode);
			WaitLibrary.waitForAngular(driver);
			
			((ExtentTest) test).log(LogStatus.INFO, "PSA : " + partyServiceAssociationCode + " - Updated");
			
			log.info("Navigate to Payment Module");
			WebElement paymentModule = SideBarMenu.paymentModule(driver);
			BrowserResolution.scrollToElement(driver, paymentModule);
			WaitLibrary.waitForElementToBeClickable(driver, paymentModule, Constants_Defects.avg_explicit).click();
			
			// Fetch sample Directory
			String sampleDir = ExcelUtilities.getCellDataBySheetName(Constants_Defects.File_TestCases, "TestCases", TestCaseName,Constants_Defects.Col_sampleDirectory);
			System.out.println(sampleDir);
			
			// Fetch 1st Payment File Name
			String paymentFile1 = ExcelUtilities.getCellDataBySheetName(Constants_Defects.File_TestData, "ach_Client_Defects" ,TestCaseName, Constants_Defects.Col_Payment_File);
			((ExtentTest) test).log(LogStatus.INFO, "Payment File 1 : " + paymentFile1);
			
    		// Upload Payment File
    		log.info("Uploading Payment File via WinSCP");
			log.info("Source File is  :" + sampleDir + paymentFile1);
			log.info("Destination File is  :" + Constants_Defects.ACHPain008ChannelIn);
			FilesUpload.uploadFileByWinSCP(sampleDir + paymentFile1, Constants_Defects.ACHPain008ChannelIn);
			((ExtentTest) test).log(LogStatus.PASS, "File Uploaded");
			Thread.sleep(Constants_Defects.short_sleep);

			log.info("Click on Received Instruction Tab");
			WebElement recInsTab = ReceivedInstruction.recInsTab(driver);
			WaitLibrary.waitForElementToBeClickable(driver, recInsTab, Constants_Defects.avg_explicit).click();
			WaitLibrary.waitForAngular(driver);
    		
			log.info("Click on List View");
			WebElement listView = ReceivedInstruction.listView(driver);
			String listViewClass = WaitLibrary.waitForElementToBeVisible(driver, listView, Constants_Defects.avg_explicit).getAttribute("class");

			if (listViewClass.equalsIgnoreCase("btn-trans cmmonBtnColors")) {
				WaitLibrary.waitForElementToBeClickable(driver, listView, Constants_Defects.avg_explicit).click();
			}
			
			// Scroll to Uploaded Payment File
			BrowserResolution.scrollToElement(driver, ReceivedInstruction.getTransportNameStatusByFileName(driver, paymentFile1));
			js.executeScript("window.scrollBy(0,-120)");

			// Get and click Instruction ID
			WebElement instdid1 = ReceivedInstruction.getInsIdByTransportName(driver, paymentFile1);
			String insId1 = WaitLibrary.waitForElementToBeVisible(driver, instdid1, Constants_ACH.avg_explicit).getText();
			((ExtentTest) test).log(LogStatus.INFO, "Instruction id Payment 1::: " + insId1);
			WaitLibrary.waitForElementToBeClickable(driver, instdid1, Constants_ACH.avg_explicit).click();
			
			log.info("Verifying Instruction Page with Transport name");
			WebElement TransportName1 = ReceivedInstruction.verifyInstructionPage(driver, paymentFile1);
			
			log.info("Verifying Instruction Status");
			WebElement insStatusElem1 = ReceivedInstruction.getInsStatus(driver);
			String insStatus1 = WaitLibrary.waitForElementToBeVisible(driver, insStatusElem1, Constants_ACH.avg_explicit).getText();			
			Assert.assertEquals(insStatus1, "DEBULKED");
			((ExtentTest) test).log(LogStatus.PASS, "Instruction Status Payment 1: " + insStatus1);
			
			// Fetch 2nd Payment File Name
			String paymentFile2 = ExcelUtilities.getCellDataBySheetName(Constants_Defects.File_TestData, "ach_Client_Defects" ,TestCaseName, Constants_Defects.Col_Payment_File_1);
			((ExtentTest) test).log(LogStatus.INFO, "Payment File 2 : " + paymentFile2);
			
			// Upload Payment File
    		log.info("Uploading Payment File via WinSCP");
			log.info("Source File is  :" + sampleDir + paymentFile2);
			log.info("Destination File is  :" + Constants_Defects.ACHPain008ChannelIn);
			FilesUpload.uploadFileByWinSCP(sampleDir + paymentFile2, Constants_Defects.ACHPain008ChannelIn);
			((ExtentTest) test).log(LogStatus.PASS, "File Uploaded");
			Thread.sleep(Constants_Defects.short_sleep);
			
			log.info("Navigate to Payment Module");
			WebElement paymentModule1 = SideBarMenu.paymentModule(driver);
			BrowserResolution.scrollToElement(driver, paymentModule1);
			WaitLibrary.waitForElementToBeClickable(driver, paymentModule1, Constants_Defects.avg_explicit).click();

			log.info("Click on Received Instruction Tab");
			WebElement recInsTab1 = ReceivedInstruction.recInsTab(driver);
			WaitLibrary.waitForElementToBeClickable(driver, recInsTab1, Constants_Defects.avg_explicit).click();
			WaitLibrary.waitForAngular(driver);
			
			log.info("Click on List View");
			WebElement listView1 = ReceivedInstruction.listView(driver);
			String listViewClass1 = WaitLibrary.waitForElementToBeVisible(driver, listView1, Constants_Defects.avg_explicit).getAttribute("class");

			if (listViewClass1.equalsIgnoreCase("btn-trans cmmonBtnColors")) {
				WaitLibrary.waitForElementToBeClickable(driver, listView1, Constants_Defects.avg_explicit).click();
			}
			
			// Scroll to Uploaded Payment File
			BrowserResolution.scrollToElement(driver, ReceivedInstruction.getTransportNameStatusByFileName(driver, paymentFile2));
			js.executeScript("window.scrollBy(0,-120)");
			
			// Get and click Instruction ID
			WebElement instdid2 = ReceivedInstruction.getInsIdByTransportName(driver, paymentFile2);
			String insId2 = WaitLibrary.waitForElementToBeVisible(driver, instdid2, Constants_ACH.avg_explicit).getText();
			((ExtentTest) test).log(LogStatus.INFO, "Instruction id Payment 2:::" + insId2);
			WaitLibrary.waitForElementToBeClickable(driver, instdid2, Constants_ACH.avg_explicit).click();
			
			log.info("Verifying Instruction Page with Transport name");
			WebElement TransportName2 = ReceivedInstruction.verifyInstructionPage(driver, paymentFile2);
			
			log.info("Verifying Instruction Status");
			WebElement insStatusElem2 = ReceivedInstruction.getInsStatus(driver);
			String insStatus2 = WaitLibrary.waitForElementToBeVisible(driver, insStatusElem2, Constants_ACH.avg_explicit).getText();			
			Assert.assertEquals(insStatus2, "DEBULKED");
			((ExtentTest) test).log(LogStatus.PASS, "Instruction Status Payment 2 : " + insStatus2);
			
			BrowserResolution.scrollDown(driver);
			log.info("Get Payment id & Verify Status"); 
			WebElement pmtIdid = ReceivedInstruction.getPaymentId(driver); 
			String pmtId = WaitLibrary.waitForElementToBeVisible(driver, pmtIdid, Constants_Defects.avg_explicit).getText();
			WaitLibrary.waitForElementToBeClickable(driver, pmtIdid, Constants_Defects.avg_explicit).click(); 
			System.out.println("Payment ID 2 :::"+pmtId);
			
			WaitLibrary.waitForAngular(driver);
			log.info("Verifying Payment Page with PaymentId");
			WebElement PaymentIdHeader = ReceivedInstruction.verifyPaymentPage(driver, pmtId);
			
			WebElement pmtStatusid = ReceivedInstruction.getStatus(driver);
			String pmtStatus = WaitLibrary.waitForElementToBeVisible(driver, pmtStatusid, Constants_Defects.avg_explicit).getText();
			
			if(pmtStatus.equals("TIME_WAREHOUSED")) {
				CommonMethods.forceRelease(driver, pmtId, paymentFile2, insId2);
							
				WaitLibrary.waitForElementToBeClickable(driver, pmtIdid, Constants_Defects.avg_explicit).click();
				WaitLibrary.waitForAngular(driver);
				
				log.info("Verifying Payment Page with PaymentId");
				WebElement PaymentIdHeader1 = ReceivedInstruction.verifyPaymentPage(driver, pmtId);
				
				pmtStatus = WaitLibrary.waitForElementToBeVisible(driver, pmtStatusid, Constants_Defects.avg_explicit).getText();
			}
			
			Assert.assertEquals(pmtStatus, "DUPLICATE");
			((ExtentTest) test).log(LogStatus.PASS, "Payment Status : " + pmtStatus);
			
			WaitLibrary.waitForAngular(driver);
			log.info("Verify the presence of AcceptPayment, Ignore and Reject Payment buttons");
			
			// Verify Accept button
			try {
				WebElement acceptPayment = ReceivedInstruction.AcceptPayment(driver);
				((ExtentTest) test).log(LogStatus.PASS, "Accept Payment Button is present");
			}catch(Exception accept) {
				((ExtentTest) test).log(LogStatus.FAIL, "Accept Payment Button is not visible");
			}

			// Verify Ignore button
			try {
				WebElement ignorePayment = ReceivedInstruction.Ignore(driver);
				((ExtentTest) test).log(LogStatus.PASS, "Ignore Button is present");
			}catch(Exception ignore) {
				((ExtentTest) test).log(LogStatus.FAIL, "Ignore Button is not visible");
			}
			
			// Verify Reject button
			try {
				WebElement rejectPayment = ReceivedInstruction.RejectPayment(driver);
				((ExtentTest) test).log(LogStatus.PASS, "Reject Payment Button is present");
				
				log.info("Click on Reject Payment Button Button");
				WaitLibrary.waitForElementToBeClickable(driver, rejectPayment, Constants_ACH.avg_explicit).click();
				WaitLibrary.waitForAngular(driver);
				((ExtentTest) test).log(LogStatus.INFO, "Rejected Payment sent for Approval");
				
				CommonMethods.approveStatement(driver, pmtId);
				WaitLibrary.waitForAngular(driver);
				
				((ExtentTest) test).log(LogStatus.INFO, "After Approval");
				log.info("Navigate to Payment Module");
				WebElement paymentModule2 = SideBarMenu.paymentModule(driver);
				BrowserResolution.scrollToElement(driver, paymentModule2);
				WaitLibrary.waitForElementToBeClickable(driver, paymentModule2, Constants_Defects.avg_explicit).click();

				log.info("Click on Received Instruction Tab");
				WebElement recInsTab2 = ReceivedInstruction.recInsTab(driver);
				WaitLibrary.waitForElementToBeClickable(driver, recInsTab2, Constants_Defects.avg_explicit).click();
				WaitLibrary.waitForAngular(driver);
				
				log.info("Click on List View");
				WebElement listView2 = ReceivedInstruction.listView(driver);
				String listViewClass2 = WaitLibrary.waitForElementToBeVisible(driver, listView2, Constants_Defects.avg_explicit).getAttribute("class");

				if (listViewClass2.equalsIgnoreCase("btn-trans cmmonBtnColors")) {
					WaitLibrary.waitForElementToBeClickable(driver, listView2, Constants_Defects.avg_explicit).click();
				}
				
				WaitLibrary.waitForAngular(driver);
	    		
	    		WebElement searchInstructionIn = ReceivedInstruction.searchWithInsID(driver);
	    		searchInstructionIn.clear();
	    		searchInstructionIn.sendKeys(insId2, Keys.ENTER);
	    		WaitLibrary.waitForAngular(driver);
	    		
	    		CommonMethods.clickStatementWithFileName(driver, paymentFile2);
	    		WaitLibrary.waitForAngular(driver);
				BrowserResolution.scrollDown(driver);
				
				log.info("Verifying Instruction Page with Transport name");
				WebElement TransportName = ReceivedInstruction.verifyInstructionPage(driver, paymentFile2);
				BrowserResolution.scrollDown(driver);
				
				log.info("Click on Payment ID after Rejecting Payment");
				WebElement pmtIdid2 = ReceivedInstruction.getPaymentId(driver); 
				String pmtId2 = WaitLibrary.waitForElementToBeVisible(driver, pmtIdid2, Constants_Defects.avg_explicit).getText();
				WaitLibrary.waitForElementToBeClickable(driver, pmtIdid2, Constants_Defects.avg_explicit).click();
				WaitLibrary.waitForAngular(driver);
				
				log.info("Verifying Payment Page with PaymentId");
				WebElement PaymentIdHeader2 = ReceivedInstruction.verifyPaymentPage(driver, pmtId2);
				
				log.info("Verify Payment Status");
				WebElement paymentStatus = ReceivedInstruction.getStatus(driver);
				String paymtStatus = WaitLibrary.waitForElementToBeVisible(driver, paymentStatus, Constants_Defects.avg_explicit).getText();			
				System.out.println("Payment 2 Status afetr Rejecting Payment::::"+paymtStatus);
				Assert.assertEquals(paymtStatus, "REJECTED");
				((ExtentTest) test).log(LogStatus.PASS, "Payment Status after Rejection is :: " + paymtStatus);
				
			}catch(Exception reject) {
				((ExtentTest) test).log(LogStatus.FAIL, "Reject Payment Button is not visible");
				((ExtentTest) test).log(LogStatus.FAIL, "Error Info : " +reject.getMessage());
			}
			
			//****************************Make Changes Default*****************************************//
			
			log.info("Navigate to Payment Module");
			WebElement paymentModule2 = SideBarMenu.paymentModule(driver);
			BrowserResolution.scrollToElement(driver, paymentModule2);
			WaitLibrary.waitForElementToBeClickable(driver, paymentModule2, Constants_Defects.avg_explicit).click();

			log.info("Click on Received Instruction Tab");
			WebElement recInsTab2 = ReceivedInstruction.recInsTab(driver);
			WaitLibrary.waitForElementToBeClickable(driver, recInsTab2, Constants_Defects.avg_explicit).click();
			WaitLibrary.waitForAngular(driver);
			
			log.info("Navigate to Onboarding Data");
			BrowserResolution.scrollToElement(driver, onboardingData);
			WaitLibrary.waitForElementToBeClickable(driver, onboardingData, Constants_Defects.avg_explicit).click();

			log.info("Click on Party Service Association");
			WaitLibrary.waitForElementToBeClickable(driver, partyServiceAssociation, Constants_Defects.avg_explicit).click();
			
			log.info("Click on Grid View");
			WebElement gridViewdef = PartyServiceAssociation.gridView(driver);
			String gridViewClassdef = WaitLibrary.waitForElementToBeVisible(driver, gridViewdef, Constants_Defects.avg_explicit).getAttribute("class");
						
			if (gridViewClassdef.equalsIgnoreCase("btn-trans cmmonBtnColors")) {			
				WaitLibrary.waitForElementToBeClickable(driver, gridViewdef, Constants_Defects.avg_explicit).click();					
			}
			WaitLibrary.waitForAngular(driver);
			
			log.info("Click on Filter Icon");
			WaitLibrary.waitForAngular(driver);
			WebElement clickFilterIcondef = Approvals.clickFilterIcon(driver);
    		WaitLibrary.waitForElementToBeClickable(driver, clickFilterIcondef, Constants_Defects.avg_explicit).click();
    		WaitLibrary.waitForAngular(driver);
    		
    		log.info("Enter Party Service Association Code to filter");
    		WebElement filterWithKeyworddef = Approvals.filterWithKeyword(driver);
    		filterWithKeyworddef.sendKeys(partyServiceAssociationCode, Keys.ENTER);
    		WaitLibrary.waitForAngular(driver);
    		
    		log.info("Click on filter Icon");
    		WaitLibrary.waitForElementToBeClickable(driver, clickFilterIcondef, Constants_Defects.avg_explicit).click();
    		WaitLibrary.waitForAngular(driver);
    		
    		log.info("Click on Party Service Association Code");    					
    		List<WebElement> partyServiceAssCodeDataDef = PartyServiceAssociation.partyServiceAssDataList(driver);
			int partyServiceAssCodeCountDef = partyServiceAssCodeDataDef.size();
			String partyServiceDef = null;
			
			for(int l = 0; l < partyServiceAssCodeCountDef; l++) {
				try {
					partyServiceDef = PartyServiceAssociation.partyServiceAssTableData(driver, l, 1).getText();
				}catch (NoSuchElementException ne) {
					throw new Exception("No Party Service Code found");
				}
				
				if(partyServiceDef.equals(partyServiceAssociationCode)) {
					WebElement PartyServiceDef = PartyServiceAssociation.partyServiceAssTableData(driver, l, 1);
					WaitLibrary.waitForElementToBeClickable(driver, PartyServiceDef, Constants_Defects.avg_explicit).click();
					break;
				}else if (l == partyServiceAssCodeCountDef-1 && !partyServiceDef.equals(partyServiceAssociationCode)) {
					throw new Exception("Party Service Association Code is not Available");
				}
			}
			WaitLibrary.waitForAngular(driver);
			
			((ExtentTest) test).log(LogStatus.INFO, "Edit PSA to Default : " + partyServiceAssociationCode);
			
    		log.info("Click on Edit");
    		WaitLibrary.waitForAngular(driver);
    		try { 		
    			js.executeScript("arguments[0].click();", PartyServiceAssociation.editBtn(driver));
    		}
    		catch (Exception ne) {
				throw new Exception("Unable to edit PSA");
			}
    		WaitLibrary.waitForAngular(driver);
    		
    		log.info("Select Payment Duplicate check to No");
    		WebElement paymentDuplicateCheckNo = PartyServiceAssociation.PaymentDuplicateNo(driver);
    		BrowserResolution.scrollToElement(driver, paymentDuplicateCheckNo);
    		js.executeScript("arguments[0].click();", paymentDuplicateCheckNo);
    		WaitLibrary.waitForAngular(driver);
    		BrowserResolution.scrollDown(driver);
    		
    		log.info("Save Changes");
    		WebElement saveChange = PartyServiceAssociation.SaveChanges(driver);
			WaitLibrary.waitForElementToBeClickable(driver, saveChange, Constants_Defects.avg_explicit).click();
			
			log.info("Approve party service association code");
			CommonMethods.approveStatement(driver, partyServiceAssociationCode);
			WaitLibrary.waitForAngular(driver);
			
			((ExtentTest) test).log(LogStatus.INFO, "PSA updated to Default : " + partyServiceAssociationCode);
			
			ExcelUtilities.setCellDataBySheetName(Constants_Defects.File_TestCases, "TestCases", TestCaseRow, Constants_Defects.Col_status, "Pass");
			ExcelUtilities.setCellDataBySheetName(Constants_Defects.File_TestCases, "TestCases", TestCaseRow, Constants_Defects.Col_outputsPath,TC_Outputs);
			ExcelUtilities.setCellDataBySheetName(Constants_Defects.File_TestCases, "TestCases", TestCaseRow, Constants_Defects.Col_exeDateTime, GenericFunctions.getCurrentTime());

		}
		catch(AssertionError ae){
			ExcelUtilities.setCellDataBySheetName(Constants_Defects.File_TestCases, "TestCases", TestCaseRow, Constants_Defects.Col_status, "Failed");
			assertionerror=ae.getMessage();
			CaptureScreenshot.captureScreenshot();
			((ExtentTest) test).log(LogStatus.FAIL, "<br>"+"<b>Assertion Error</b>"+"<br>"+ae.getMessage()+LoginLogout.test.addScreenCapture(CaptureScreenshot.screenshotName));
			ExcelUtilities.setCellDataBySheetName(Constants_Defects.File_TestCases, "TestCases", TestCaseRow, Constants_Defects.Col_outputsPath,TC_Outputs);
			ExcelUtilities.setCellDataBySheetName(Constants_Defects.File_TestCases, "TestCases", TestCaseRow, Constants_Defects.Col_AssertionComments, assertionerror);
		}
		catch(Exception et){
			ExcelUtilities.setCellDataBySheetName(Constants_Defects.File_TestCases, "TestCases", TestCaseRow, Constants_Defects.Col_status, "Failed");
			exceptionerror=et.getMessage();			
			CaptureScreenshot.captureScreenshot();
			((ExtentTest) test).log(LogStatus.FAIL, "<br>"+"<b>Exception</b>"+"<br>"+et.getMessage()+LoginLogout.test.addScreenCapture(CaptureScreenshot.screenshotName));
			ExcelUtilities.setCellDataBySheetName(Constants_Defects.File_TestCases, "TestCases", TestCaseRow, Constants_Defects.Col_outputsPath,TC_Outputs);
			ExcelUtilities.setCellDataBySheetName(Constants_Defects.File_TestCases, "TestCases", TestCaseRow, Constants_Defects.Col_comments, exceptionerror);
		}
	}
}
