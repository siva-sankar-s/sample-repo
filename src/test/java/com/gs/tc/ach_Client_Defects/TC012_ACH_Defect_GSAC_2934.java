package com.gs.tc.ach_Client_Defects;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.gs.pages.DistributedInstructions;
import com.gs.pages.ReceivedInstruction;
import com.gs.pages.SideBarMenu;
import com.gs.utilities.BrowserResolution;
import com.gs.utilities.CaptureScreenshot;
import com.gs.utilities.CommonMethods;
import com.gs.utilities.Constants_ACH;
import com.gs.utilities.Constants_Defects;
import com.gs.utilities.ExcelUtilities;
import com.gs.utilities.FilesUpload;
import com.gs.utilities.GenericFunctions;
import com.gs.utilities.Log;
import com.gs.utilities.LoginLogout;
import com.gs.utilities.SampleFileModifier;
import com.gs.utilities.WaitLibrary;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class TC012_ACH_Defect_GSAC_2934 extends LoginLogout{
	@Test

	public void executeTC012() throws Exception {
		try {
			Log.startTestCase(log, TestCaseName);
			
			((ExtentTest) test).log(LogStatus.INFO, "TC012_ACH_Defect_GSAC_2934 with File NACK");
			
			LocalDateTime now = LocalDateTime.now();			
			DateTimeFormatter dateYMD = DateTimeFormatter.ofPattern("yyyyMMdd");					
			String locDateYr = dateYMD.format(now);
			
			DateTimeFormatter dateMDY = DateTimeFormatter.ofPattern("MM/dd/yy"); 
			String locDate = dateMDY.format(now);

			// Fetch sample Directory
			String sampleDir = ExcelUtilities.getCellDataBySheetName(Constants_Defects.File_TestCases, "TestCases", TestCaseName,Constants_Defects.Col_sampleDirectory);
			System.out.println(sampleDir);
			
			// Fetch Incoming Payment File Name
			String paymentFile = ExcelUtilities.getCellDataBySheetName(Constants_Defects.File_TestData, "ach_Client_Defects" ,TestCaseName, Constants_Defects.Col_Payment_File);
			System.out.println(paymentFile);
			((ExtentTest) test).log(LogStatus.INFO, "Payment File name is : " + paymentFile);
			
			// Fetch ACK File Name
			String nackFile = ExcelUtilities.getCellDataBySheetName(Constants_Defects.File_TestData, "ach_Client_Defects" ,TestCaseName, Constants_Defects.Col_ACK_File);
			System.out.println(nackFile);
			((ExtentTest) test).log(LogStatus.INFO, "NACK File name is : " + nackFile);
			
			// Upload Payment File
    		log.info("Uploading Payment File via WinSCP");
			log.info("Source File is  :" + sampleDir + paymentFile);
			log.info("Destination File is  :" + Constants_Defects.ACHPain001ChannelIn);
			FilesUpload.uploadFileByWinSCP(sampleDir + paymentFile, Constants_Defects.ACHPain001ChannelIn);
			((ExtentTest) test).log(LogStatus.PASS, "Payment File Uploaded");
			Thread.sleep(Constants_Defects.short_sleep);

			log.info("Click on Received Instruction Tab");
			WebElement recInsTab = ReceivedInstruction.recInsTab(driver);
			WaitLibrary.waitForElementToBeClickable(driver, recInsTab, Constants_ACH.avg_explicit).click();
			WaitLibrary.waitForAngular(driver);
			
			log.info("Click on List View");
			WebElement listView = ReceivedInstruction.listView(driver);
			String listViewClass = WaitLibrary.waitForElementToBeVisible(driver, listView, Constants_ACH.avg_explicit).getAttribute("class");

			if (listViewClass.equalsIgnoreCase("btn-trans cmmonBtnColors")) {
				WaitLibrary.waitForElementToBeClickable(driver, listView, Constants_ACH.avg_explicit).click();
			}
			
			// Verify Status of Uploaded Payment File
			BrowserResolution.scrollToElement(driver, ReceivedInstruction.getTransportNameStatusByFileName(driver, paymentFile));
			js.executeScript("window.scrollBy(0,-120)");
			
			// Get and click Instruction ID
			WebElement instdid = ReceivedInstruction.getInsIdByTransportName(driver, paymentFile);
			String insId = WaitLibrary.waitForElementToBeVisible(driver, instdid, Constants_ACH.avg_explicit).getText();
			((ExtentTest) test).log(LogStatus.INFO, "Payment Instruction id:::" + insId);
			WaitLibrary.waitForElementToBeClickable(driver, instdid, Constants_ACH.avg_explicit).click();
			
			log.info("Verifying Instruction Page with Transport name");
			WebElement TransportName = ReceivedInstruction.verifyInstructionPage(driver, paymentFile);
			WaitLibrary.waitForAngular(driver);
			
			Thread.sleep(Constants_Defects.tooshort_sleep);
			
			log.info("Verifying Instruction Status");
			WebElement insStatusElem = ReceivedInstruction.getInsStatus(driver);
			String insStatus = WaitLibrary.waitForElementToBeVisible(driver, insStatusElem, Constants_ACH.avg_explicit).getText();			
			Assert.assertEquals(insStatus, "DEBULKED");
			((ExtentTest) test).log(LogStatus.PASS, "Payment Instruction Status : " + insStatus);
			
			Thread.sleep(Constants_Defects.short_sleep);
			
			log.info("Click on Payment ID");
			BrowserResolution.scrollDown(driver);
			List<WebElement> paymentsList = ReceivedInstruction.paymentList(driver);
			int paymentscount = paymentsList.size();
			String paymentID="";
			for(int l=0; l<paymentscount; l++) {
				String paymentFun = ReceivedInstruction.originalPaymentReference(driver, l, "MethodOfPayment").getText();
				String status = ReceivedInstruction.originalPaymentReference(driver, l, "Status").getText();
				if(paymentFun.equals("SAMEDAY_ACH_OUT")) {
					WebElement pmtIdElem = ReceivedInstruction.originalPaymentReference(driver, l, "PaymentID");
					paymentID = WaitLibrary.waitForElementToBeVisible(driver, pmtIdElem, Constants_ACH.avg_explicit).getText();
					if(status.equals("WAIT DEBIT PROCESSING BATCH")) {
						//Wait for Bulking for 5 mins
						TimeUnit.MINUTES.sleep(6);
					}else if(status.equals("TIME WAREHOUSED")) {
						WaitLibrary.waitForElementToBeClickable(driver, pmtIdElem, Constants_ACH.avg_explicit).click();
						WaitLibrary.waitForAngular(driver);
						Thread.sleep(Constants_Defects.tooshort_sleep);
						
						CommonMethods.forceRelease(driver, paymentID, paymentFile, insId);
						WaitLibrary.waitForAngular(driver);
						Thread.sleep(Constants_Defects.tooshort_sleep);
						TimeUnit.MINUTES.sleep(6);
					}else if(status.equals("FOR BULKING")) {
						//Wait for Bulking for 4 mins
						TimeUnit.MINUTES.sleep(4);
					}
					WebElement clickPayment = ReceivedInstruction.clickPaymentID(driver, paymentID);
					WaitLibrary.waitForElementToBeClickable(driver, clickPayment, Constants_ACH.avg_explicit).click();
					break;
					}else if (l==paymentscount-1 && !paymentFun.equals("SAMEDAY_ACH_OUT")) {
						throw new Exception("SAMEDAY_ACH_OUT payment is not Available");
				}
			}

			WaitLibrary.waitForAngular(driver);
			
			log.info("Verifying Payment Page with PaymentId");
			WebElement paymentIdHeader = ReceivedInstruction.verifyPaymentPage(driver, paymentID);
			
			Thread.sleep(Constants_Defects.tooshort_sleep);
				
			// Verifying Payment status
			WebElement PaymentStatusElement = ReceivedInstruction.getStatus(driver);
			String PaymentStatus = WaitLibrary.waitForElementToBeVisible(driver, PaymentStatusElement, Constants_ACH.avg_explicit).getText();
			Assert.assertEquals(PaymentStatus, "COMPLETED");
			((ExtentTest) test).log(LogStatus.PASS, "Payment Status : " + PaymentStatus);
			
			//get Output Instruction Id
			WebElement outInsIdElem = ReceivedInstruction.outputInstructionId(driver);
			String outInsId = WaitLibrary.waitForElementToBeVisible(driver, outInsIdElem, Constants_ACH.avg_explicit).getText();
			((ExtentTest) test).log(LogStatus.PASS, "Output Instruction ID : " + outInsId);
			
			Thread.sleep(3000);
			
			// Getting and Validating modifier
			char modifierIDChar = outInsId.charAt(outInsId.length()-1);
			String modifierID = String.valueOf(modifierIDChar);
			
			if (Character.isAlphabetic(modifierIDChar)) {
				((ExtentTest) test).log(LogStatus.PASS, "Modifier ID : " + modifierIDChar + " is a valid");
		    }
		    else {
		      throw new Exception("Modifier ID : " + modifierIDChar + " is a not valid");
		    }
			
			Thread.sleep(3000);
			
			//Creating new NACK Batch from reference
			SampleFileModifier.createNACKRef(sampleDir+"Ref\\"+ nackFile, sampleDir+nackFile);
			System.out.println("REF : "+sampleDir+"Ref\\"+ nackFile);
			System.out.println("Dest : "+sampleDir+ nackFile);
			
			// creating new values for Nack file
			HashMap<String, String> updateVal = new HashMap<>();
			
			updateVal.put("{{mm/dd/yy}}", locDate);
			updateVal.put("{{AFIDMOD}}", modifierID);
			
			// Updating Nack File
			SampleFileModifier.updateNackFile(sampleDir+nackFile, updateVal);
			
			// Upload Payment File
    		log.info("Uploading ACK File via WinSCP");
			log.info("Source File is  :" + sampleDir + nackFile);
			log.info("Destination File is  :" + Constants_Defects.FEDACH_IN);
			FilesUpload.uploadFileByWinSCP(sampleDir + nackFile, Constants_Defects.FEDACH_IN);
			((ExtentTest) test).log(LogStatus.PASS, "NACK File Uploaded");
			Thread.sleep(Constants_Defects.short_sleep);
			
			log.info("Click on Received Instruction Tab");
			WebElement recInsTab1 = ReceivedInstruction.recInsTab(driver);
			WaitLibrary.waitForElementToBeClickable(driver, recInsTab1, Constants_ACH.avg_explicit).click();
			WaitLibrary.waitForAngular(driver);
			
			Thread.sleep(Constants_ACH.tooshort_sleep);
    		WebElement searchInstructionClear = ReceivedInstruction.searchWithInsID(driver);
    		searchInstructionClear.clear();
    		searchInstructionClear.sendKeys(Keys.ENTER);
    		WaitLibrary.waitForAngular(driver);
			
			log.info("Click on List View");
			WebElement listView1 = ReceivedInstruction.listView(driver);
			String listViewClass1 = WaitLibrary.waitForElementToBeVisible(driver, listView1, Constants_ACH.avg_explicit).getAttribute("class");

			if (listViewClass1.equalsIgnoreCase("btn-trans cmmonBtnColors")) {
				WaitLibrary.waitForElementToBeClickable(driver, listView1, Constants_ACH.avg_explicit).click();
			}
			
			// Verify Status of Uploaded Payment File
			BrowserResolution.scrollToElement(driver, ReceivedInstruction.getTransportNameStatusByFileName(driver, nackFile));
			js.executeScript("window.scrollBy(0,-120)");
			
			// Get and click Instruction ID
			WebElement instdid1 = ReceivedInstruction.getInsIdByTransportName(driver, nackFile);
			String insId1 = WaitLibrary.waitForElementToBeVisible(driver, instdid1, Constants_ACH.avg_explicit).getText();
			((ExtentTest) test).log(LogStatus.INFO, "NACK Instruction id:::" + insId1);
			WaitLibrary.waitForElementToBeClickable(driver, instdid1, Constants_ACH.avg_explicit).click();
			WaitLibrary.waitForAngular(driver);
			
			log.info("Verifying Instruction Page with Transport name");
			WebElement TransportName1 = ReceivedInstruction.verifyInstructionPage(driver, nackFile);
			WaitLibrary.waitForAngular(driver);
			
			log.info("Verifying Instruction Status");
			WebElement insStatusElem1 = ReceivedInstruction.getInsStatus(driver);
			String insStatus1 = WaitLibrary.waitForElementToBeVisible(driver, insStatusElem1, Constants_ACH.avg_explicit).getText();			
			Assert.assertEquals(insStatus1, "DEBULKED");
			((ExtentTest) test).log(LogStatus.PASS, "NACK Instruction Status : " + insStatus1);
			
			log.info("Get Original Instruction ID");
			WebElement origInsId = ReceivedInstruction.getOriginalInsID(driver);
			String origInstructionID = WaitLibrary.waitForElementToBeVisible(driver, origInsId, Constants_Defects.avg_explicit).getText();
			WaitLibrary.waitForAngular(driver);
			String originalInsID = CommonMethods.filterOriginalInsId(origInstructionID);
			Assert.assertEquals(originalInsID, outInsId);
			((ExtentTest) test).log(LogStatus.PASS, "Original Instruction ID matched with Output Instruction ID in NACK Ins : " + originalInsID);
			
			log.info("Get Instruction Status");
			WebElement instrStatus = ReceivedInstruction.InsStatus(driver);
			String instructionStatus = WaitLibrary.waitForElementToBeVisible(driver, instrStatus, Constants_Defects.avg_explicit).getText();
			WaitLibrary.waitForAngular(driver);
			String instStatus = CommonMethods.filterInstructionStatus(instructionStatus);
			Assert.assertEquals(instStatus, "REJECTED");
			((ExtentTest) test).log(LogStatus.PASS, "Instruction details Status is : " + instStatus);
				
			log.info("Navigate to Distributed Instructions");
			WebElement distInstructions = SideBarMenu.distInstructions(driver);
			BrowserResolution.scrollToElement(driver, distInstructions);
			WaitLibrary.waitForElementToBeClickable(driver, distInstructions, Constants_ACH.avg_explicit).click();
			
			log.info("Click on Grid View");
			WebElement gridView = DistributedInstructions.gridView(driver);
			String gridViewClass= WaitLibrary.waitForElementToBeVisible(driver, gridView, Constants_ACH.avg_explicit).getAttribute("class");
						
			if (gridViewClass.equalsIgnoreCase("btn-trans cmmonBtnColors")) {			
				WaitLibrary.waitForElementToBeClickable(driver, gridView, Constants_ACH.avg_explicit).click();					
			}
			Thread.sleep(Constants_ACH.short_sleep);
			WaitLibrary.waitForAngular(driver);
			
			log.info("Search Distributed Instructions");
			WebElement searchInstructionIn = DistributedInstructions.searchWithDistInsID(driver);
    		searchInstructionIn.clear();
    		searchInstructionIn.sendKeys(outInsId, Keys.ENTER);
    		WaitLibrary.waitForAngular(driver);
    		
    		List<WebElement> distInsData = DistributedInstructions.distDataList(driver);
			int distDataCount = distInsData.size();
    		
			String uniqueOutputRef = null;
			
    		for(int l=0; l<distDataCount; l++) {
				try {
					uniqueOutputRef = DistributedInstructions.distInstData(driver, l, 1).getText();
					System.out.println(uniqueOutputRef);
				}catch (NoSuchElementException ne) {
					throw new Exception("No Distribution instruction found");
				}
				
				if(uniqueOutputRef.equals(outInsId)) {
					((ExtentTest) test).log(LogStatus.INFO, "Unique output reference : " + uniqueOutputRef);
					
					String outInsName = DistributedInstructions.distInstData(driver, l, 3).getText();
					System.out.println("outInsName : " +outInsName);
					
					if (outInsName.startsWith("ACH026015079D"+locDateYr+"T")) {
						((ExtentTest) test).log(LogStatus.PASS, "Output Instruction Name is valid : " + outInsName);
						System.out.println("Output Instruction Name is valid");
					}else {
						throw new Exception("Output Instruction Name is not valid " + outInsName);
					}
					
					String disInsStatus = DistributedInstructions.distInstData(driver, l, 7).getText();
					System.out.println("Status : " +disInsStatus);
					Assert.assertEquals(disInsStatus, "WAIT_INSTRUCTION_REJECT");
					((ExtentTest) test).log(LogStatus.PASS, "Distributed instruction status : " + disInsStatus);
					Thread.sleep(Constants_Defects.tooshort_sleep);
					
					WebElement rejectBtnElem = DistributedInstructions.rejectDistribution(driver);
					WaitLibrary.waitForElementToBeClickable(driver, rejectBtnElem, Constants_ACH.avg_explicit).click();
					((ExtentTest) test).log(LogStatus.INFO, "Reject Distributed instruction");
					WaitLibrary.waitForAngular(driver);
					
					Thread.sleep(Constants_Defects.tooshort_sleep);
					
					CommonMethods.approveStatement(driver, uniqueOutputRef);
					WaitLibrary.waitForAngular(driver);
					
					Thread.sleep(Constants_Defects.tooshort_sleep);
					
					WebElement uniqueOutputRefe = DistributedInstructions.distInstData(driver, l, 1);
					WaitLibrary.waitForElementToBeClickable(driver, uniqueOutputRefe, Constants_ACH.avg_explicit).click();
					WaitLibrary.waitForAngular(driver);
					Thread.sleep(Constants_Defects.tooshort_sleep);
					
					WebElement getInsStatusElem = DistributedInstructions.getInsStatus(driver);
					String distInsStatus = WaitLibrary.waitForElementToBeVisible(driver, getInsStatusElem, Constants_Defects.avg_explicit).getText();
					System.out.println("Dist Status : " +distInsStatus);
					((ExtentTest) test).log(LogStatus.INFO, "Distributed instruction status after Rejection : " +distInsStatus);
					
					break;
				}else if (l==distDataCount-1 && !uniqueOutputRef.equals(outInsId)) {
					throw new Exception("Distribution instruction is not Available");
				}
			}			
    		
			ExcelUtilities.setCellDataBySheetName(Constants_Defects.File_TestCases, "TestCases", TestCaseRow, Constants_Defects.Col_status, "Pass");
			ExcelUtilities.setCellDataBySheetName(Constants_Defects.File_TestCases, "TestCases", TestCaseRow, Constants_Defects.Col_outputsPath,TC_Outputs);
			ExcelUtilities.setCellDataBySheetName(Constants_Defects.File_TestCases, "TestCases", TestCaseRow, Constants_Defects.Col_exeDateTime, GenericFunctions.getCurrentTime());
			
		}
		catch(AssertionError ae){
			ExcelUtilities.setCellDataBySheetName(Constants_Defects.File_TestCases, "TestCases", TestCaseRow, Constants_Defects.Col_status, "Failed");
			assertionerror=ae.getMessage();
			CaptureScreenshot.captureScreenshot();
			((ExtentTest) test).log(LogStatus.FAIL, "<br>"+"<b>Assertion Error</b>"+"<br>"+ae.getMessage()+LoginLogout.test.addScreenCapture(CaptureScreenshot.screenshotName));
			ExcelUtilities.setCellDataBySheetName(Constants_Defects.File_TestCases, "TestCases", TestCaseRow, Constants_Defects.Col_outputsPath,TC_Outputs);
			ExcelUtilities.setCellDataBySheetName(Constants_Defects.File_TestCases, "TestCases", TestCaseRow, Constants_Defects.Col_AssertionComments, assertionerror);
		}
		catch(Exception et){
			ExcelUtilities.setCellDataBySheetName(Constants_Defects.File_TestCases, "TestCases", TestCaseRow, Constants_Defects.Col_status, "Failed");
			exceptionerror=et.getMessage();			
			CaptureScreenshot.captureScreenshot();
			((ExtentTest) test).log(LogStatus.FAIL, "<br>"+"<b>Exception</b>"+"<br>"+et.getMessage()+LoginLogout.test.addScreenCapture(CaptureScreenshot.screenshotName));
			ExcelUtilities.setCellDataBySheetName(Constants_Defects.File_TestCases, "TestCases", TestCaseRow, Constants_Defects.Col_outputsPath,TC_Outputs);
			ExcelUtilities.setCellDataBySheetName(Constants_Defects.File_TestCases, "TestCases", TestCaseRow, Constants_Defects.Col_comments, exceptionerror);
		}
	}
}