package com.gs.tc.ach_Client_Defects;

import java.util.List;

import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.gs.pages.Approvals;
import com.gs.pages.PartyServiceAssociation;
import com.gs.pages.ReceivedInstruction;
import com.gs.pages.SideBarMenu;
import com.gs.utilities.BrowserResolution;
import com.gs.utilities.CaptureScreenshot;
import com.gs.utilities.CommonMethods;
import com.gs.utilities.Constants_ACH;
import com.gs.utilities.Constants_Defects;
import com.gs.utilities.ExcelUtilities;
import com.gs.utilities.FilesUpload;
import com.gs.utilities.GenericFunctions;
import com.gs.utilities.Log;
import com.gs.utilities.LoginLogout;
import com.gs.utilities.WaitLibrary;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class TC021_ACH_Defects_GSAC_2575_IB_Batch extends LoginLogout{
	@Test

	public void executeTC021() throws Exception {
		try {

			Log.startTestCase(log, TestCaseName);
			
			((ExtentTest) test).log(LogStatus.INFO, "Test Case : Batch Duplication - Inbound");
			
			String partyServiceAssociationCode = ExcelUtilities.getCellDataBySheetName(Constants_Defects.File_TestData, "ach_Client_Defects" ,TestCaseName, Constants_Defects.Col_Party_Service_Association_Code);
			
			log.info("Navigate to Onboarding Data");
			WebElement onboardingData = SideBarMenu.onboardingData(driver);
			BrowserResolution.scrollToElement(driver, onboardingData);
			WaitLibrary.waitForElementToBeClickable(driver, onboardingData, Constants_Defects.avg_explicit).click();

			log.info("Click on Party Service Association");
			WebElement partyServiceAssociation = SideBarMenu.partyServiceAss(driver);
			WaitLibrary.waitForElementToBeClickable(driver, partyServiceAssociation, Constants_Defects.avg_explicit).click();

			log.info("Click on Grid View");
			WebElement gridView = PartyServiceAssociation.gridView(driver);
			String gridViewClass= WaitLibrary.waitForElementToBeVisible(driver, gridView, Constants_Defects.avg_explicit).getAttribute("class");
						
			if (gridViewClass.equalsIgnoreCase("btn-trans cmmonBtnColors")) {			
				WaitLibrary.waitForElementToBeClickable(driver, gridView, Constants_Defects.avg_explicit).click();					
			}
			WaitLibrary.waitForAngular(driver);
			
			log.info("Click on Filter Icon");
			WaitLibrary.waitForAngular(driver);
			WebElement clickFilterIcon = Approvals.clickFilterIcon(driver);
    		WaitLibrary.waitForElementToBeClickable(driver, clickFilterIcon, Constants_Defects.avg_explicit).click();
    		WaitLibrary.waitForAngular(driver);
    	
    		log.info("Enter Party Service Association Code to filter");
    		WebElement filterWithKeyword = Approvals.filterWithKeyword(driver);
    		filterWithKeyword.sendKeys(partyServiceAssociationCode, Keys.ENTER);
    		WaitLibrary.waitForAngular(driver);
    		
    		log.info("Click on filter Icon");
    		WaitLibrary.waitForElementToBeClickable(driver, clickFilterIcon, Constants_Defects.avg_explicit).click();
    		WaitLibrary.waitForAngular(driver);
    		
    		log.info("Click on Party Service Association Code");    					
    		List<WebElement> partyServiceAssCodeData = PartyServiceAssociation.partyServiceAssDataList(driver);
			int partyServiceAssCodeCount = partyServiceAssCodeData.size();
			String partyService = null;
			
			for(int l = 0; l < partyServiceAssCodeCount; l++) {
				try {
					partyService = PartyServiceAssociation.partyServiceAssTableData(driver, l, 1).getText();
				}catch (NoSuchElementException ne) {
					throw new Exception("No Party Service Code found");
				}
				
				if(partyService.equals(partyServiceAssociationCode)) {
					WebElement PartyService = PartyServiceAssociation.partyServiceAssTableData(driver, l, 1);
					WaitLibrary.waitForElementToBeClickable(driver, PartyService, Constants_Defects.avg_explicit).click();
					break;
				}else if (l == partyServiceAssCodeCount-1 && !partyService.equals(partyServiceAssociationCode)) {
					throw new Exception("Party Service Association Code is not Available");
				}
			}
			WaitLibrary.waitForAngular(driver);
			
			((ExtentTest) test).log(LogStatus.INFO, "Edit PSA : " + partyServiceAssociationCode);
			
    		log.info("Click on Edit");
    		try {
    			js.executeScript("arguments[0].click();", PartyServiceAssociation.editBtn(driver));
    		}
    		catch (Exception ne) {
				throw new Exception("Unable to edit PSA");
			}
    		WaitLibrary.waitForAngular(driver);
    		
    		log.info("Select Batch Duplicate check to Yes");
			WebElement batchDuplicateCheckYes = PartyServiceAssociation.BatchDuplicateYes(driver);
			BrowserResolution.scrollToElement(driver, batchDuplicateCheckYes);
			js.executeScript("arguments[0].click();", batchDuplicateCheckYes);
			js.executeScript("window.scrollBy(0,-120)");
			WaitLibrary.waitForAngular(driver);
			
			log.info("Enter Batch Duplicate Check - Number Of Days");
			WebElement batchDuplicateCheckDays = PartyServiceAssociation.BatchDuplicateDays(driver);
			BrowserResolution.scrollToElement(driver, batchDuplicateCheckDays);
			js.executeScript("arguments[0].click();", batchDuplicateCheckDays);
			batchDuplicateCheckDays.clear();
			batchDuplicateCheckDays.sendKeys("2");
    		WaitLibrary.waitForAngular(driver);
    		
    		String batchFiledValue="ValueDate";
    		Thread.sleep(Constants_Defects.tooshort_sleep);
    		log.info("Select Batch Field Name");
			WebElement batchFieldName = PartyServiceAssociation.BatchFieldName(driver);
			WaitLibrary.waitForElementToBeClickable(driver, batchFieldName, Constants_Defects.avg_explicit).click();
			WaitLibrary.waitForAngular(driver);
			
			WebElement batchFieldList = PartyServiceAssociation.BatchFieldList(driver, batchFiledValue);
			String batchList = PartyServiceAssociation.BatchFieldList(driver, batchFiledValue).getText();
			WaitLibrary.waitForElementToBeClickable(driver, batchFieldList, Constants_Defects.avg_explicit).click();
			((ExtentTest) test).log(LogStatus.INFO, "Selected Field : "+batchList);
			BrowserResolution.scrollDown(driver);
			Thread.sleep(Constants_Defects.tooshort_sleep);
			
    		log.info("Save Changes");
    		WebElement saveChanges = PartyServiceAssociation.SaveChanges(driver);
			WaitLibrary.waitForElementToBeClickable(driver, saveChanges, Constants_Defects.avg_explicit).click();
			WaitLibrary.waitForAngular(driver);
			
			log.info("Approve party service association code");
			CommonMethods.approveStatement(driver, partyServiceAssociationCode);
			WaitLibrary.waitForAngular(driver);
			
			((ExtentTest) test).log(LogStatus.INFO, "PSA : " + partyServiceAssociationCode + " - Updated");
			
			log.info("Navigate to Payment Module");
			WebElement paymentModule = SideBarMenu.paymentModule(driver);
			BrowserResolution.scrollToElement(driver, paymentModule);
			WaitLibrary.waitForElementToBeClickable(driver, paymentModule, Constants_Defects.avg_explicit).click();
			
			// Fetch sample Directory
			String sampleDir = ExcelUtilities.getCellDataBySheetName(Constants_Defects.File_TestCases, "TestCases", TestCaseName,Constants_Defects.Col_sampleDirectory);
			System.out.println(sampleDir);
			
			// Fetch 1st Payment File Name
			String paymentFile1 = ExcelUtilities.getCellDataBySheetName(Constants_Defects.File_TestData, "ach_Client_Defects" ,TestCaseName, Constants_Defects.Col_Payment_File);
			((ExtentTest) test).log(LogStatus.INFO, "Payment File 1 : " + paymentFile1);
			
    		// Upload Payment File
    		log.info("Uploading Payment File via WinSCP");
			log.info("Source File is  :" + sampleDir + paymentFile1);
			log.info("Destination File is  :" + Constants_Defects.FEDACH_IN);
			FilesUpload.uploadFileByWinSCP(sampleDir + paymentFile1, Constants_Defects.FEDACH_IN);
			((ExtentTest) test).log(LogStatus.PASS, "File Uploaded");
			Thread.sleep(Constants_Defects.short_sleep);

			log.info("Click on Received Instruction Tab");
			WebElement recInsTab = ReceivedInstruction.recInsTab(driver);
			WaitLibrary.waitForElementToBeClickable(driver, recInsTab, Constants_Defects.avg_explicit).click();
			WaitLibrary.waitForAngular(driver);
    		
			log.info("Click on List View");
			WebElement listView = ReceivedInstruction.listView(driver);
			String listViewClass = WaitLibrary.waitForElementToBeVisible(driver, listView, Constants_Defects.avg_explicit).getAttribute("class");

			if (listViewClass.equalsIgnoreCase("btn-trans cmmonBtnColors")) {
				WaitLibrary.waitForElementToBeClickable(driver, listView, Constants_Defects.avg_explicit).click();
			}
			
			// Scroll to Uploaded Payment File
			BrowserResolution.scrollToElement(driver, ReceivedInstruction.getTransportNameStatusByFileName(driver, paymentFile1));
			js.executeScript("window.scrollBy(0,-120)");

			// Get and click Instruction ID
			WebElement instdid1 = ReceivedInstruction.getInsIdByTransportName(driver, paymentFile1);
			String insId1 = WaitLibrary.waitForElementToBeVisible(driver, instdid1, Constants_ACH.avg_explicit).getText();
			((ExtentTest) test).log(LogStatus.INFO, "Instruction id Payment 1::: " + insId1);
			WaitLibrary.waitForElementToBeClickable(driver, instdid1, Constants_ACH.avg_explicit).click();
			
			log.info("Verifying Instruction Page with Transport name");
			WebElement TransportName1 = ReceivedInstruction.verifyInstructionPage(driver, paymentFile1);
			
			log.info("Verifying Instruction Status");
			WebElement insStatusElem1 = ReceivedInstruction.getInsStatus(driver);
			String insStatus1 = WaitLibrary.waitForElementToBeVisible(driver, insStatusElem1, Constants_ACH.avg_explicit).getText();			
			Assert.assertEquals(insStatus1, "DEBULKED");
			((ExtentTest) test).log(LogStatus.PASS, "Instruction Status Payment 1: " + insStatus1);
			
			// Fetch 2nd Payment File Name
			String paymentFile2 = ExcelUtilities.getCellDataBySheetName(Constants_Defects.File_TestData, "ach_Client_Defects" ,TestCaseName, Constants_Defects.Col_Payment_File_1);
			((ExtentTest) test).log(LogStatus.INFO, "Payment File 2 : " + paymentFile2);
			
			// Upload Payment File
    		log.info("Uploading Payment File via WinSCP");
			log.info("Source File is  :" + sampleDir + paymentFile2);
			log.info("Destination File is  :" + Constants_Defects.FEDACH_IN);
			FilesUpload.uploadFileByWinSCP(sampleDir + paymentFile2, Constants_Defects.FEDACH_IN);
			((ExtentTest) test).log(LogStatus.PASS, "File Uploaded");
			Thread.sleep(Constants_Defects.short_sleep);
			
			log.info("Navigate to Payment Module");
			WebElement paymentModule1 = SideBarMenu.paymentModule(driver);
			BrowserResolution.scrollToElement(driver, paymentModule1);
			WaitLibrary.waitForElementToBeClickable(driver, paymentModule1, Constants_Defects.avg_explicit).click();

			log.info("Click on Received Instruction Tab");
			WebElement recInsTab1 = ReceivedInstruction.recInsTab(driver);
			WaitLibrary.waitForElementToBeClickable(driver, recInsTab1, Constants_Defects.avg_explicit).click();
			WaitLibrary.waitForAngular(driver);
			
			log.info("Click on List View");
			WebElement listView1 = ReceivedInstruction.listView(driver);
			String listViewClass1 = WaitLibrary.waitForElementToBeVisible(driver, listView1, Constants_Defects.avg_explicit).getAttribute("class");

			if (listViewClass1.equalsIgnoreCase("btn-trans cmmonBtnColors")) {
				WaitLibrary.waitForElementToBeClickable(driver, listView1, Constants_Defects.avg_explicit).click();
			}
			
			// Scroll to Uploaded Payment File
			BrowserResolution.scrollToElement(driver, ReceivedInstruction.getTransportNameStatusByFileName(driver, paymentFile2));
			js.executeScript("window.scrollBy(0,-120)");
			
			// Get and click Instruction ID
			WebElement instdid2 = ReceivedInstruction.getInsIdByTransportName(driver, paymentFile2);
			String insId2 = WaitLibrary.waitForElementToBeVisible(driver, instdid2, Constants_ACH.avg_explicit).getText();
			((ExtentTest) test).log(LogStatus.INFO, "Instruction id Payment 2:::" + insId2);
			WaitLibrary.waitForElementToBeClickable(driver, instdid2, Constants_ACH.avg_explicit).click();
			
			log.info("Verifying Instruction Page with Transport name");
			WebElement TransportName2 = ReceivedInstruction.verifyInstructionPage(driver, paymentFile2);
			
			log.info("Verifying Instruction Status");
			WebElement insStatusElem2 = ReceivedInstruction.getInsStatus(driver);
			String insStatus2 = WaitLibrary.waitForElementToBeVisible(driver, insStatusElem2, Constants_ACH.avg_explicit).getText();			
			Assert.assertEquals(insStatus2, "DEBULKED");
			((ExtentTest) test).log(LogStatus.PASS, "Instruction Status Payment 2 : " + insStatus2);
			
			log.info("Navigate to Input Batch Information Tab");
			BrowserResolution.scrollDown(driver);
			js.executeScript("arguments[0].click();", ReceivedInstruction.batchInformation(driver));
			Thread.sleep(Constants_Defects.tooshort_sleep);

			log.info("Get Batch id & Verify Status");
			String battIdid = ReceivedInstruction.inputBatchData(driver, 0, "BatchID").getText();
			WebElement batchId = ReceivedInstruction.inputBatchData(driver, 0, "BatchID");
			WaitLibrary.waitForElementToBeClickable(driver, batchId, Constants_Defects.avg_explicit).click();
			((ExtentTest) test).log(LogStatus.INFO, "Batch id:::"+battIdid);
			
			WaitLibrary.waitForAngular(driver);
			
			log.info("Verifying Batch Page with BatchId");
			WebElement batchIdHeader = ReceivedInstruction.verifyBatchPage(driver, battIdid);
			
			WebElement batStatusid = ReceivedInstruction.getBatchStatus(driver);
			String batStatus = WaitLibrary.waitForElementToBeVisible(driver, batStatusid, Constants_Defects.avg_explicit).getText();			
			System.out.println("Batch Status::::"+batStatus);
			Assert.assertEquals(batStatus, "DUPLICATE");
			((ExtentTest) test).log(LogStatus.PASS, "Batch Status : " + batStatus);
			
			//****************************Make Changes Default*****************************************//
			
			log.info("Navigate to Payment Module");
			WebElement paymentModule2 = SideBarMenu.paymentModule(driver);
			BrowserResolution.scrollToElement(driver, paymentModule2);
			WaitLibrary.waitForElementToBeClickable(driver, paymentModule2, Constants_Defects.avg_explicit).click();

			log.info("Click on Received Instruction Tab");
			WebElement recInsTab2 = ReceivedInstruction.recInsTab(driver);
			WaitLibrary.waitForElementToBeClickable(driver, recInsTab2, Constants_Defects.avg_explicit).click();
			WaitLibrary.waitForAngular(driver);
			
			log.info("Navigate to Onboarding Data");
			BrowserResolution.scrollToElement(driver, onboardingData);
			WaitLibrary.waitForElementToBeClickable(driver, onboardingData, Constants_Defects.avg_explicit).click();

			log.info("Click on Party Service Association");
			WaitLibrary.waitForElementToBeClickable(driver, partyServiceAssociation, Constants_Defects.avg_explicit).click();
			
			log.info("Click on Grid View");
			WebElement gridViewdef = PartyServiceAssociation.gridView(driver);
			String gridViewClassdef = WaitLibrary.waitForElementToBeVisible(driver, gridViewdef, Constants_Defects.avg_explicit).getAttribute("class");
						
			if (gridViewClassdef.equalsIgnoreCase("btn-trans cmmonBtnColors")) {			
				WaitLibrary.waitForElementToBeClickable(driver, gridViewdef, Constants_Defects.avg_explicit).click();					
			}
			WaitLibrary.waitForAngular(driver);
			
			log.info("Click on Filter Icon");
			WaitLibrary.waitForAngular(driver);
			WebElement clickFilterIcondef = Approvals.clickFilterIcon(driver);
    		WaitLibrary.waitForElementToBeClickable(driver, clickFilterIcondef, Constants_Defects.avg_explicit).click();
    		WaitLibrary.waitForAngular(driver);
    		
    		log.info("Enter Party Service Association Code to filter");
    		WebElement filterWithKeyworddef = Approvals.filterWithKeyword(driver);
    		filterWithKeyworddef.sendKeys(partyServiceAssociationCode, Keys.ENTER);
    		WaitLibrary.waitForAngular(driver);
    		
    		log.info("Click on filter Icon");
    		WaitLibrary.waitForElementToBeClickable(driver, clickFilterIcondef, Constants_Defects.avg_explicit).click();
    		WaitLibrary.waitForAngular(driver);
    		
    		log.info("Click on Party Service Association Code");    					
    		List<WebElement> partyServiceAssCodeDataDef = PartyServiceAssociation.partyServiceAssDataList(driver);
			int partyServiceAssCodeCountDef = partyServiceAssCodeDataDef.size();
			String partyServiceDef = null;
			
			for(int l = 0; l < partyServiceAssCodeCountDef; l++) {
				try {
					partyServiceDef = PartyServiceAssociation.partyServiceAssTableData(driver, l, 1).getText();
				}catch (NoSuchElementException ne) {
					throw new Exception("No Party Service Code found");
				}
				
				if(partyServiceDef.equals(partyServiceAssociationCode)) {
					WebElement PartyServiceDef = PartyServiceAssociation.partyServiceAssTableData(driver, l, 1);
					WaitLibrary.waitForElementToBeClickable(driver, PartyServiceDef, Constants_Defects.avg_explicit).click();
					break;
				}else if (l == partyServiceAssCodeCountDef-1 && !partyServiceDef.equals(partyServiceAssociationCode)) {
					throw new Exception("Party Service Association Code is not Available");
				}
			}
			WaitLibrary.waitForAngular(driver);
			
			((ExtentTest) test).log(LogStatus.INFO, "Edit PSA to Default : " + partyServiceAssociationCode);
			
    		log.info("Click on Edit");
    		try { 		
    			js.executeScript("arguments[0].click();", PartyServiceAssociation.editBtn(driver));
    		}
    		catch (Exception ne) {
				throw new Exception("Unable to edit PSA");
			}	
    		WaitLibrary.waitForAngular(driver);
    		
    		log.info("Select Batch Duplicate check to No");
			WebElement batchDuplicateCheckNo = PartyServiceAssociation.BatchDuplicateNo(driver);
			BrowserResolution.scrollToElement(driver, batchDuplicateCheckNo);
			js.executeScript("arguments[0].click();", batchDuplicateCheckNo);
			WaitLibrary.waitForAngular(driver);
			BrowserResolution.scrollDown(driver);
    		
    		log.info("Save Changes");
    		WebElement saveChange = PartyServiceAssociation.SaveChanges(driver);
			WaitLibrary.waitForElementToBeClickable(driver, saveChange, Constants_Defects.avg_explicit).click();
			
			log.info("Approve party service association code");
			CommonMethods.approveStatement(driver, partyServiceAssociationCode);
			WaitLibrary.waitForAngular(driver);
			
			((ExtentTest) test).log(LogStatus.INFO, "PSA updated to Default : " + partyServiceAssociationCode);
			
			ExcelUtilities.setCellDataBySheetName(Constants_Defects.File_TestCases, "TestCases", TestCaseRow, Constants_Defects.Col_status, "Pass");
			ExcelUtilities.setCellDataBySheetName(Constants_Defects.File_TestCases, "TestCases", TestCaseRow, Constants_Defects.Col_outputsPath,TC_Outputs);
			ExcelUtilities.setCellDataBySheetName(Constants_Defects.File_TestCases, "TestCases", TestCaseRow, Constants_Defects.Col_exeDateTime, GenericFunctions.getCurrentTime());

		}
		catch(AssertionError ae){
			ExcelUtilities.setCellDataBySheetName(Constants_Defects.File_TestCases, "TestCases", TestCaseRow, Constants_Defects.Col_status, "Failed");
			assertionerror=ae.getMessage();
			CaptureScreenshot.captureScreenshot();
			((ExtentTest) test).log(LogStatus.FAIL, "<br>"+"<b>Assertion Error</b>"+"<br>"+ae.getMessage()+LoginLogout.test.addScreenCapture(CaptureScreenshot.screenshotName));
			ExcelUtilities.setCellDataBySheetName(Constants_Defects.File_TestCases, "TestCases", TestCaseRow, Constants_Defects.Col_outputsPath,TC_Outputs);
			ExcelUtilities.setCellDataBySheetName(Constants_Defects.File_TestCases, "TestCases", TestCaseRow, Constants_Defects.Col_AssertionComments, assertionerror);
		}
		catch(Exception et){
			ExcelUtilities.setCellDataBySheetName(Constants_Defects.File_TestCases, "TestCases", TestCaseRow, Constants_Defects.Col_status, "Failed");
			exceptionerror=et.getMessage();			
			CaptureScreenshot.captureScreenshot();
			((ExtentTest) test).log(LogStatus.FAIL, "<br>"+"<b>Exception</b>"+"<br>"+et.getMessage()+LoginLogout.test.addScreenCapture(CaptureScreenshot.screenshotName));
			ExcelUtilities.setCellDataBySheetName(Constants_Defects.File_TestCases, "TestCases", TestCaseRow, Constants_Defects.Col_outputsPath,TC_Outputs);
			ExcelUtilities.setCellDataBySheetName(Constants_Defects.File_TestCases, "TestCases", TestCaseRow, Constants_Defects.Col_comments, exceptionerror);
		}
	}
}
