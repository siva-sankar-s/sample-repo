package com.gs.tc.ach_Client_Defects;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.camel.language.Constant;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.gs.pages.ReceivedInstruction;
import com.gs.utilities.BrowserResolution;
import com.gs.utilities.CaptureScreenshot;
import com.gs.utilities.CommonMethods;

import com.gs.utilities.Constants_ACH;
import com.gs.utilities.Constants_Defects;
import com.gs.utilities.ExcelUtilities;
import com.gs.utilities.FilesUpload;
import com.gs.utilities.GenericFunctions;
import com.gs.utilities.Log;
import com.gs.utilities.LoginLogout;
import com.gs.utilities.SampleFileModifier;
import com.gs.utilities.WaitLibrary;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class TC015_ACH_Defect_GSAC_4641 extends LoginLogout{
	@Test

	public void executeTC015() throws Exception {
		try {
			Log.startTestCase(log, TestCaseName);
			
			((ExtentTest) test).log(LogStatus.INFO, "TC015_ACH_Defect_GSAC_4641 for Outbound DD payment");
			
			LocalDateTime now = LocalDateTime.now();			
			DateTimeFormatter dateYMD = DateTimeFormatter.ofPattern("yyyy-MM-dd");					
			String locDateYr = dateYMD.format(now);
			System.out.println(locDateYr);
			
			// Fetch sample Directory
			String sampleDir = ExcelUtilities.getCellDataBySheetName(Constants_Defects.File_TestCases, "TestCases", TestCaseName,Constants_Defects.Col_sampleDirectory);
			System.out.println(sampleDir);
			
			// Fetch Incoming Payment File Name
			String paymentFile = ExcelUtilities.getCellDataBySheetName(Constants_Defects.File_TestData, "ach_Client_Defects" ,TestCaseName, Constants_Defects.Col_Payment_File);
			System.out.println(paymentFile);
			((ExtentTest) test).log(LogStatus.INFO, "File name is : " + paymentFile);
			
			//Creating new payment File from reference
			SampleFileModifier.createNACKRef(sampleDir+"Ref\\"+ paymentFile, sampleDir+paymentFile);
			System.out.println("REF : "+sampleDir+"Ref\\"+ paymentFile);
			System.out.println("Dest : "+sampleDir+ paymentFile);
			
			// creating new values for Payment file
			HashMap<String, String> updateVal = new HashMap<>();
			
			updateVal.put("{{yyyy-MM-dd}}", locDateYr);
			
			// Updating Payment File
			SampleFileModifier.updateNackFile(sampleDir+paymentFile, updateVal);
			
			// Upload Payment File
    		log.info("Uploading Payment File via WinSCP");
			log.info("Source File is  :" + sampleDir + paymentFile);
			log.info("Destination File is  :" + Constants_Defects.ACHPain008ChannelIn);
			FilesUpload.uploadFileByWinSCP(sampleDir + paymentFile, Constants_Defects.ACHPain008ChannelIn);
			((ExtentTest) test).log(LogStatus.PASS, "File Uploaded");
			Thread.sleep(Constants_Defects.short_sleep);
			
			log.info("Click on Received Instruction Tab");
			WebElement recInsTab = ReceivedInstruction.recInsTab(driver);
			WaitLibrary.waitForElementToBeClickable(driver, recInsTab, Constants_ACH.avg_explicit).click();
			WaitLibrary.waitForAngular(driver);
			
			log.info("Click on List View");
			WebElement listView = ReceivedInstruction.listView(driver);
			String listViewClass = WaitLibrary.waitForElementToBeVisible(driver, listView, Constants_ACH.avg_explicit).getAttribute("class");

			if (listViewClass.equalsIgnoreCase("btn-trans cmmonBtnColors")) {
				WaitLibrary.waitForElementToBeClickable(driver, listView, Constants_ACH.avg_explicit).click();
			}
			
			// Verify Status of Uploaded Payment File
			BrowserResolution.scrollToElement(driver, ReceivedInstruction.getTransportNameStatusByFileName(driver, paymentFile));
			js.executeScript("window.scrollBy(0,-120)");
			
			// Get and click Instruction ID
			WebElement instdid = ReceivedInstruction.getInsIdByTransportName(driver, paymentFile);
			String insId = WaitLibrary.waitForElementToBeVisible(driver, instdid, Constants_ACH.avg_explicit).getText();
			((ExtentTest) test).log(LogStatus.INFO, "Instruction id:::" + insId);
			WaitLibrary.waitForElementToBeClickable(driver, instdid, Constants_ACH.avg_explicit).click();
			
			log.info("Verifying Instruction Page with Transport name");
			WebElement TransportName = ReceivedInstruction.verifyInstructionPage(driver, paymentFile);
			WaitLibrary.waitForAngular(driver);
			Thread.sleep(Constants_Defects.tooshort_sleep);
			
			log.info("Verifying Instruction Status");
			WebElement insStatusElem = ReceivedInstruction.getInsStatus(driver);
			String insStatus = WaitLibrary.waitForElementToBeVisible(driver, insStatusElem, Constants_ACH.avg_explicit).getText();			
			Assert.assertEquals(insStatus, "DEBULKED");
			((ExtentTest) test).log(LogStatus.PASS, "Instruction Status : " + insStatus);
			WaitLibrary.waitForAngular(driver);
			Thread.sleep(Constants_Defects.tooshort_sleep);
			
			log.info("Verify Status of Payment");
			BrowserResolution.scrollDown(driver);
			WaitLibrary.waitForAngular(driver);
			Thread.sleep(Constants_Defects.tooshort_sleep);
			
			String status = ReceivedInstruction.originalPaymentReference(driver, 0, "Status").getText();
			WebElement pmtIdElem = ReceivedInstruction.originalPaymentReference(driver, 0, "PaymentID");
			String paymentID = WaitLibrary.waitForElementToBeVisible(driver, pmtIdElem, Constants_ACH.avg_explicit).getText();
			if(status.equals("WAIT CREDIT PROCESSING BATCH") || status.equals("TIME WAREHOUSED") || status.equals("FOR BULKING")) {
				if(status.equals("WAIT CREDIT PROCESSING BATCH")) {
					//Wait for Bulking for 5 mins
					TimeUnit.MINUTES.sleep(8);
				}else if(status.equals("TIME WAREHOUSED")) {
					WaitLibrary.waitForElementToBeClickable(driver, pmtIdElem, Constants_ACH.avg_explicit).click();
					WaitLibrary.waitForAngular(driver);
					Thread.sleep(Constants_Defects.tooshort_sleep);
					
					CommonMethods.forceRelease(driver, paymentID, paymentFile, insId);
					WaitLibrary.waitForAngular(driver);
					Thread.sleep(Constants_Defects.tooshort_sleep);
					TimeUnit.MINUTES.sleep(6);
				}
				else if(status.equals("FOR BULKING")) {
					//Wait for Bulking for 4 mins
					TimeUnit.MINUTES.sleep(4);
				}
				
				log.info("Click on Received Instruction Tab");
				WaitLibrary.waitForElementToBeClickable(driver, recInsTab, Constants_ACH.avg_explicit).click();
				WaitLibrary.waitForAngular(driver);
				
				log.info("Click on List View");
				WebElement listView2 = ReceivedInstruction.listView(driver);
				String listViewClass2 = WaitLibrary.waitForElementToBeVisible(driver, listView2, Constants_ACH.avg_explicit).getAttribute("class");
	
				if (listViewClass2.equalsIgnoreCase("btn-trans cmmonBtnColors")) {
					WaitLibrary.waitForElementToBeClickable(driver, listView2, Constants_ACH.avg_explicit).click();
				}
				
				Thread.sleep(Constants_ACH.tooshort_sleep);
	    		WebElement searchInstructionIn = ReceivedInstruction.searchWithInsID(driver);
	    		searchInstructionIn.clear();
	    		searchInstructionIn.sendKeys(insId, Keys.ENTER);
	    		WaitLibrary.waitForAngular(driver);
	    		CommonMethods.clickStatementWithFileName(driver, paymentFile);
	    		
				log.info("Verifying Instruction Page with Transport name");
				WebElement TransportNameIn = ReceivedInstruction.verifyInstructionPage(driver, paymentFile);
				WaitLibrary.waitForAngular(driver);
				
				BrowserResolution.scrollDown(driver);
				WaitLibrary.waitForAngular(driver);
				Thread.sleep(Constants_Defects.tooshort_sleep);
			}
			Thread.sleep(Constants_Defects.tooshort_sleep);
			List<WebElement> paymentsList = ReceivedInstruction.paymentList(driver);
			int paymentscount = paymentsList.size();
			System.out.println(paymentscount);
			int statusCount=0;
			for(int l=0; l<paymentscount; l++) {
				String Status = ReceivedInstruction.originalPaymentReference(driver, l, "Status").getText();
				System.out.println(Status);
				if(Status.equals("COMPLETED")) {
					statusCount++;
					System.out.println(statusCount);
				}
			}
			System.out.println(statusCount);
			if(statusCount == paymentscount) {
				((ExtentTest) test).log(LogStatus.PASS, "Payment Status of all " + paymentscount +" "+"Payments are in COMPLETED status");
			}else {
				((ExtentTest) test).log(LogStatus.FAIL,"Payment status is not moved to COMPLETED status");
				throw new Exception("Payment status is not moved to COMPLETED status");
			}
			
			Thread.sleep(Constants_Defects.tooshort_sleep);
			WebElement pmtIdElement = ReceivedInstruction.originalPaymentReference(driver, 0, "PaymentID");
			String paymentId = WaitLibrary.waitForElementToBeVisible(driver, pmtIdElement, Constants_ACH.avg_explicit).getText();
			WaitLibrary.waitForElementToBeClickable(driver, pmtIdElement, Constants_ACH.avg_explicit).click();
			WaitLibrary.waitForAngular(driver);
			
			log.info("Verifying Payment Page with PaymentId");
			WebElement paymentIdHeader = ReceivedInstruction.verifyPaymentPage(driver, paymentId);
				
			// Verifying Payment status
			WebElement PaymentStatusElement = ReceivedInstruction.getStatus(driver);
			String PaymentStatus = WaitLibrary.waitForElementToBeVisible(driver, PaymentStatusElement, Constants_ACH.avg_explicit).getText();
			Assert.assertEquals(PaymentStatus, "COMPLETED");
			((ExtentTest) test).log(LogStatus.PASS, "Payment Status : " + PaymentStatus);
			WaitLibrary.waitForAngular(driver);
			
			//Get Value Date
			WebElement getValueDate = ReceivedInstruction.getValDate(driver);
			Thread.sleep(Constants_Defects.tooshort_sleep);
			String valueDate = getValueDate.getText();
			String valuedate = CommonMethods.filterValueDate(valueDate);
			((ExtentTest) test).log(LogStatus.INFO, "Obtained Value Date is:: "+valuedate);
			BrowserResolution.scrollDown(driver);
			
			DateTimeFormatter DateYMD = DateTimeFormatter.ofPattern("yyMMdd");
			LocalDate date = LocalDate.parse(valuedate);
			String ValueDate = DateYMD.format(date);
			System.out.println(ValueDate);
			
			log.info("Navigate to External Communication Tab");
			js.executeScript("arguments[0].click();", ReceivedInstruction.getExtCommunicationTab(driver));
			WaitLibrary.waitForAngular(driver);
			BrowserResolution.scrollDown(driver);
			((ExtentTest) test).log(LogStatus.INFO, "Verifying value date with out file");
			
			log.info("Verify Direction");
			List<WebElement> extDataLsit = ReceivedInstruction.extCommunicationList(driver);
			int extDataCount = extDataLsit.size();
			int directionCount = 0;
			for (int j = 0; j < extDataCount; j++) {
				String getDirection = ReceivedInstruction.externalCommunication(driver, j, "Direction").getText();
				if (getDirection.equals("OUT")) {
					Assert.assertEquals(getDirection, "OUT");
					// Download the file
					ReceivedInstruction.externalCommDwnldBtn(driver, j).click();
					directionCount++;
					break;
				}
			}
			WaitLibrary.waitForAngular(driver);
			Thread.sleep(Constants_Defects.short_sleep);
			
			if (directionCount != 1) {
				System.out.println("Direction not matched");
				((ExtentTest) test).log(LogStatus.FAIL, "OUT file not found in External Communication");
			}
			Thread.sleep(Constants_Defects.tooshort_sleep);
			
			String val = CommonMethods.verifyACHOutFile("valueDate", "DD");
			System.out.println("Value Date : " + val);
			
			if(ValueDate.equals(val)) {
				((ExtentTest) test).log(LogStatus.PASS, "Value date matched with Out File Date");
			}else {
				((ExtentTest) test).log(LogStatus.FAIL, "Value date is not matched with Out File Date");
			}
			
			ExcelUtilities.setCellDataBySheetName(Constants_Defects.File_TestCases, "TestCases", TestCaseRow, Constants_Defects.Col_status, "Pass");
			ExcelUtilities.setCellDataBySheetName(Constants_Defects.File_TestCases, "TestCases", TestCaseRow, Constants_Defects.Col_outputsPath,TC_Outputs);
			ExcelUtilities.setCellDataBySheetName(Constants_Defects.File_TestCases, "TestCases", TestCaseRow, Constants_Defects.Col_exeDateTime, GenericFunctions.getCurrentTime());
			
		}
		catch(AssertionError ae){
			ExcelUtilities.setCellDataBySheetName(Constants_Defects.File_TestCases, "TestCases", TestCaseRow, Constants_Defects.Col_status, "Failed");
			assertionerror=ae.getMessage();
			CaptureScreenshot.captureScreenshot();
			((ExtentTest) test).log(LogStatus.FAIL, "<br>"+"<b>Assertion Error</b>"+"<br>"+ae.getMessage()+LoginLogout.test.addScreenCapture(CaptureScreenshot.screenshotName));
			ExcelUtilities.setCellDataBySheetName(Constants_Defects.File_TestCases, "TestCases", TestCaseRow, Constants_Defects.Col_outputsPath,TC_Outputs);
			ExcelUtilities.setCellDataBySheetName(Constants_Defects.File_TestCases, "TestCases", TestCaseRow, Constants_Defects.Col_AssertionComments, assertionerror);
		}
		catch(Exception et){
			ExcelUtilities.setCellDataBySheetName(Constants_Defects.File_TestCases, "TestCases", TestCaseRow, Constants_Defects.Col_status, "Failed");
			exceptionerror=et.getMessage();			
			CaptureScreenshot.captureScreenshot();
			((ExtentTest) test).log(LogStatus.FAIL, "<br>"+"<b>Exception</b>"+"<br>"+et.getMessage()+LoginLogout.test.addScreenCapture(CaptureScreenshot.screenshotName));
			ExcelUtilities.setCellDataBySheetName(Constants_Defects.File_TestCases, "TestCases", TestCaseRow, Constants_Defects.Col_outputsPath,TC_Outputs);
			ExcelUtilities.setCellDataBySheetName(Constants_Defects.File_TestCases, "TestCases", TestCaseRow, Constants_Defects.Col_comments, exceptionerror);
		}
	}
}
